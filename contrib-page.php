<?php include("inc/head.php"); ?>

<?php include("inc/header.php"); ?>


<div class="container-fluid mt-4 ">

	<div id="main_row" class="row no-gutters">

		<!-- contribution -->
		<div class="col-8" id="container_contrib">
			
<?php foreach ($page->images as $key => $img): ?>
<?php
$options = array(
	'upscaling' => false,
	 'quality' => 80
);
$low_img = $img->width("100px", $options);
$med_img = $img->width("500px", $options);
?>

		    <div class="col-<?=random(3,4)?> offset-<?=random(0,2)?> contrib" data-ws="<?= $contrib->related_workshop->id ?>">
		    	<article class="media">
					<img class="blur-up lazyload" src="<?= $low_img->url; ?>" data-src="<?= $med_img->url ?>">
		      	</article>
		    </div>
<?php endforeach; ?>

<?php foreach ($page->sounds as $key => $snd): ?>
		    <div class="col-5 offset-<?=random(0,2)?> contrib sound" data-ws="<?= $contrib->related_workshop->id ?>">
		    	<article class="media">
					<audio class="player" controls>
					    <source src="<?= $snd->url ?>" type="audio/mp3" />
					</audio>
		    	</article>
		    </div>
<?php endforeach; ?>

		    <div class="col-5 offset-<?=random(0,2)?> contrib sound" data-ws="<?= $contrib->related_workshop->id ?>">
		    	<article class="media">
					<?= $page->body ?>
		    	</article>
		    </div>

		</div>

</div>


<?php include("inc/foot.php"); ?>

