<div class="container-fluid p-0 caption">
	<div class="row no-gutters">
<?php if ($contrib->related_workshop): ?>
		<div class="col-0 col-sm-0 col-md-8">
			<h3><?= $contrib->related_workshop->title ?></h3>
		</div>
<?php endif; ?>
		<div class="col-12 col-sm-12 col-md-4">
			<p><i class="fas fa-edit"></i> <?= $contrib->author ?></p>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<p class="id"><?= $key ?></p>
		</div>
	</div>
</div>