<!DOCTYPE html>
<html lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?= $page->title; ?></title>

	<script src="<?= $config->urls->templates?>scripts/node_modules/lazysizes/lazysizes.min.js" defer></script>

	<!-- fontawesome -->
	<link href="<?= $config->urls->templates?>styles/fontawesome/css/all.min.css" rel="stylesheet" />

	<!-- bootstrap grid -->
	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>scripts/node_modules/bootstrap/dist/css/bootstrap-grid.min.css" />

	<!-- Plyr js -->
	<link rel="stylesheet" href="<?= $config->urls->templates?>scripts/node_modules/plyr/dist/plyr.css" />
	<script src="<?= $config->urls->templates?>scripts/node_modules/plyr/dist/plyr.min.js" defer></script>

	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>styles/main.css" />
	<script src="<?= $config->urls->templates?>scripts/main.js" defer></script>

	<!-- flickity -->
	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>scripts/node_modules/flickity/dist/flickity.min.css" />
	<script src="<?= $config->urls->templates?>scripts/node_modules/flickity/dist/flickity.pkgd.min.js" defer></script>

</head>
<body>