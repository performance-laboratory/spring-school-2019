<header class="container-fluid p-3">
	<div class="row">
		<div class="col">
			<p>Université Grenoble Alpes</p>
		</div>
		<div class="col">
			<h2><a href="<?= $homepage->url ?>"><?= $homepage->title; ?></a></h2>
		</div>
		<div class="col">
			<p>
<?php include("inc/lang_switcher.php"); ?>
			</p>
		</div>
	</div>
</header>