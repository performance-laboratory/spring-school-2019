<?php
foreach($languages as $language) {
  $selected = '';

  // if this page isn't viewable (active) for the language, skip it
  if(!$page->viewable($language)) continue;

  // if language is current user's language, make it selected
  if($user->language->id == $language->id) $selected = "selected";

  // determine the "local" URL for this language
  $url = $page->localUrl($language);

  // output the option tag
  echo "<a class='p-2 $selected' href='$url'>$language->title</a>";
}
?>