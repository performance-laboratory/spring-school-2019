document.addEventListener("DOMContentLoaded",  function() {
  init();
}, false);

function init() {
	const players = Plyr.setup('.player');

	// var audio_conf = document.querySelector(".audio_conf");
	// if(audio_conf) updateOnAudio(players[0]);

  var filtres = document.querySelectorAll(".container_checkbox input");
  for (var i = 0; i < filtres.length; i++) {
      filtres[i].addEventListener("click", checkboxInteractions);
  }

  ScreenSize();
  // console.log(screen_width);

  // shuffle nodelist des contributions
  // cf. https://stackoverflow.com/a/25176872
  var list = document.querySelector('#container_contrib'), i;
  for (i = list.children.length; i >= 0; i--) {
      list.appendChild(list.children[Math.random() * i | 0]);
  }
  // compo pour écrans mobiles
  if (screen_width < 768) {
    displayMobile();
  }

  /* fenetre modale */
  // slider
  flkty = false;
  var imgs = document.querySelectorAll(".contrib img");
  for (var i = 0; i < imgs.length; i++) {
    imgs[i].addEventListener("click", showSlider);
  }

  // modal contributions text
  var contrib_txt = document.querySelectorAll(".contrib.text");
  for (var i = 0; i < contrib_txt.length; i++) {
    contrib_txt[i].addEventListener("click", showText);
  }

  // modal conf
  var div_conf = document.querySelectorAll(".event.conf");
  for (var i = 0; i < div_conf.length; i++) {
    div_conf[i].addEventListener("click", showText);
  }

  // fermeture fenêtre modale
  // au click
  var close_modal = document.querySelectorAll(".close_modal");
  for (var i = 0; i < close_modal.length; i++) {
    close_modal[i].addEventListener("click", closeModal);
  }
  // à la touche ESc
  var body = document.querySelector("body");
  body.addEventListener("keydown", keysPressed); 
}

function keysPressed(e) {
  var modal = document.querySelector(".modal.active");
  if (e.key === "Escape" && modal) {
    closeModal();
  }
}



function showText() {
  // console.log(this);

  var div = closest(this, function(el) {
    return el.tagName === "DIV";
    // return el.classList.contains('contrib');
  });

  var container_txt = document.querySelector("#container_txt");
  var txt = container_txt.querySelector(".modal_content");
  clone_div = div.cloneNode(true);
  clone_div.className = "contrib txt";
  clone_div.querySelector("article").className = "";

  txt.appendChild(clone_div);
  // slider.appendChild(slide);
  container_txt.classList.add("active");
}




function displayMobile() {
  var array_of_ws_id = [];
  var divs_ws = document.querySelectorAll("#container_ws > div");
  for (var i = 0; i < divs_ws.length; i++) {
    array_of_ws_id.push(divs_ws[i].getAttribute("data-ws"));
  }
  for (var i = 0; i < array_of_ws_id.length; i++) {
    var related_contribs = document.querySelectorAll(".contrib[data-ws='"+array_of_ws_id[i]+"']");
    // console.log(related_contribs);
    for (var j = 0; j < related_contribs.length; j++) {
      var related_ws_container = document.querySelector("#container_ws > div[data-ws='"+array_of_ws_id[i]+"']");
      var mobile_container = related_ws_container.querySelector(".container_contrib_mobile");
      mobile_container.appendChild(related_contribs[j]);
    }
    // array_of_ws[i]
  }
  

  var els = document.querySelectorAll("#container_contrib > div");
  // var data = els[j].getAttribute("data-ws");
}


function ScreenSize() {
  var w = window,
  d = document,
  e = d.documentElement,
  g = d.getElementsByTagName('body')[0];

  var w2 = window,
  d2 = document,
  e2 = d2.documentElement,
  g2 = d2.getElementsByTagName('body')[0];
    
  screen_height = w2.innerHeight|| e2.clientHeight|| g2.clientHeight;
  screen_width = w2.innerWidth|| e2.clientWidth|| g2.clientWidth;
}


function closeModal() {
  
  if (flkty) flkty.destroy();
  
  var modal = document.querySelector(".modal.active");
  var modal_content = modal.querySelector(".modal_content");
  while (modal_content.firstChild) {
    modal_content.removeChild(modal_content.firstChild);
  }
  modal.classList.remove("active");

  /*
  var container_slider = document.querySelector("#container_slider");
  while (slider.firstChild) {
    slider.removeChild(slider.firstChild);
  }
  container_slider.classList.remove("active");
  */
}


function showSlider() {
  var div = closest(this, function(el) {
    return el.classList.contains('contrib');
  });

  var ws = div.getAttribute("data-ws");
  var divs_related = Array.from(selectImgs(ws));
  // index de la .contrib choisie
  var index = divs_related.indexOf(div);

  var container_slider = document.querySelector("#container_slider");
  var slider = document.querySelector("#slider");
  for (var i = 0; i < divs_related.length; i++) {
    var slide = document.createElement("DIV");
    slide.className = "slide";
    clone_div = divs_related[i].cloneNode(true)
    clone_div.classList = "contrib";
    clone_div.querySelector("img").className = "";
    slide.appendChild(clone_div);
    slider.appendChild(slide);
  }

  container_slider.classList.add("active");
  flkty = new Flickity(slider, {
    // options
    lazyLoad: true,
    cellAlign: 'center',
    fullscreen: true,
    initialIndex: index, // le slide goto index de la .contrib choisie
    setGallerySize: false,
    pageDots: false
  });
}

function selectImgs(ws) {
  var els = document.querySelectorAll(".contrib");
  var tableau_choix = [];
  tableau_choix.push(ws);

  var set_resultat = new Set();
  var ou = true;

  for (var j = 0; j < els.length; j++) {
    var data = els[j].getAttribute("data-ws");

    if (ou) {
      var results = tableau_choix.some(function(element){
        if (!els[j].classList.contains("text") && !els[j].classList.contains("video")) {
          return data.indexOf(element) != -1;
        }
      });
    } else {
      var results = tableau_choix.every(function(element){
        return data.indexOf(element) != -1;
      });
    }

    if (results) {
      set_resultat.add(els[j]);
    }
  }
  return set_resultat;
}

/*
function array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
            arr.push(undefined);
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing
}
*/


var closest = function(el, fn) {
    return el && (fn(el) ? el : closest(el.parentNode, fn));
}


/*
function updateOnAudio(el) {
  var nodes = [];
  figures = document.querySelectorAll(".gallery figure");
  for (var i = 0; i < figures.length; i++) {
    nodes[i] = { "el": figures[i],
                "start": parseInt(figures[i].getAttribute("data-start")),
                "end": parseInt(figures[i].getAttribute("data-end")),
              };
  }

  el.on('timeupdate', event => {
    var time = parseFloat(el.currentTime);
    console.log(time);

    for (var i = 0; i < nodes.length; i++) {
      var node = nodes[i];
      if (time >= node.start && (node.end ? time < node.end : true)) { // si besoin : && (node.end ? time < node.end : true)
        node.el.classList.add("visible");
      } else if (time < node.start || (node.end ? time >= node.end : false)) {
        node.el.classList.remove("visible");
      }
    }
  });
}
*/

function checkboxInteractions() {
  var selectorSelection = ".contrib";
  var selectorContainerInput = ".container_checkbox";
  var selectorContainer = "#container_contrib";
  var data_attr = "data-ws";

  var resetCheckbox = true;
  var operator = "or";
  var showConsole = false;

  var filter = this.value;
  var els = document.querySelectorAll(selectorSelection);
    
  var deja_selected = document.querySelectorAll(selectorContainerInput + ' input:checked');

  if (resetCheckbox) {
    for (var i = 0; i < deja_selected.length; i++) {
      if (this != deja_selected[i]) deja_selected[i].checked = false;
    }
    var deja_selected = document.querySelectorAll(selectorContainerInput + ' input:checked');
  }
  var tableau_choix = [];
  for (var i = 0; i < deja_selected.length; i++) {
    tableau_choix.push(deja_selected[i].value);
  }

  if (showConsole) console.log(tableau_choix);

  // reset 
  var el_deja_selected = document.querySelectorAll(selectorSelection+'.selected');
  for (var i = 0; i < el_deja_selected.length; i++) {
    el_deja_selected[i].classList.remove("selected");
  }
    
  // collection des résultats
  var set_resultat = new Set();

  // opérateur de conditions
  var ou = (operator === "or") ? true : false;

  for (var j = 0; j < els.length; j++) {
    var datas = els[j].getAttribute(data_attr);

    if (ou) {
      var results = tableau_choix.some(function(element){
        return datas.indexOf(element) != -1;
      });
    } else {
      var results = tableau_choix.every(function(element){
        return datas.indexOf(element) != -1;
      });
    }

    if (results) {
      set_resultat.add(els[j]);
    }
  }

  // sélection des résultats front-end
  for (let item of set_resultat) item.classList.add("selected");

  var container = document.querySelector(selectorContainer);
  if (tableau_choix.length) {
    if (showConsole) console.log(set_resultat);
    
    container.classList.add("filtered");

    // si aucun résultats
    if (!set_resultat.size) {
      if (showConsole) console.log("aucun résultats");
    } else {
      if (showConsole) console.log(set_resultat.size + " résultats");
    }
  } else {
    if (showConsole) console.log("remove filtered");
    container.classList.remove("filtered");
  }
}











/*



************ AIDE MÉMOIRE JS ***********

****** AFFICHER UN RÉSULTAT DANS LA CONSOLE ******

console.log();




****** SÉLECTION D'ÉLÉMENT ******

var el = document.getElementById("mon_id");
var el = document.getElementsByClassName("ma_classe")[];
var el = document.getElementsByTagName("ma_balise")[];
var el = document.querySelector("mon_sélecteur");
var el = document.querySelectorAll("mon_sélecteur")[];
var el = document.querySelectorAll("el[data-attr='" + attr + "']");



****** DÉCLARATION DE FONCTION ******

function maFonction() {
  console.log("hello");
}




****** BOUCLE FOR ******

for (i = 0; i < 10; i++) {
  console.log(i);
}




****** CONDITION ******

var condition = true;
if (condition) {
  console.log("vrai");
}

if (condition1) {
  console.log("condition1 : vrai");
} else if (condition2) {
  console.log("condition2 : vrai");
} else {
  console.log("condition1 et condition2 : faux");
}




****** ÉVÉNEMENT ******

element.addEventListener(mon_event, ma_fonction);
element.removeEventListener(mon_event, ma_fonction)

"click" "mousedown" "mousemove" "mouseover" "mouseenter" "mouseleave"
"keypress" "keydown" "keyup"




****** TOUCHES CLAVIER ******

À venir…




****** CRÉATION D'ÉLÉMENT ******

var el = document.createElement('div');
var el = document.createTextNode();

container.appendChild(el);




****** INSERTION D'ÉLÉMENT ******

container.appendChild(el);
container.removeChild(el);




****** ÉLÉMENT : ID ET ATTRIBUT ******

el.id = "mon_id";

el.setAttribute("nom_attribut",  "valeur_attribut");
el.getAttribute("nom_attribut");  




****** ÉLÉMENT : LISTE DE CLASS ******

element.classList.toggle();
el.classList.remove();
el.classList.add();
el.classList.contains();




****** NOMBRE D'ÉLÉMENTS / LONGUEUR D'UN ÉLÉMENT ******

var nb = el.length;




****** STYLE ******

el.style.propriété_css = "valeur";




****** TAILLE DE LA FENETRE ******

var screen_width = window.innerWidth;
var screen_height = window.innerHeight;




****** VIDÉO ******

video.load();
video.pause();
video.play();
video.setAttribute("onended",  "maFonction()");
video.setAttribute("controls",  "true");




****** GESTION DE TABLEAU ******

var tableau = [];
tableau.push();




****** GESTION DE CHAÎNE DE CARACTÈRES ******

el.toLowerCase().split(' ').join('-').replace();




****** ATTENTE ET RÉPÉTITION ******

setTimeout(function(){
  console.log("hello");
}, 3000);

setInterval(maFonction, 1000);




****** OPÉRATEURS ******

== égal à

!= différent de

=== contenu et type égal à

!== contenu ou type différent de

> supérieur à

>= supérieur ou égal à

< inférieur à

<= inférieur ou égal à




****** CONDITIONS ******

&& ET

|| OU

! NON



*/




