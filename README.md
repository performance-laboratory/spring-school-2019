# Spring School 2019

Site Profile pour ProcessWire développé pour la [Spring School 2019](https://www.arts-in-the-alps.com/).



## Installation

- copier le git dans le dossier `site/templates` d'une installation ProcessWire (la faire avec un blank profile)
- importer la db `_spring_school_pw.sql` dans la table dédiée à l'installation
- installer les modules listés ci-dessous (fichiers dans le dossier `site/modules`)
- copier les fichiers (img, vid) dans `site/assets/files`
- dans `site/templates/scripts/` faire `npm install`
- dans `site/config.php` ajouter : `$config->prependTemplateFile = '_init.php';`



## Requirements

- droit d'écriture récursifs sur les dossiers `site/assets` et `site/modules` ([détails ici](https://processwire.com/docs/security/file-permissions/))
- A Unix or Windows-based web server running Apache or compatible
- PHP version 5.4 or newer with PDO database support (PHP 7+ preferable)
- MySQL or MariaDB, 5.0.15 or greater (5.5+ preferable)
- Apache must have mod_rewrite enabled
- Apache must support .htaccess files
- PHP's bundled GD 2 library or ImageMagick library



## Modules

- [Site Profile Exporter 3.0.0](https://modules.processwire.com/modules/process-export-profile/)
- [Video embed for YouTube/Vimeo 1.1.1](https://modules.processwire.com/modules/textformatter-video-embed/)



## License

GNU Affero General Public License v3.0


---


[![Logo SFR](logos/logo-sfr.png "Lien vers le site web de la SFR")](https://www.arts-in-the-alps.com/)

[![Logo Performance Laboratory](logos/logo-pl.jpg "Lien vers le site web du Performance Laboratory")](https://performance.univ-grenoble-alpes.fr/)

[![Logo UGA](logos/logo-uga.jpg "Lien vers le site web de l'UGA")](https://www.univ-grenoble-alpes.fr/)