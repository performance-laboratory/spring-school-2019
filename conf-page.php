<?php include("inc/head.php"); ?>

<?php include("inc/header.php"); ?>
	
	<div class="container">

		<section class="sub-header">
			<p>Conference</p>
			<h2><?= $page->title; ?></h2>
			<h3><?= $page->author; ?></h3>
			<?= $page->body; ?>
		</section>

<?php foreach ($page->sounds as $snd): ?>
	
		<article class="media">
			<audio class="player audio_conf" controls>
				<source src="<?= $snd->url ?>" type="audio/mp3" />
			</audio>
	    </article>
<?php endforeach; ?>
	
	<div class="container gallery">
		<div class="row justify-content-center">
<?php foreach ($page->images as $img): ?>
	    <div class="col-8">
			<figure data-start="<?= $img->start ?>" data-end="<?= $img->end ?>">
				<img src="<?= $img->url ?>">
			</figure>
		</div>
<?php endforeach; ?>
		</div>
	</div>

	</div>

<?php include("inc/foot.php"); ?>

