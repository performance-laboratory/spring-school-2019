<?php

$homepage = $pages->get('/');
$contributions = $pages->find('template=contrib-page');
$workshops = $pages->find('template=workshop-page, sort=sort');
$conferences = $pages->find('template=conf-page, sort=sort');
$events = $pages->get('/events/')->children();

function random(int $min, int $max) {
	return mt_rand($min, $max);
}