-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `caches`;
CREATE TABLE `caches` (
  `name` varchar(250) NOT NULL,
  `data` mediumtext NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`name`),
  KEY `expires` (`expires`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `caches` (`name`, `data`, `expires`) VALUES
('Modules.wire/modules/',	'Textformatter/TextformatterStripTags.module\nTextformatter/TextformatterNewlineUL.module\nTextformatter/TextformatterSmartypants/TextformatterSmartypants.module\nTextformatter/TextformatterNewlineBR.module\nTextformatter/TextformatterMarkdownExtra/TextformatterMarkdownExtra.module\nTextformatter/TextformatterPstripper.module\nTextformatter/TextformatterEntities.module\nProcess/ProcessPagesExportImport/ProcessPagesExportImport.module\nProcess/ProcessPageTrash.module\nProcess/ProcessProfile/ProcessProfile.module\nProcess/ProcessForgotPassword.module\nProcess/ProcessLogger/ProcessLogger.module\nProcess/ProcessPageSort.module\nProcess/ProcessModule/ProcessModule.module\nProcess/ProcessList.module\nProcess/ProcessCommentsManager/ProcessCommentsManager.module\nProcess/ProcessPageAdd/ProcessPageAdd.module\nProcess/ProcessPageView.module\nProcess/ProcessPageType/ProcessPageType.module\nProcess/ProcessPageSearch/ProcessPageSearch.module\nProcess/ProcessRecentPages/ProcessRecentPages.module\nProcess/ProcessPageEdit/ProcessPageEdit.module\nProcess/ProcessRole/ProcessRole.module\nProcess/ProcessLogin/ProcessLogin.module\nProcess/ProcessUser/ProcessUser.module\nProcess/ProcessHome.module\nProcess/ProcessPageList/ProcessPageList.module\nProcess/ProcessPageEditImageSelect/ProcessPageEditImageSelect.module\nProcess/ProcessTemplate/ProcessTemplate.module\nProcess/ProcessPageLister/ProcessPageLister.module\nProcess/ProcessPermission/ProcessPermission.module\nProcess/ProcessPageClone.module\nProcess/ProcessField/ProcessField.module\nProcess/ProcessPageEditLink/ProcessPageEditLink.module\nLanguageSupport/FieldtypePageTitleLanguage.module\nLanguageSupport/LanguageTabs.module\nLanguageSupport/LanguageSupportPageNames.module\nLanguageSupport/FieldtypeTextLanguage.module\nLanguageSupport/LanguageSupportFields.module\nLanguageSupport/ProcessLanguage.module\nLanguageSupport/ProcessLanguageTranslator.module\nLanguageSupport/FieldtypeTextareaLanguage.module\nLanguageSupport/LanguageSupport.module\nInputfield/InputfieldRadios/InputfieldRadios.module\nInputfield/InputfieldPageListSelect/InputfieldPageListSelect.module\nInputfield/InputfieldPageListSelect/InputfieldPageListSelectMultiple.module\nInputfield/InputfieldImage/InputfieldImage.module\nInputfield/InputfieldEmail.module\nInputfield/InputfieldPageTable/InputfieldPageTable.module\nInputfield/InputfieldSelector/InputfieldSelector.module\nInputfield/InputfieldPassword/InputfieldPassword.module\nInputfield/InputfieldToggle/InputfieldToggle.module\nInputfield/InputfieldFieldset.module\nInputfield/InputfieldTextarea.module\nInputfield/InputfieldSelectMultiple.module\nInputfield/InputfieldCheckboxes/InputfieldCheckboxes.module\nInputfield/InputfieldIcon/InputfieldIcon.module\nInputfield/InputfieldFloat.module\nInputfield/InputfieldCheckbox.module\nInputfield/InputfieldURL.module\nInputfield/InputfieldSelect.module\nInputfield/InputfieldSubmit/InputfieldSubmit.module\nInputfield/InputfieldButton.module\nInputfield/InputfieldPageTitle/InputfieldPageTitle.module\nInputfield/InputfieldPageAutocomplete/InputfieldPageAutocomplete.module\nInputfield/InputfieldCKEditor/InputfieldCKEditor.module\nInputfield/InputfieldName.module\nInputfield/InputfieldInteger.module\nInputfield/InputfieldPage/InputfieldPage.module\nInputfield/InputfieldDatetime/InputfieldDatetime.module\nInputfield/InputfieldForm.module\nInputfield/InputfieldHidden.module\nInputfield/InputfieldText.module\nInputfield/InputfieldMarkup.module\nInputfield/InputfieldFile/InputfieldFile.module\nInputfield/InputfieldAsmSelect/InputfieldAsmSelect.module\nInputfield/InputfieldPageName/InputfieldPageName.module\nSession/SessionHandlerDB/ProcessSessionDB.module\nSession/SessionHandlerDB/SessionHandlerDB.module\nSession/SessionLoginThrottle/SessionLoginThrottle.module\nMarkup/MarkupAdminDataTable/MarkupAdminDataTable.module\nMarkup/MarkupHTMLPurifier/MarkupHTMLPurifier.module\nMarkup/MarkupPageArray.module\nMarkup/MarkupCache.module\nMarkup/MarkupRSS.module\nMarkup/MarkupPagerNav/MarkupPagerNav.module\nMarkup/MarkupPageFields.module\nFieldtype/FieldtypeCheckbox.module\nFieldtype/FieldtypeToggle.module\nFieldtype/FieldtypeFile.module\nFieldtype/FieldtypeText.module\nFieldtype/FieldtypeTextarea.module\nFieldtype/FieldtypeFieldsetOpen.module\nFieldtype/FieldtypePageTitle.module\nFieldtype/FieldtypePassword.module\nFieldtype/FieldtypeImage.module\nFieldtype/FieldtypeSelector.module\nFieldtype/FieldtypeFieldsetTabOpen.module\nFieldtype/FieldtypeComments/CommentFilterAkismet.module\nFieldtype/FieldtypeComments/FieldtypeComments.module\nFieldtype/FieldtypeComments/InputfieldCommentsAdmin.module\nFieldtype/FieldtypePage.module\nFieldtype/FieldtypePageTable.module\nFieldtype/FieldtypeFloat.module\nFieldtype/FieldtypeInteger.module\nFieldtype/FieldtypeEmail.module\nFieldtype/FieldtypeURL.module\nFieldtype/FieldtypeDatetime.module\nFieldtype/FieldtypeFieldsetClose.module\nFieldtype/FieldtypeOptions/FieldtypeOptions.module\nFieldtype/FieldtypeRepeater/InputfieldRepeater.module\nFieldtype/FieldtypeRepeater/FieldtypeFieldsetPage.module\nFieldtype/FieldtypeRepeater/FieldtypeRepeater.module\nFieldtype/FieldtypeModule.module\nFieldtype/FieldtypeCache.module\nPagePathHistory.module\nAdminTheme/AdminThemeUikit/AdminThemeUikit.module\nAdminTheme/AdminThemeDefault/AdminThemeDefault.module\nAdminTheme/AdminThemeReno/AdminThemeReno.module\nSystem/SystemNotifications/SystemNotifications.module\nSystem/SystemNotifications/FieldtypeNotifications.module\nSystem/SystemUpdater/SystemUpdater.module\nImage/ImageSizerEngineIMagick/ImageSizerEngineIMagick.module\nImage/ImageSizerEngineAnimatedGif/ImageSizerEngineAnimatedGif.module\nPagePaths.module\nPage/PageFrontEdit/PageFrontEdit.module\nFileCompilerTags.module\nPageRender.module\nLazyCron.module\nJquery/JqueryTableSorter/JqueryTableSorter.module\nJquery/JqueryCore/JqueryCore.module\nJquery/JqueryUI/JqueryUI.module\nJquery/JqueryMagnific/JqueryMagnific.module\nJquery/JqueryWireTabs/JqueryWireTabs.module\nPagePermissions.module',	'2010-04-08 03:10:01'),
('ModulesUninstalled.info',	'{\"TextformatterStripTags\":{\"name\":\"TextformatterStripTags\",\"title\":\"Strip Markup Tags\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Strips HTML\\/XHTML Markup Tags\",\"created\":1576506090,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterNewlineUL\":{\"name\":\"TextformatterNewlineUL\",\"title\":\"Newlines to Unordered List\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Converts newlines to <li> list items and surrounds in an <ul> unordered list. \",\"created\":1576506090,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterSmartypants\":{\"name\":\"TextformatterSmartypants\",\"title\":\"SmartyPants Typographer\",\"version\":171,\"versionStr\":\"1.7.1\",\"summary\":\"Smart typography for web sites, by Michel Fortin based on SmartyPants by John Gruber. If combined with Markdown, it should be applied AFTER Markdown.\",\"created\":1576506127,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"url\":\"https:\\/\\/github.com\\/michelf\\/php-smartypants\"},\"TextformatterNewlineBR\":{\"name\":\"TextformatterNewlineBR\",\"title\":\"Newlines to XHTML Line Breaks\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Converts newlines to XHTML line break <br \\/> tags. \",\"created\":1576506090,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterMarkdownExtra\":{\"name\":\"TextformatterMarkdownExtra\",\"title\":\"Markdown\\/Parsedown Extra\",\"version\":130,\"versionStr\":\"1.3.0\",\"summary\":\"Markdown\\/Parsedown extra lightweight markup language by Emanuil Rusev. Based on Markdown by John Gruber.\",\"created\":1576506127,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"TextformatterPstripper\":{\"name\":\"TextformatterPstripper\",\"title\":\"Paragraph Stripper\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Strips paragraph <p> tags that may have been applied by other text formatters before it. \",\"created\":1576506090,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessPagesExportImport\":{\"name\":\"ProcessPagesExportImport\",\"title\":\"Pages Export\\/Import\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Ryan Cramer\",\"summary\":\"Enables exporting and importing of pages. Development version, not yet recommended for production use.\",\"icon\":\"paper-plane-o\",\"permission\":\"page-edit-export\",\"created\":1576506120,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"export-import\",\"parent\":\"page\",\"title\":\"Export\\/Import\"}},\"ProcessForgotPassword\":{\"name\":\"ProcessForgotPassword\",\"title\":\"Forgot Password\",\"version\":103,\"versionStr\":\"1.0.3\",\"summary\":\"Provides password reset\\/email capability for the Login process.\",\"permission\":\"page-view\",\"created\":1576506090,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ProcessCommentsManager\":{\"name\":\"ProcessCommentsManager\",\"title\":\"Comments\",\"version\":8,\"versionStr\":\"0.0.8\",\"author\":\"Ryan Cramer\",\"summary\":\"Manage comments in your site outside of the page editor.\",\"icon\":\"comments\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"permission\":\"comments-manager\",\"permissions\":{\"comments-manager\":\"Use the comments manager\"},\"created\":1576506113,\"installed\":false,\"searchable\":\"comments\",\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"comments\",\"parent\":\"setup\",\"title\":\"Comments\"},\"nav\":[{\"url\":\"?go=approved\",\"label\":\"Approved\"},{\"url\":\"?go=pending\",\"label\":\"Pending\"},{\"url\":\"?go=spam\",\"label\":\"Spam\"},{\"url\":\"?go=all\",\"label\":\"All\"}]},\"ProcessPageClone\":{\"name\":\"ProcessPageClone\",\"title\":\"Page Clone\",\"version\":104,\"versionStr\":\"1.0.4\",\"summary\":\"Provides ability to clone\\/copy\\/duplicate pages in the admin. Adds a &quot;copy&quot; option to all applicable pages in the PageList.\",\"permission\":\"page-clone\",\"permissions\":{\"page-clone\":\"Clone a page\",\"page-clone-tree\":\"Clone a tree of pages\"},\"autoload\":\"template=admin\",\"created\":1576506090,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"page\":{\"name\":\"clone\",\"title\":\"Clone\",\"parent\":\"page\",\"status\":1024}},\"InputfieldPageTable\":{\"name\":\"InputfieldPageTable\",\"title\":\"ProFields: Page Table\",\"version\":13,\"versionStr\":\"0.1.3\",\"summary\":\"Inputfield to accompany FieldtypePageTable\",\"requiresVersions\":{\"FieldtypePageTable\":[\">=\",0]},\"created\":1576506104,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldToggle\":{\"name\":\"InputfieldToggle\",\"title\":\"Toggle\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"A toggle providing similar input capability to a checkbox but much more configurable.\",\"created\":1576506107,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"MarkupCache\":{\"name\":\"MarkupCache\",\"title\":\"Markup Cache\",\"version\":101,\"versionStr\":\"1.0.1\",\"summary\":\"A simple way to cache segments of markup in your templates. \",\"href\":\"https:\\/\\/processwire.com\\/api\\/modules\\/markupcache\\/\",\"autoload\":true,\"singular\":true,\"created\":1576506090,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"MarkupRSS\":{\"name\":\"MarkupRSS\",\"title\":\"Markup RSS Feed\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Renders an RSS feed. Given a PageArray, renders an RSS feed of them.\",\"created\":1576506090,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"MarkupPageFields\":{\"name\":\"MarkupPageFields\",\"title\":\"Markup Page Fields\",\"version\":100,\"versionStr\":\"1.0.0\",\"summary\":\"Adds $page->renderFields() and $page->images->render() methods that return basic markup for output during development and debugging.\",\"autoload\":true,\"singular\":true,\"created\":1576506090,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"permanent\":true},\"FieldtypeToggle\":{\"name\":\"FieldtypeToggle\",\"title\":\"Toggle (Yes\\/No)\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Configurable yes\\/no, on\\/off toggle alternative to a checkbox, plus optional \\u201cother\\u201d option.\",\"requiresVersions\":{\"InputfieldToggle\":[\">=\",0]},\"created\":1576506088,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeSelector\":{\"name\":\"FieldtypeSelector\",\"title\":\"Selector\",\"version\":13,\"versionStr\":\"0.1.3\",\"author\":\"Avoine + ProcessWire\",\"summary\":\"Build a page finding selector visually.\",\"created\":1576506088,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"CommentFilterAkismet\":{\"name\":\"CommentFilterAkismet\",\"title\":\"Comment Filter: Akismet\",\"version\":200,\"versionStr\":\"2.0.0\",\"summary\":\"Uses the Akismet service to identify comment spam. Module plugin for the Comments Fieldtype.\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"created\":1576506094,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeComments\":{\"name\":\"FieldtypeComments\",\"title\":\"Comments\",\"version\":107,\"versionStr\":\"1.0.7\",\"summary\":\"Field that stores user posted comments for a single Page\",\"installs\":[\"InputfieldCommentsAdmin\"],\"created\":1576506095,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldCommentsAdmin\":{\"name\":\"InputfieldCommentsAdmin\",\"title\":\"Comments Admin\",\"version\":104,\"versionStr\":\"1.0.4\",\"summary\":\"Provides an administrative interface for working with comments\",\"requiresVersions\":{\"FieldtypeComments\":[\">=\",0]},\"created\":1576506095,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypePageTable\":{\"name\":\"FieldtypePageTable\",\"title\":\"ProFields: Page Table\",\"version\":8,\"versionStr\":\"0.0.8\",\"summary\":\"A fieldtype containing a group of editable pages.\",\"installs\":[\"InputfieldPageTable\"],\"autoload\":true,\"created\":1576506088,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeOptions\":{\"name\":\"FieldtypeOptions\",\"title\":\"Select Options\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Field that stores single and multi select options.\",\"created\":1576506095,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"InputfieldRepeater\":{\"name\":\"InputfieldRepeater\",\"title\":\"Repeater\",\"version\":106,\"versionStr\":\"1.0.6\",\"summary\":\"Repeats fields from another template. Provides the input for FieldtypeRepeater.\",\"requiresVersions\":{\"FieldtypeRepeater\":[\">=\",0]},\"created\":1576506096,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeFieldsetPage\":{\"name\":\"FieldtypeFieldsetPage\",\"title\":\"Fieldset (Page)\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Fieldset with fields isolated to separate namespace (page), enabling re-use of fields.\",\"requiresVersions\":{\"FieldtypeRepeater\":[\">=\",0]},\"autoload\":true,\"created\":1576506096,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeRepeater\":{\"name\":\"FieldtypeRepeater\",\"title\":\"Repeater\",\"version\":106,\"versionStr\":\"1.0.6\",\"summary\":\"Maintains a collection of fields that are repeated for any number of times.\",\"installs\":[\"InputfieldRepeater\"],\"autoload\":true,\"created\":1576506096,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeCache\":{\"name\":\"FieldtypeCache\",\"title\":\"Cache\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Caches the values of other fields for fewer runtime queries. Can also be used to combine multiple text fields and have them all be searchable under the cached field name.\",\"created\":1576506087,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"PagePathHistory\":{\"name\":\"PagePathHistory\",\"title\":\"Page Path History\",\"version\":5,\"versionStr\":\"0.0.5\",\"summary\":\"Keeps track of past URLs where pages have lived and automatically redirects (301 permament) to the new location whenever the past URL is accessed.\",\"autoload\":true,\"singular\":true,\"created\":1576506086,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"AdminThemeReno\":{\"name\":\"AdminThemeReno\",\"title\":\"Reno\",\"version\":17,\"versionStr\":\"0.1.7\",\"author\":\"Tom Reno (Renobird)\",\"summary\":\"Admin theme for ProcessWire 2.5+ by Tom Reno (Renobird)\",\"requiresVersions\":{\"AdminThemeDefault\":[\">=\",0]},\"autoload\":\"template=admin\",\"created\":1576506092,\"installed\":false,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"SystemNotifications\":{\"name\":\"SystemNotifications\",\"title\":\"System Notifications\",\"version\":12,\"versionStr\":\"0.1.2\",\"summary\":\"Adds support for notifications in ProcessWire (currently in development)\",\"icon\":\"bell\",\"installs\":[\"FieldtypeNotifications\"],\"autoload\":true,\"created\":1576506125,\"installed\":false,\"configurable\":\"SystemNotificationsConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"FieldtypeNotifications\":{\"name\":\"FieldtypeNotifications\",\"title\":\"Notifications\",\"version\":4,\"versionStr\":\"0.0.4\",\"summary\":\"Field that stores user notifications.\",\"requiresVersions\":{\"SystemNotifications\":[\">=\",0]},\"created\":1576506125,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ImageSizerEngineIMagick\":{\"name\":\"ImageSizerEngineIMagick\",\"title\":\"IMagick Image Sizer\",\"version\":3,\"versionStr\":\"0.0.3\",\"author\":\"Horst Nogajski\",\"summary\":\"Upgrades image manipulations to use PHP\'s ImageMagick library when possible.\",\"created\":1576506096,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"ImageSizerEngineAnimatedGif\":{\"name\":\"ImageSizerEngineAnimatedGif\",\"title\":\"Animated GIF Image Sizer\",\"version\":1,\"versionStr\":\"0.0.1\",\"author\":\"Horst Nogajski\",\"summary\":\"Upgrades image manipulations for animated GIFs.\",\"created\":1576506096,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"PagePaths\":{\"name\":\"PagePaths\",\"title\":\"Page Paths\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Enables page paths\\/urls to be queryable by selectors. Also offers potential for improved load performance. Builds an index at install (may take time on a large site). Currently supports only single languages sites.\",\"autoload\":true,\"singular\":true,\"created\":1576506086,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"PageFrontEdit\":{\"name\":\"PageFrontEdit\",\"title\":\"Front-End Page Editor\",\"version\":3,\"versionStr\":\"0.0.3\",\"author\":\"Ryan Cramer\",\"summary\":\"Enables front-end editing of page fields.\",\"icon\":\"cube\",\"permissions\":{\"page-edit-front\":\"Use the front-end page editor\"},\"autoload\":true,\"created\":1576506113,\"installed\":false,\"configurable\":\"PageFrontEditConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"core\":true,\"license\":\"MPL 2.0\"},\"FileCompilerTags\":{\"name\":\"FileCompilerTags\",\"title\":\"Tags File Compiler\",\"version\":1,\"versionStr\":\"0.0.1\",\"summary\":\"Enables {var} or {var.property} variables in markup sections of a file. Can be used with any API variable.\",\"created\":1576506086,\"installed\":false,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"core\":true},\"LazyCron\":{\"name\":\"LazyCron\",\"title\":\"Lazy Cron\",\"version\":102,\"versionStr\":\"1.0.2\",\"summary\":\"Provides hooks that are automatically executed at various intervals. It is called \'lazy\' because it\'s triggered by a pageview, so the interval is guaranteed to be at least the time requested, rather than exactly the time requested. This is fine for most cases, but you can make it not lazy by connecting this to a real CRON job. See the module file for details. \",\"href\":\"https:\\/\\/processwire.com\\/api\\/modules\\/lazy-cron\\/\",\"autoload\":true,\"singular\":true,\"created\":1576506086,\"installed\":false,\"namespace\":\"ProcessWire\\\\\",\"core\":true}}',	'2010-04-08 03:10:01'),
('ModulesVerbose.info',	'{\"61\":{\"summary\":\"Entity encode ampersands, quotes (single and double) and greater-than\\/less-than signs using htmlspecialchars(str, ENT_QUOTES). It is recommended that you use this on all text\\/textarea fields except those using a rich text editor or a markup language like Markdown.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"109\":{\"summary\":\"Handles emptying of Page trash\",\"core\":true,\"versionStr\":\"1.0.3\"},\"138\":{\"summary\":\"Enables user to change their password, email address and other settings that you define.\",\"core\":true,\"versionStr\":\"1.0.4\"},\"161\":{\"summary\":\"View and manage system logs.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.0.2\",\"permissions\":{\"logs-view\":\"Can view system logs\",\"logs-edit\":\"Can manage system logs\"},\"page\":{\"name\":\"logs\",\"parent\":\"setup\",\"title\":\"Logs\"}},\"14\":{\"summary\":\"Handles page sorting and moving for PageList\",\"core\":true,\"versionStr\":\"1.0.0\"},\"50\":{\"summary\":\"List, edit or install\\/uninstall modules\",\"core\":true,\"versionStr\":\"1.1.8\"},\"76\":{\"summary\":\"Lists the Process assigned to each child page of the current\",\"core\":true,\"versionStr\":\"1.0.1\"},\"17\":{\"summary\":\"Add a new page\",\"core\":true,\"versionStr\":\"1.0.8\"},\"83\":{\"summary\":\"All page views are routed through this Process\",\"core\":true,\"versionStr\":\"1.0.4\"},\"134\":{\"summary\":\"List, Edit and Add pages of a specific type\",\"core\":true,\"versionStr\":\"1.0.1\"},\"104\":{\"summary\":\"Provides a page search engine for admin use.\",\"core\":true,\"versionStr\":\"1.0.6\"},\"159\":{\"summary\":\"Shows a list of recently edited pages in your admin.\",\"author\":\"Ryan Cramer\",\"href\":\"http:\\/\\/modules.processwire.com\\/\",\"core\":true,\"versionStr\":\"0.0.2\",\"permissions\":{\"page-edit-recent\":\"Can see recently edited pages\"},\"page\":{\"name\":\"recent-pages\",\"parent\":\"page\",\"title\":\"Recent\"}},\"7\":{\"summary\":\"Edit a Page\",\"core\":true,\"versionStr\":\"1.0.9\"},\"68\":{\"summary\":\"Manage user roles and what permissions are attached\",\"core\":true,\"versionStr\":\"1.0.4\"},\"10\":{\"summary\":\"Login to ProcessWire\",\"core\":true,\"versionStr\":\"1.0.7\"},\"66\":{\"summary\":\"Manage system users\",\"core\":true,\"versionStr\":\"1.0.7\",\"searchable\":\"users\"},\"87\":{\"summary\":\"Acts as a placeholder Process for the admin root. Ensures proper flow control after login.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"12\":{\"summary\":\"List pages in a hierarchical tree structure\",\"core\":true,\"versionStr\":\"1.2.2\"},\"129\":{\"summary\":\"Provides image manipulation functions for image fields and rich text editors.\",\"core\":true,\"versionStr\":\"1.2.0\"},\"47\":{\"summary\":\"List and edit the templates that control page output\",\"core\":true,\"versionStr\":\"1.1.4\",\"searchable\":\"templates\"},\"150\":{\"summary\":\"Admin tool for finding and listing pages by any property.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.2.6\",\"permissions\":{\"page-lister\":\"Use Page Lister\"}},\"136\":{\"summary\":\"Manage system permissions\",\"core\":true,\"versionStr\":\"1.0.1\"},\"48\":{\"summary\":\"Edit individual fields that hold page data\",\"core\":true,\"versionStr\":\"1.1.3\",\"searchable\":\"fields\"},\"121\":{\"summary\":\"Provides a link capability as used by some Fieldtype modules (like rich text editors).\",\"core\":true,\"versionStr\":\"1.0.8\"},\"168\":{\"summary\":\"Field that stores a page title in multiple languages. Use this only if you want title inputs created for ALL languages on ALL pages. Otherwise create separate languaged-named title fields, i.e. title_fr, title_es, title_fi, etc. \",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.0\"},\"167\":{\"summary\":\"Field that stores a single line of text in multiple languages\",\"core\":true,\"versionStr\":\"1.0.0\"},\"164\":{\"summary\":\"Manage system languages\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.3\",\"permissions\":{\"lang-edit\":\"Administer languages and static translation files\"}},\"165\":{\"summary\":\"Provides language translation capabilities for ProcessWire core and modules.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.1\"},\"169\":{\"summary\":\"Field that stores a multiple lines of text in multiple languages\",\"core\":true,\"versionStr\":\"1.0.0\"},\"163\":{\"summary\":\"ProcessWire multi-language support.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.3\"},\"172\":{\"summary\":\"Organizes multi-language fields into tabs for a cleaner easier to use interface.\",\"author\":\"adamspruijt, ryan\",\"core\":true,\"versionStr\":\"1.1.4\"},\"166\":{\"summary\":\"Required to use multi-language fields.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"1.0.0\"},\"171\":{\"summary\":\"Required to use multi-language page names.\",\"author\":\"Ryan Cramer\",\"core\":true,\"versionStr\":\"0.1.0\"},\"39\":{\"summary\":\"Radio buttons for selection of a single item\",\"core\":true,\"versionStr\":\"1.0.5\"},\"15\":{\"summary\":\"Selection of a single page from a ProcessWire page tree list\",\"core\":true,\"versionStr\":\"1.0.1\"},\"137\":{\"summary\":\"Selection of multiple pages from a ProcessWire page tree list\",\"core\":true,\"versionStr\":\"1.0.2\"},\"56\":{\"summary\":\"One or more image uploads (sortable)\",\"core\":true,\"versionStr\":\"1.2.3\"},\"80\":{\"summary\":\"E-Mail address in valid format\",\"core\":true,\"versionStr\":\"1.0.1\"},\"149\":{\"summary\":\"Build a page finding selector visually.\",\"author\":\"Avoine + ProcessWire\",\"core\":true,\"versionStr\":\"0.2.8\"},\"122\":{\"summary\":\"Password input with confirmation field that doesn&#039;t ever echo the input back.\",\"core\":true,\"versionStr\":\"1.0.2\"},\"78\":{\"summary\":\"Groups one or more fields together in a container\",\"core\":true,\"versionStr\":\"1.0.1\"},\"35\":{\"summary\":\"Multiple lines of text\",\"core\":true,\"versionStr\":\"1.0.3\"},\"43\":{\"summary\":\"Select multiple items from a list\",\"core\":true,\"versionStr\":\"1.0.1\"},\"38\":{\"summary\":\"Multiple checkbox toggles\",\"core\":true,\"versionStr\":\"1.0.7\"},\"162\":{\"summary\":\"Select an icon\",\"core\":true,\"versionStr\":\"0.0.2\"},\"90\":{\"summary\":\"Floating point number with precision\",\"core\":true,\"versionStr\":\"1.0.3\"},\"37\":{\"summary\":\"Single checkbox toggle\",\"core\":true,\"versionStr\":\"1.0.6\"},\"108\":{\"summary\":\"URL in valid format\",\"core\":true,\"versionStr\":\"1.0.2\"},\"36\":{\"summary\":\"Selection of a single value from a select pulldown\",\"core\":true,\"versionStr\":\"1.0.2\"},\"32\":{\"summary\":\"Form submit button\",\"core\":true,\"versionStr\":\"1.0.2\"},\"131\":{\"summary\":\"Form button element that you can optionally pass an href attribute to.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"112\":{\"summary\":\"Handles input of Page Title and auto-generation of Page Name (when name is blank)\",\"core\":true,\"versionStr\":\"1.0.2\"},\"178\":{\"summary\":\"Multiple Page selection using auto completion and sorting capability. Intended for use as an input field for Page reference fields.\",\"core\":true,\"versionStr\":\"1.1.2\"},\"155\":{\"summary\":\"CKEditor textarea rich text editor.\",\"core\":true,\"versionStr\":\"1.6.2\"},\"41\":{\"summary\":\"Text input validated as a ProcessWire name field\",\"core\":true,\"versionStr\":\"1.0.0\"},\"85\":{\"summary\":\"Integer (positive or negative)\",\"core\":true,\"versionStr\":\"1.0.4\"},\"60\":{\"summary\":\"Select one or more pages\",\"core\":true,\"versionStr\":\"1.0.7\"},\"94\":{\"summary\":\"Inputfield that accepts date and optionally time\",\"core\":true,\"versionStr\":\"1.0.6\"},\"30\":{\"summary\":\"Contains one or more fields in a form\",\"core\":true,\"versionStr\":\"1.0.7\"},\"40\":{\"summary\":\"Hidden value in a form\",\"core\":true,\"versionStr\":\"1.0.1\"},\"34\":{\"summary\":\"Single line of text\",\"core\":true,\"versionStr\":\"1.0.6\"},\"79\":{\"summary\":\"Contains any other markup and optionally child Inputfields\",\"core\":true,\"versionStr\":\"1.0.2\"},\"55\":{\"summary\":\"One or more file uploads (sortable)\",\"core\":true,\"versionStr\":\"1.2.6\"},\"25\":{\"summary\":\"Multiple selection, progressive enhancement to select multiple\",\"core\":true,\"versionStr\":\"2.0.2\"},\"86\":{\"summary\":\"Text input validated as a ProcessWire Page name field\",\"core\":true,\"versionStr\":\"1.0.6\"},\"125\":{\"summary\":\"Throttles login attempts to help prevent dictionary attacks.\",\"core\":true,\"versionStr\":\"1.0.3\"},\"67\":{\"summary\":\"Generates markup for data tables used by ProcessWire admin\",\"core\":true,\"versionStr\":\"1.0.7\"},\"156\":{\"summary\":\"Front-end to the HTML Purifier library.\",\"core\":true,\"versionStr\":\"4.9.5\"},\"113\":{\"summary\":\"Adds renderPager() method to all PaginatedArray types, for easy pagination output. Plus a render() method to PageArray instances.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"98\":{\"summary\":\"Generates markup for pagination navigation\",\"core\":true,\"versionStr\":\"1.0.5\"},\"97\":{\"summary\":\"This Fieldtype stores an ON\\/OFF toggle via a single checkbox. The ON value is 1 and OFF value is 0.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"6\":{\"summary\":\"Field that stores one or more files\",\"core\":true,\"versionStr\":\"1.0.6\"},\"3\":{\"summary\":\"Field that stores a single line of text\",\"core\":true,\"versionStr\":\"1.0.1\"},\"1\":{\"summary\":\"Field that stores multiple lines of text\",\"core\":true,\"versionStr\":\"1.0.7\"},\"105\":{\"summary\":\"Open a fieldset to group fields. Should be followed by a Fieldset (Close) after one or more fields.\",\"core\":true,\"versionStr\":\"1.0.1\"},\"111\":{\"summary\":\"Field that stores a page title\",\"core\":true,\"versionStr\":\"1.0.0\"},\"133\":{\"summary\":\"Field that stores a hashed and salted password\",\"core\":true,\"versionStr\":\"1.0.1\"},\"57\":{\"summary\":\"Field that stores one or more GIF, JPG, or PNG images\",\"core\":true,\"versionStr\":\"1.0.1\"},\"107\":{\"summary\":\"Open a fieldset to group fields. Same as Fieldset (Open) except that it displays in a tab instead.\",\"core\":true,\"versionStr\":\"1.0.0\"},\"4\":{\"summary\":\"Field that stores one or more references to ProcessWire pages\",\"core\":true,\"versionStr\":\"1.0.5\"},\"89\":{\"summary\":\"Field that stores a floating point (decimal) number\",\"core\":true,\"versionStr\":\"1.0.5\"},\"84\":{\"summary\":\"Field that stores an integer\",\"core\":true,\"versionStr\":\"1.0.1\"},\"29\":{\"summary\":\"Field that stores an e-mail address\",\"core\":true,\"versionStr\":\"1.0.0\"},\"135\":{\"summary\":\"Field that stores a URL\",\"core\":true,\"versionStr\":\"1.0.1\"},\"28\":{\"summary\":\"Field that stores a date and optionally time\",\"core\":true,\"versionStr\":\"1.0.5\"},\"106\":{\"summary\":\"Close a fieldset opened by FieldsetOpen. \",\"core\":true,\"versionStr\":\"1.0.0\"},\"27\":{\"summary\":\"Field that stores a reference to another module\",\"core\":true,\"versionStr\":\"1.0.1\"},\"160\":{\"summary\":\"Uikit v3 admin theme\",\"core\":true,\"versionStr\":\"0.3.0\"},\"148\":{\"summary\":\"Minimal admin theme that supports all ProcessWire features.\",\"core\":true,\"versionStr\":\"0.1.4\"},\"139\":{\"summary\":\"Manages system versions and upgrades.\",\"core\":true,\"versionStr\":\"0.1.7\"},\"115\":{\"summary\":\"Adds a render method to Page and caches page output.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"103\":{\"summary\":\"Provides a jQuery plugin for sorting tables.\",\"href\":\"http:\\/\\/mottie.github.io\\/tablesorter\\/\",\"core\":true,\"versionStr\":\"2.2.1\"},\"116\":{\"summary\":\"jQuery Core as required by ProcessWire Admin and plugins\",\"href\":\"http:\\/\\/jquery.com\",\"core\":true,\"versionStr\":\"1.8.3\"},\"117\":{\"summary\":\"jQuery UI as required by ProcessWire and plugins\",\"href\":\"http:\\/\\/ui.jquery.com\",\"core\":true,\"versionStr\":\"1.9.6\"},\"151\":{\"summary\":\"Provides lightbox capability for image galleries. Replacement for FancyBox. Uses Magnific Popup by @dimsemenov.\",\"href\":\"http:\\/\\/dimsemenov.com\\/plugins\\/magnific-popup\\/\",\"core\":true,\"versionStr\":\"0.0.1\"},\"45\":{\"summary\":\"Provides a jQuery plugin for generating tabs in ProcessWire.\",\"core\":true,\"versionStr\":\"1.0.9\"},\"114\":{\"summary\":\"Adds various permission methods to Page objects that are used by Process modules.\",\"core\":true,\"versionStr\":\"1.0.5\"},\"173\":{\"summary\":\"Adds custom fields to image fields (including multi-language support)\",\"href\":\"https:\\/\\/github.com\\/justonestep\\/processwire-imageextra\",\"versionStr\":\"1.0.6\"},\"179\":{\"summary\":\"Enter a full YouTube or Vimeo URL by itself in any paragraph (example: http:\\/\\/www.youtube.com\\/watch?v=Wl4XiYadV_k) and this will automatically convert it to an embedded video. This formatter is intended to be run on trusted input. Recommended for use with TinyMCE textarea fields.\",\"author\":\"Ryan Cramer\",\"href\":\"http:\\/\\/modules.processwire.com\\/modules\\/textformatter-video-embed\\/\",\"versionStr\":\"1.1.1\"},\"180\":{\"summary\":\"Installing this module makes ProcessWire store sessions in the database rather than the file system. Note that this module will log you out after install or uninstall.\",\"core\":true,\"versionStr\":\"0.0.5\"},\"181\":{\"summary\":\"Enables you to browse active database sessions.\",\"core\":true,\"versionStr\":\"0.0.3\"}}',	'2010-04-08 03:10:01'),
('Permissions.names',	'{\"lang-edit\":1015,\"logs-edit\":1014,\"logs-view\":1013,\"page-delete\":34,\"page-edit\":32,\"page-edit-recent\":1011,\"page-lister\":1006,\"page-lock\":54,\"page-move\":35,\"page-sort\":50,\"page-template\":51,\"page-view\":36,\"profile-edit\":53,\"user-admin\":52}',	'2010-04-08 03:10:10'),
('Modules.site/modules/',	'ImageExtra/ImageExtra.module\nTextformatterVideoEmbed/TextformatterVideoEmbed.module',	'2010-04-08 03:10:01'),
('FileCompiler__535cdfacdd152773559d44235c673e73',	'{\"source\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/modules\\/TextformatterVideoEmbed\\/TextformatterVideoEmbed.module\",\"hash\":\"a4a8e7032d9f3fbc21399e44d34a27e4\",\"size\":9327,\"time\":1576506586,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/modules\\/TextformatterVideoEmbed\\/TextformatterVideoEmbed.module\",\"hash\":\"f9b73f5e8f93e8dc62c7cb96c6de73e0\",\"size\":9757,\"time\":1576506586}}',	'2010-04-08 03:10:10'),
('Modules.info',	'{\"61\":{\"name\":\"TextformatterEntities\",\"title\":\"HTML Entity Encoder (htmlspecialchars)\",\"version\":100,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\"},\"109\":{\"name\":\"ProcessPageTrash\",\"title\":\"Page Trash\",\"version\":103,\"singular\":1,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"138\":{\"name\":\"ProcessProfile\",\"title\":\"User Profile\",\"version\":104,\"permission\":\"profile-edit\",\"singular\":1,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"161\":{\"name\":\"ProcessLogger\",\"title\":\"Logs\",\"version\":2,\"icon\":\"tree\",\"permission\":\"logs-view\",\"singular\":1,\"created\":1569310790,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true},\"14\":{\"name\":\"ProcessPageSort\",\"title\":\"Page Sort and Move\",\"version\":100,\"permission\":\"page-edit\",\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"50\":{\"name\":\"ProcessModule\",\"title\":\"Modules\",\"version\":118,\"permission\":\"module-admin\",\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"nav\":[{\"url\":\"?site#tab_site_modules\",\"label\":\"Site\",\"icon\":\"plug\",\"navJSON\":\"navJSON\\/?site=1\"},{\"url\":\"?core#tab_core_modules\",\"label\":\"Core\",\"icon\":\"plug\",\"navJSON\":\"navJSON\\/?core=1\"},{\"url\":\"?configurable#tab_configurable_modules\",\"label\":\"Configure\",\"icon\":\"gear\",\"navJSON\":\"navJSON\\/?configurable=1\"},{\"url\":\"?install#tab_install_modules\",\"label\":\"Install\",\"icon\":\"sign-in\",\"navJSON\":\"navJSON\\/?install=1\"},{\"url\":\"?new#tab_new_modules\",\"label\":\"New\",\"icon\":\"plus\"},{\"url\":\"?reset=1\",\"label\":\"Refresh\",\"icon\":\"refresh\"}]},\"76\":{\"name\":\"ProcessList\",\"title\":\"List\",\"version\":101,\"permission\":\"page-view\",\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"17\":{\"name\":\"ProcessPageAdd\",\"title\":\"Page Add\",\"version\":108,\"icon\":\"plus-circle\",\"permission\":\"page-edit\",\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"83\":{\"name\":\"ProcessPageView\",\"title\":\"Page View\",\"version\":104,\"permission\":\"page-view\",\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"134\":{\"name\":\"ProcessPageType\",\"title\":\"Page Type\",\"version\":101,\"singular\":1,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"104\":{\"name\":\"ProcessPageSearch\",\"title\":\"Page Search\",\"version\":106,\"permission\":\"page-edit\",\"singular\":1,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"159\":{\"name\":\"ProcessRecentPages\",\"title\":\"Recent Pages\",\"version\":2,\"icon\":\"clock-o\",\"permission\":\"page-edit-recent\",\"singular\":1,\"created\":1569310783,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true,\"nav\":[{\"url\":\"?edited=1\",\"label\":\"Edited\",\"icon\":\"users\",\"navJSON\":\"navJSON\\/?edited=1\"},{\"url\":\"?added=1\",\"label\":\"Created\",\"icon\":\"users\",\"navJSON\":\"navJSON\\/?added=1\"},{\"url\":\"?edited=1&me=1\",\"label\":\"Edited by me\",\"icon\":\"user\",\"navJSON\":\"navJSON\\/?edited=1&me=1\"},{\"url\":\"?added=1&me=1\",\"label\":\"Created by me\",\"icon\":\"user\",\"navJSON\":\"navJSON\\/?added=1&me=1\"},{\"url\":\"another\\/\",\"label\":\"Add another\",\"icon\":\"plus-circle\",\"navJSON\":\"anotherNavJSON\\/\"}]},\"7\":{\"name\":\"ProcessPageEdit\",\"title\":\"Page Edit\",\"version\":109,\"icon\":\"edit\",\"permission\":\"page-edit\",\"singular\":1,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"68\":{\"name\":\"ProcessRole\",\"title\":\"Roles\",\"version\":104,\"icon\":\"gears\",\"permission\":\"role-admin\",\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"10\":{\"name\":\"ProcessLogin\",\"title\":\"Login\",\"version\":107,\"permission\":\"page-view\",\"created\":1569310763,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"66\":{\"name\":\"ProcessUser\",\"title\":\"Users\",\"version\":107,\"icon\":\"group\",\"permission\":\"user-admin\",\"created\":1569310763,\"configurable\":\"ProcessUserConfig.php\",\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"87\":{\"name\":\"ProcessHome\",\"title\":\"Admin Home\",\"version\":101,\"permission\":\"page-view\",\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"12\":{\"name\":\"ProcessPageList\",\"title\":\"Page List\",\"version\":122,\"icon\":\"sitemap\",\"permission\":\"page-edit\",\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"129\":{\"name\":\"ProcessPageEditImageSelect\",\"title\":\"Page Edit Image\",\"version\":120,\"permission\":\"page-edit\",\"singular\":1,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"47\":{\"name\":\"ProcessTemplate\",\"title\":\"Templates\",\"version\":114,\"icon\":\"cubes\",\"permission\":\"template-admin\",\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"150\":{\"name\":\"ProcessPageLister\",\"title\":\"Lister\",\"version\":26,\"icon\":\"search\",\"permission\":\"page-lister\",\"created\":1569310763,\"configurable\":true,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"136\":{\"name\":\"ProcessPermission\",\"title\":\"Permissions\",\"version\":101,\"icon\":\"gear\",\"permission\":\"permission-admin\",\"singular\":1,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true},\"48\":{\"name\":\"ProcessField\",\"title\":\"Fields\",\"version\":113,\"icon\":\"cube\",\"permission\":\"field-admin\",\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true,\"useNavJSON\":true,\"addFlag\":32},\"121\":{\"name\":\"ProcessPageEditLink\",\"title\":\"Page Edit Link\",\"version\":108,\"icon\":\"link\",\"permission\":\"page-edit\",\"singular\":1,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"168\":{\"name\":\"FieldtypePageTitleLanguage\",\"title\":\"Page Title (Multi-Language)\",\"version\":100,\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0],\"FieldtypeTextLanguage\":[\">=\",0]},\"singular\":true,\"created\":1569314759,\"namespace\":\"ProcessWire\\\\\"},\"167\":{\"name\":\"FieldtypeTextLanguage\",\"title\":\"Text (Multi-language)\",\"version\":100,\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0]},\"singular\":true,\"created\":1569314758,\"namespace\":\"ProcessWire\\\\\"},\"164\":{\"name\":\"ProcessLanguage\",\"title\":\"Languages\",\"version\":103,\"icon\":\"language\",\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"permission\":\"lang-edit\",\"singular\":1,\"created\":1569314738,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"useNavJSON\":true},\"165\":{\"name\":\"ProcessLanguageTranslator\",\"title\":\"Language Translator\",\"version\":101,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"permission\":\"lang-edit\",\"singular\":1,\"created\":1569314739,\"namespace\":\"ProcessWire\\\\\"},\"169\":{\"name\":\"FieldtypeTextareaLanguage\",\"title\":\"Textarea (Multi-language)\",\"version\":100,\"requiresVersions\":{\"LanguageSupportFields\":[\">=\",0]},\"singular\":true,\"created\":1569314759,\"namespace\":\"ProcessWire\\\\\"},\"163\":{\"name\":\"LanguageSupport\",\"title\":\"Languages Support\",\"version\":103,\"installs\":[\"ProcessLanguage\",\"ProcessLanguageTranslator\"],\"autoload\":true,\"singular\":true,\"created\":1569314738,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"addFlag\":32},\"172\":{\"name\":\"LanguageTabs\",\"title\":\"Languages Support - Tabs\",\"version\":114,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"autoload\":\"template=admin\",\"singular\":true,\"created\":1569415437,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\"},\"166\":{\"name\":\"LanguageSupportFields\",\"title\":\"Languages Support - Fields\",\"version\":100,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0]},\"installs\":[\"FieldtypePageTitleLanguage\",\"FieldtypeTextareaLanguage\",\"FieldtypeTextLanguage\"],\"autoload\":true,\"singular\":true,\"created\":1569314758,\"namespace\":\"ProcessWire\\\\\"},\"171\":{\"name\":\"LanguageSupportPageNames\",\"title\":\"Languages Support - Page Names\",\"version\":10,\"requiresVersions\":{\"LanguageSupport\":[\">=\",0],\"LanguageSupportFields\":[\">=\",0]},\"autoload\":true,\"singular\":true,\"created\":1569314785,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"39\":{\"name\":\"InputfieldRadios\",\"title\":\"Radio Buttons\",\"version\":105,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"15\":{\"name\":\"InputfieldPageListSelect\",\"title\":\"Page List Select\",\"version\":101,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"137\":{\"name\":\"InputfieldPageListSelectMultiple\",\"title\":\"Page List Select Multiple\",\"version\":102,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"56\":{\"name\":\"InputfieldImage\",\"title\":\"Images\",\"version\":123,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"80\":{\"name\":\"InputfieldEmail\",\"title\":\"Email\",\"version\":101,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\"},\"149\":{\"name\":\"InputfieldSelector\",\"title\":\"Selector\",\"version\":28,\"autoload\":\"template=admin\",\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"addFlag\":32},\"122\":{\"name\":\"InputfieldPassword\",\"title\":\"Password\",\"version\":102,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"78\":{\"name\":\"InputfieldFieldset\",\"title\":\"Fieldset\",\"version\":101,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"35\":{\"name\":\"InputfieldTextarea\",\"title\":\"Textarea\",\"version\":103,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"43\":{\"name\":\"InputfieldSelectMultiple\",\"title\":\"Select Multiple\",\"version\":101,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"38\":{\"name\":\"InputfieldCheckboxes\",\"title\":\"Checkboxes\",\"version\":107,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"162\":{\"name\":\"InputfieldIcon\",\"title\":\"Icon\",\"version\":2,\"created\":1569310790,\"namespace\":\"ProcessWire\\\\\"},\"90\":{\"name\":\"InputfieldFloat\",\"title\":\"Float\",\"version\":103,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"37\":{\"name\":\"InputfieldCheckbox\",\"title\":\"Checkbox\",\"version\":106,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"108\":{\"name\":\"InputfieldURL\",\"title\":\"URL\",\"version\":102,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\"},\"36\":{\"name\":\"InputfieldSelect\",\"title\":\"Select\",\"version\":102,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"32\":{\"name\":\"InputfieldSubmit\",\"title\":\"Submit\",\"version\":102,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"131\":{\"name\":\"InputfieldButton\",\"title\":\"Button\",\"version\":100,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"112\":{\"name\":\"InputfieldPageTitle\",\"title\":\"Page Title\",\"version\":102,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"178\":{\"name\":\"InputfieldPageAutocomplete\",\"title\":\"Page Auto Complete\",\"version\":112,\"created\":1575886766,\"namespace\":\"ProcessWire\\\\\"},\"155\":{\"name\":\"InputfieldCKEditor\",\"title\":\"CKEditor\",\"version\":162,\"installs\":[\"MarkupHTMLPurifier\"],\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\"},\"41\":{\"name\":\"InputfieldName\",\"title\":\"Name\",\"version\":100,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"85\":{\"name\":\"InputfieldInteger\",\"title\":\"Integer\",\"version\":104,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"60\":{\"name\":\"InputfieldPage\",\"title\":\"Page\",\"version\":107,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"94\":{\"name\":\"InputfieldDatetime\",\"title\":\"Datetime\",\"version\":106,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"30\":{\"name\":\"InputfieldForm\",\"title\":\"Form\",\"version\":107,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"40\":{\"name\":\"InputfieldHidden\",\"title\":\"Hidden\",\"version\":101,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"34\":{\"name\":\"InputfieldText\",\"title\":\"Text\",\"version\":106,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"79\":{\"name\":\"InputfieldMarkup\",\"title\":\"Markup\",\"version\":102,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"55\":{\"name\":\"InputfieldFile\",\"title\":\"Files\",\"version\":126,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"25\":{\"name\":\"InputfieldAsmSelect\",\"title\":\"asmSelect\",\"version\":202,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"86\":{\"name\":\"InputfieldPageName\",\"title\":\"Page Name\",\"version\":106,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"125\":{\"name\":\"SessionLoginThrottle\",\"title\":\"Session Login Throttle\",\"version\":103,\"autoload\":\"function\",\"singular\":true,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"67\":{\"name\":\"MarkupAdminDataTable\",\"title\":\"Admin Data Table\",\"version\":107,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"156\":{\"name\":\"MarkupHTMLPurifier\",\"title\":\"HTML Purifier\",\"version\":495,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\"},\"113\":{\"name\":\"MarkupPageArray\",\"title\":\"PageArray Markup\",\"version\":100,\"autoload\":true,\"singular\":true,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\"},\"98\":{\"name\":\"MarkupPagerNav\",\"title\":\"Pager (Pagination) Navigation\",\"version\":105,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\"},\"97\":{\"name\":\"FieldtypeCheckbox\",\"title\":\"Checkbox\",\"version\":101,\"singular\":1,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"6\":{\"name\":\"FieldtypeFile\",\"title\":\"Files\",\"version\":106,\"singular\":true,\"created\":1569310763,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"3\":{\"name\":\"FieldtypeText\",\"title\":\"Text\",\"version\":101,\"singular\":true,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"1\":{\"name\":\"FieldtypeTextarea\",\"title\":\"Textarea\",\"version\":107,\"singular\":true,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"105\":{\"name\":\"FieldtypeFieldsetOpen\",\"title\":\"Fieldset (Open)\",\"version\":101,\"singular\":1,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"111\":{\"name\":\"FieldtypePageTitle\",\"title\":\"Page Title\",\"version\":100,\"singular\":1,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"133\":{\"name\":\"FieldtypePassword\",\"title\":\"Password\",\"version\":101,\"singular\":true,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"57\":{\"name\":\"FieldtypeImage\",\"title\":\"Images\",\"version\":101,\"singular\":true,\"created\":1569310763,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"107\":{\"name\":\"FieldtypeFieldsetTabOpen\",\"title\":\"Fieldset in Tab (Open)\",\"version\":100,\"singular\":1,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"4\":{\"name\":\"FieldtypePage\",\"title\":\"Page Reference\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"89\":{\"name\":\"FieldtypeFloat\",\"title\":\"Float\",\"version\":105,\"singular\":1,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"84\":{\"name\":\"FieldtypeInteger\",\"title\":\"Integer\",\"version\":101,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"29\":{\"name\":\"FieldtypeEmail\",\"title\":\"E-Mail\",\"version\":100,\"singular\":true,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\"},\"135\":{\"name\":\"FieldtypeURL\",\"title\":\"URL\",\"version\":101,\"singular\":1,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"28\":{\"name\":\"FieldtypeDatetime\",\"title\":\"Datetime\",\"version\":105,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\"},\"106\":{\"name\":\"FieldtypeFieldsetClose\",\"title\":\"Fieldset (Close)\",\"version\":100,\"singular\":1,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"27\":{\"name\":\"FieldtypeModule\",\"title\":\"Module Reference\",\"version\":101,\"singular\":true,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"160\":{\"name\":\"AdminThemeUikit\",\"title\":\"Uikit\",\"version\":30,\"requiresVersions\":{\"ProcessWire\":[\">=\",\"3.0.100\"]},\"autoload\":\"template=admin\",\"created\":1569310784,\"configurable\":4,\"namespace\":\"ProcessWire\\\\\"},\"148\":{\"name\":\"AdminThemeDefault\",\"title\":\"Default\",\"version\":14,\"autoload\":\"template=admin\",\"created\":1569310763,\"configurable\":19,\"namespace\":\"ProcessWire\\\\\"},\"139\":{\"name\":\"SystemUpdater\",\"title\":\"System Updater\",\"version\":17,\"singular\":true,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"115\":{\"name\":\"PageRender\",\"title\":\"Page Render\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"103\":{\"name\":\"JqueryTableSorter\",\"title\":\"jQuery Table Sorter Plugin\",\"version\":221,\"singular\":1,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\"},\"116\":{\"name\":\"JqueryCore\",\"title\":\"jQuery Core\",\"version\":183,\"singular\":true,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"117\":{\"name\":\"JqueryUI\",\"title\":\"jQuery UI\",\"version\":196,\"singular\":true,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"151\":{\"name\":\"JqueryMagnific\",\"title\":\"jQuery Magnific Popup\",\"version\":1,\"singular\":1,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\"},\"45\":{\"name\":\"JqueryWireTabs\",\"title\":\"jQuery Wire Tabs Plugin\",\"version\":109,\"created\":1569310763,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"114\":{\"name\":\"PagePermissions\",\"title\":\"Page Permissions\",\"version\":105,\"autoload\":true,\"singular\":true,\"created\":1569310763,\"namespace\":\"ProcessWire\\\\\",\"permanent\":true},\"173\":{\"name\":\"ImageExtra\",\"title\":\"Image Extra\",\"version\":106,\"autoload\":true,\"singular\":true,\"created\":1569579876},\"179\":{\"name\":\"TextformatterVideoEmbed\",\"title\":\"Video embed for YouTube\\/Vimeo\",\"version\":111,\"singular\":1,\"created\":1575902940,\"configurable\":3,\"namespace\":\"\\\\\"},\"180\":{\"name\":\"SessionHandlerDB\",\"title\":\"Session Handler Database\",\"version\":5,\"installs\":[\"ProcessSessionDB\"],\"autoload\":true,\"singular\":true,\"configurable\":3,\"namespace\":\"ProcessWire\\\\\"},\"181\":{\"name\":\"ProcessSessionDB\",\"title\":\"Sessions\",\"version\":3,\"icon\":\"dashboard\",\"requiresVersions\":{\"SessionHandlerDB\":[\">=\",0]},\"singular\":true,\"namespace\":\"ProcessWire\\\\\"}}',	'2010-04-08 03:10:01'),
('FileCompiler__07aed021663f1f21944652d16b484aad',	'{\"source\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1576506585,\"ns\":\"ProcessWire\"},\"target\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/admin.php\",\"hash\":\"9636f854995462a4cb877cb1204bc2fe\",\"size\":467,\"time\":1576506585}}',	'2010-04-08 03:10:10'),
('FileCompiler__d47982ae70af3a6db87e00a6dabd051d',	'{\"source\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/templates\\/_init.php\",\"hash\":\"93010c41c75fb9be305b4f3a08285a7e\",\"size\":335,\"time\":1576506585,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/_init.php\",\"hash\":\"93010c41c75fb9be305b4f3a08285a7e\",\"size\":335,\"time\":1576506585}}',	'2010-04-08 03:10:10'),
('FileCompiler__e2392506e0d41a7ed2ff8107269faed1',	'{\"source\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/templates\\/home.php\",\"hash\":\"56dd3b4f3d3c1e08480a21f178be3fd7\",\"size\":4410,\"time\":1581591442,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/home.php\",\"hash\":\"a7aae9143bff7674a2ab336343fb4023\",\"size\":5362,\"time\":1581591442}}',	'2010-04-08 03:10:10'),
('FileCompiler__43edd7282704ea16fc2432872391ffe6',	'{\"source\":{\"file\":\"inc\\/head.php\",\"hash\":\"6119817caa7538eb7fd20d2ced3647e3\",\"size\":1059,\"time\":1578911116,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/assets\\/cache\\/FileCompiler\\/inc\\/head.php\",\"hash\":\"6119817caa7538eb7fd20d2ced3647e3\",\"size\":1059,\"time\":1578911116}}',	'2010-04-08 03:10:10'),
('FileCompiler__d7a7bcf317300c85b4945aa0243892e2',	'{\"source\":{\"file\":\"inc\\/header.php\",\"hash\":\"5084a1dfd1491927928ae59cb62b6ef6\",\"size\":328,\"time\":1581591443,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/assets\\/cache\\/FileCompiler\\/inc\\/header.php\",\"hash\":\"aec383405a00be276a0d36dcf2b18ddd\",\"size\":447,\"time\":1581591443}}',	'2010-04-08 03:10:10'),
('FileCompiler__2e86ff23a7421a61b678eb9822bf435a',	'{\"source\":{\"file\":\"inc\\/lang_switcher.php\",\"hash\":\"18327eef17f17a3a3eca2b2cd5c7eea5\",\"size\":489,\"time\":1576506586,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/assets\\/cache\\/FileCompiler\\/inc\\/lang_switcher.php\",\"hash\":\"18327eef17f17a3a3eca2b2cd5c7eea5\",\"size\":489,\"time\":1576506586}}',	'2010-04-08 03:10:10'),
('FileCompiler__9db2f6d10e110d460882970bffdeb7ff',	'{\"source\":{\"file\":\"inc\\/media_caption.php\",\"hash\":\"8257850b5d436054cfa0f1ae813d6ff5\",\"size\":448,\"time\":1576506586,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/assets\\/cache\\/FileCompiler\\/inc\\/media_caption.php\",\"hash\":\"8257850b5d436054cfa0f1ae813d6ff5\",\"size\":448,\"time\":1576506586}}',	'2010-04-08 03:10:10'),
('FileCompiler__0b8963959fe258294ea1deeb5b6908e8',	'{\"source\":{\"file\":\"inc\\/footer.php\",\"hash\":\"06d5295e1f378b13f2c1cc238fc0edc8\",\"size\":68,\"time\":1581591443,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/assets\\/cache\\/FileCompiler\\/inc\\/footer.php\",\"hash\":\"06d5295e1f378b13f2c1cc238fc0edc8\",\"size\":68,\"time\":1581591443}}',	'2010-04-08 03:10:10'),
('FileCompiler__07913411b83668b5fc5dc800464505f6',	'{\"source\":{\"file\":\"inc\\/foot.php\",\"hash\":\"4d214dd44eaca17a5b8c100a0cee7ebc\",\"size\":15,\"time\":1576506586,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/assets\\/cache\\/FileCompiler\\/inc\\/foot.php\",\"hash\":\"4d214dd44eaca17a5b8c100a0cee7ebc\",\"size\":15,\"time\":1576506586}}',	'2010-04-08 03:10:10'),
('FileCompiler__b14a1df3c8988e469ae126753c054ced',	'{\"source\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/templates\\/basic-page.php\",\"hash\":\"02e494d345c639c463640ba87ff942b7\",\"size\":103,\"time\":1578911067,\"ns\":\"\\\\\"},\"target\":{\"file\":\"\\/home\\/vmaillard\\/performance-lab.huma-num.fr\\/spring_school_book\\/site\\/assets\\/cache\\/FileCompiler\\/site\\/templates\\/basic-page.php\",\"hash\":\"f5649bb4f8cdd02206b571168eeb5dd2\",\"size\":341,\"time\":1578911067}}',	'2010-04-08 03:10:10');

DROP TABLE IF EXISTS `fieldgroups`;
CREATE TABLE `fieldgroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `fieldgroups` (`id`, `name`) VALUES
(2,	'admin'),
(3,	'user'),
(4,	'role'),
(5,	'permission'),
(1,	'home'),
(83,	'basic-page'),
(97,	'abstract-page'),
(98,	'workshop-page'),
(99,	'contrib-page'),
(100,	'language'),
(101,	'conf-page');

DROP TABLE IF EXISTS `fieldgroups_fields`;
CREATE TABLE `fieldgroups_fields` (
  `fieldgroups_id` int(10) unsigned NOT NULL DEFAULT '0',
  `fields_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sort` int(11) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`fieldgroups_id`,`fields_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES
(2,	2,	1,	NULL),
(2,	1,	0,	NULL),
(3,	3,	0,	NULL),
(3,	4,	2,	NULL),
(4,	5,	0,	NULL),
(5,	1,	0,	NULL),
(3,	92,	1,	NULL),
(3,	97,	3,	NULL),
(83,	1,	0,	NULL),
(1,	101,	2,	NULL),
(3,	100,	4,	NULL),
(97,	1,	0,	NULL),
(98,	105,	1,	NULL),
(99,	108,	5,	NULL),
(100,	99,	2,	NULL),
(100,	98,	1,	NULL),
(98,	1,	0,	NULL),
(99,	1,	0,	NULL),
(99,	105,	1,	NULL),
(99,	104,	2,	NULL),
(99,	103,	3,	NULL),
(99,	102,	4,	NULL),
(101,	101,	3,	NULL),
(101,	103,	4,	NULL),
(101,	1,	0,	NULL),
(101,	105,	1,	NULL),
(98,	103,	2,	NULL),
(1,	107,	3,	NULL),
(1,	1,	0,	NULL),
(1,	103,	1,	NULL),
(101,	102,	2,	NULL),
(100,	1,	0,	NULL),
(99,	101,	6,	NULL);

DROP TABLE IF EXISTS `fields`;
CREATE TABLE `fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(128) CHARACTER SET ascii NOT NULL,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `label` varchar(250) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES
(1,	'FieldtypePageTitleLanguage',	'title',	13,	'Title',	'{\"required\":1,\"textformatters\":[\"TextformatterEntities\"],\"size\":0,\"maxlength\":255,\"minlength\":0,\"showCount\":0}'),
(2,	'FieldtypeModule',	'process',	25,	'Process',	'{\"description\":\"The process that is executed on this page. Since this is mostly used by ProcessWire internally, it is recommended that you don\'t change the value of this unless adding your own pages in the admin.\",\"collapsed\":1,\"required\":1,\"moduleTypes\":[\"Process\"],\"permanent\":1}'),
(3,	'FieldtypePassword',	'pass',	24,	'Set Password',	'{\"collapsed\":1,\"size\":50,\"maxlength\":128}'),
(5,	'FieldtypePage',	'permissions',	24,	'Permissions',	'{\"derefAsPage\":0,\"parent_id\":31,\"labelFieldName\":\"title\",\"inputfield\":\"InputfieldCheckboxes\"}'),
(4,	'FieldtypePage',	'roles',	24,	'Roles',	'{\"derefAsPage\":0,\"parent_id\":30,\"labelFieldName\":\"name\",\"inputfield\":\"InputfieldCheckboxes\",\"description\":\"User will inherit the permissions assigned to each role. You may assign multiple roles to a user. When accessing a page, the user will only inherit permissions from the roles that are also assigned to the page\'s template.\"}'),
(92,	'FieldtypeEmail',	'email',	9,	'E-Mail Address',	'{\"size\":70,\"maxlength\":255}'),
(97,	'FieldtypeModule',	'admin_theme',	8,	'Admin Theme',	'{\"moduleTypes\":[\"AdminTheme\"],\"labelField\":\"title\",\"inputfieldClass\":\"InputfieldRadios\"}'),
(98,	'FieldtypeFile',	'language_files_site',	24,	'Site Translation Files',	'{\"extensions\":\"json csv\",\"maxFiles\":0,\"inputfieldClass\":\"InputfieldFile\",\"unzip\":1,\"description\":\"Use this field for translations specific to your site (like files in \\/site\\/templates\\/ for example).\",\"descriptionRows\":0,\"fileSchema\":6}'),
(99,	'FieldtypeFile',	'language_files',	24,	'Core Translation Files',	'{\"extensions\":\"json csv\",\"maxFiles\":0,\"inputfieldClass\":\"InputfieldFile\",\"unzip\":1,\"description\":\"Use this field for [language packs](http:\\/\\/modules.processwire.com\\/categories\\/language-pack\\/). To delete all files, double-click the trash can for any file, then save.\",\"descriptionRows\":0,\"fileSchema\":6}'),
(100,	'FieldtypePage',	'language',	24,	'Language',	'{\"derefAsPage\":1,\"parent_id\":1016,\"labelFieldName\":\"title\",\"inputfield\":\"InputfieldRadios\",\"required\":1}'),
(101,	'FieldtypeImage',	'images',	0,	'Images',	'{\"extensions\":\"gif jpg jpeg png svg\",\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"focusMode\":\"on\",\"maxWidth\":2000,\"maxHeight\":2000,\"resizeServer\":0,\"clientQuality\":85,\"fileSchema\":6,\"collapsed\":0,\"noLang\":1,\"otherField\":\"start, end\",\"otherFieldSettings\":\"{\\\"cf_textformatter\\\":{\\\"start\\\":\\\"\\\",\\\"end\\\":\\\"\\\"},\\\"cf_label\\\":{\\\"cf_label__start\\\":\\\"\\\",\\\"cf_label__end\\\":\\\"\\\"}}\"}'),
(102,	'FieldtypeFile',	'sounds',	0,	'Sounds',	'{\"extensions\":\"mp3\",\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldFile\",\"descriptionRows\":1,\"fileSchema\":6,\"collapsed\":0,\"label1019\":\"Sons\"}'),
(103,	'FieldtypeTextareaLanguage',	'body',	0,	'Text',	'{\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":0,\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0,\"rows\":15,\"toolbar\":\"Format, Styles, Image, PWImage, ShowBlocks, Bold, Italic, -, RemoveFormat, Source, PWImage, PWLink\",\"inlineMode\":0,\"useACF\":1,\"usePurifier\":1,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\",\"imageFields\":[\"images\"],\"textformatters\":[\"TextformatterVideoEmbed\"]}'),
(104,	'FieldtypePage',	'related_workshop',	0,	'Workshop',	'{\"derefAsPage\":1,\"inputfield\":\"InputfieldSelect\",\"parent_id\":0,\"labelFieldName\":\"title\",\"collapsed\":0,\"template_id\":44}'),
(105,	'FieldtypeText',	'author',	0,	'Author',	'{\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}'),
(106,	'FieldtypeTextLanguage',	'display_title',	0,	'Display title',	'{\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0}'),
(107,	'FieldtypeTextarea',	'footer',	0,	'Footer',	'{\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":0,\"collapsed\":0,\"rows\":10,\"toolbar\":\"Format, Styles, -, Bold, Italic, -, RemoveFormat\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog\",\"inlineMode\":0,\"useACF\":1,\"usePurifier\":1,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\"}'),
(108,	'FieldtypeFile',	'videos',	0,	'Videos',	'{\"extensions\":\"mp4\",\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldFile\",\"descriptionRows\":1,\"fileSchema\":6}');

DROP TABLE IF EXISTS `field_admin_theme`;
CREATE TABLE `field_admin_theme` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_admin_theme` (`pages_id`, `data`) VALUES
(41,	160);

DROP TABLE IF EXISTS `field_author`;
CREATE TABLE `field_author` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_author` (`pages_id`, `data`) VALUES
(1029,	'Célia Hoffstetter'),
(1030,	'Célia Hoffstetter'),
(1031,	'Alice Owen'),
(1033,	'Elena Roig Cardona'),
(1063,	'Laurent Gagnol, Coralie Mounet'),
(1038,	'Nataliya Grulois'),
(1039,	'Michel Morin'),
(1023,	'Jen Harvie'),
(1024,	'CURIOUS: Helen Paris, Leslie Hill'),
(1025,	'Anne-Laure Amilhat Szary, Philippe Rekacewicz'),
(1026,	'Gretchen Schiller, Elizabeth Claire, Lionel Reveret, David Pagnon, Andrea Giomi'),
(1027,	'Anne Dalmasso, Catherine Hänni'),
(1040,	'Ozgul Akinci'),
(1042,	'Ozgul Akinci'),
(1043,	'Michel Morin'),
(1044,	'Michel Morin'),
(1045,	'Ozgul Akinci'),
(1046,	'Michel Morin'),
(1047,	'Nataliya Grulois'),
(1048,	'Ozgul Akinci'),
(1049,	'Ozgul Akinci'),
(1050,	'Michel Morin'),
(1052,	'Jen Harvie'),
(1053,	'Jiayi Lu'),
(1054,	'Jul McOisans'),
(1055,	'Luis Meyer'),
(1056,	'Natalia Amaya'),
(1057,	'Nuala Ní Fhlathúin'),
(1058,	'François Laplantine'),
(1060,	'Natalia Amaya García'),
(1061,	'Tom Stockton, Annalisa Paroni, Miru Kim, Clémence Vendryes, Vincent Maillard'),
(1064,	'Célia Hoffstetter'),
(1065,	'Luis Meyer'),
(1032,	'Miru Kim'),
(1066,	'Miru Kim');

DROP TABLE IF EXISTS `field_body`;
CREATE TABLE `field_body` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1019` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1019` (`data1019`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_body` (`pages_id`, `data`, `data1019`) VALUES
(1022,	'<h2>Gyrotonic ®</h2>\n\n<h3><a href=\"https://art-du-mouvement.fr/\" target=\"_blank\" rel=\"noreferrer noopener\">Stéphanie Pons</a></h3>\n\n<p>Danseuse professionnelle pendant 23 ans dans de nombreuses compagnies internationales telles qu’Angelin Preljocaj, Blanca Li, Redha et Olivier Dubois, Stéphanie Pons est professeure de danse diplômée d’Etat depuis avril 2003.</p>',	''),
(1023,	'<h2>Archiving the Future City</h2>\n\n<h3>Jen Harvie</h3>\n\n<p>Inspired by the work of urban art collective zURBS, including Cecilia Sachs Olsen (a british academy postdoctoral research fellow at the Royal Holloway Centre for the GeoHumanities), this workshop will explore the city as an urban archive to investigate different meanings and priorities for urban life. We will practice a treasure hunt, collecting materials locally to bring back to the Maison de la création et de l’innovation. We will analyze and diagnose these materials as future memories of acts, feelings, bodies, and more contained in the present. The workshop will practically explore creative strategies of documentation, interpretation, and imagining the world we want to create and live in.</p>',	'<h2>Archiver la ville du futur</h2>\n\n<h3>Jen Harvie</h3>\n\n<p>Inspiré par le travail du collectif d’art urbain zURBS, incluant celui de Cecilia Sachs Olsen (chercheure postdoctorale de l’académie britannique au Royal Holloway Centre pour les humanités géographiques), cet atelier explorera la ville en tant qu’archive urbaine afin d’en explorer différentes significations et priorités pour la vie urbaine. Nous pratiquerons une chasse au trésor en collectant localement des matériaux à rapporter à la Maison de la création et de l’innovation. Nous analyserons et diagnostiquerons ces matériaux comme de futurs souvenirs d’actes, de sentiments, de corps, et faisant encore partis du présent. L’atelier explorera en pratique des stratégies créatives de documentation, d’interprétation et d’imagination du monde que nous voulons créer et dans lequel nous voulons vivre.</p>'),
(1024,	'<h2>Creative acts of rooting</h2>\n\n<h3>CURIOUS: Helen Paris and Leslie Hill</h3>\n\n<p>Using their recent site specific conservation performance “Wild Longings” as case study, Curious (Helen Paris and Leslie Hill) leads participants on a creative writing workshop that asks: “How might creative acts of rooting help sustain and nurture things we hold dear?” We will undertake a “Research Garden Planting”, creatively employing the language of planting and plants to think about our research, what it needs in order to take root, bed-in, and flourish.</p>',	'<h2>Actes créatifs d’enracinement</h2>\n\n<h3>CURIOUS : Helen Paris etLeslie Hill</h3>\n\n<p>Utilisant comme cas d’étude leur récente performance <em>Wild Longings</em> de conservation spécifique  à  un  site, Helen  Paris  et  Leslie  Hill  conduiront  les  participants  à s’interroger, à travers un atelier d’écriture créative, sur la question : comment des actes créatifs d’enracinement peuvent-ils aider à maintenir et à entretenir des choses qui nous sont chères ? Nous entreprendrons une « plantation de jardins de recherche », en employant de façon  créative  le  langage  de  la  plantation  et  des  plantes  pour  réfléchir  à  notre recherche, à ce dont elle a besoin pour s\'enraciner, s\'installer et s\'épanouir.</p>'),
(1025,	'<h2>Creative geography</h2>\n\n<h3>Anne-Laure Amilhat Szary, Philippe Rekacewicz</h3>\n\n<p>The workshop is organized around the spatial dimension of traces. It will unfold around the making of sensitive maps, to be drawn during the day. The main question that underpins the dialogue is: do we have a choice of traces? In a world where traces 10 are merchandized, what leeway do we have to make them visible or invisible? What are the traces that we wish to leave or silence? What do our traces tell us about ourselves? How can a map foster agency in the choice of trace-making? Specialist of critical counter-cartography, Philippe Rekacewicz offers us to take a pen to draw our journeys and gestures in renewed manners. This work will be done in dialogue with Anne-Laure Amilhat Szary, a geographer that looks into the linkage between the poetics and the politics of space.</p>',	'<h2>Géographies créatives</h2>\n\n<h3>Anne-Laure Amilhat Szary, Philippe Rekacewicz</h3>\n\n<p>L’atelier est organisé par Philippe Rekacewicz et Anne-Laure Amilhat Szary autour de la dimension spatiale des traces. Il s’articulera autour de la fabrique de cartes sensibles, dessinées pendant la journée d’atelier. L’idée qui traverse le dialogue que nous voulons instaurer est celle du choix des traces : dans un monde où la trace devient marchandise, quelle est notre marge de manœuvre esthétique ou politique pour visibiliser ou invisibiliser ces marques ? Quelles traces veut-on laisser ou taire ? Que disent nos traces de nous-mêmes ? En quoi la carte peut-elle constituer une trace choisie ? Spécialiste de contre-cartographie critique, Philippe Rekacewicz propose de reprendre un crayon pour dessiner d’une autre manière nos parcours et nos gestes. Ce travail se fera en dialogue avec Anne-Laure Amilhat Szary, géographe qui s’intéresse au lien entre poétique et politique de l’espace.</p>'),
(1026,	'<h2>Echoes of Embodiment</h2>\n\n<h3>Gretchen Schiller, Elizabeth Claire, Lionel Reveret, David Pagnon, Andrea Giomi</h3>\n\n<p>Traces of bodily movement are closely linked to the ways in which the body is or has been experienced, understood, imagined, defined and contextualized, culturally and historically. This workshop aims to look at the ways in which past iterations of embodiment have materialized distinct traces, drawing attention to different blueprints of movement literacy and how we make meaning from the remains of embodied experience.</p>\n\n<p>How do we consider echoes of embodiment in practice-based research for choreographing the future, that is to say, articulating as well as listening to experiences of and through movement remains? The day workshop will be split into two parts - the first half of the day will highlight a seminar presentation on the links between embodiment, history and herstory; and the second segment will provide a lecture-demonstration and experiential installation of sounding gestures interpreted as frequencies.</p>',	'<h2>Echoes of Embodiment</h2>\n\n<h3>Gretchen Schiller, Elizabeth Claire, Lionel Reveret, David Pagnon, Andrea Giomi</h3>\n\n<p>Les traces du mouvement corporel sont étroitement liées aux manières dont le corps est ou a été expérimenté, compris, imaginé, défini et contextualisé, culturellement et historiquement. Cet atelier a pour but d’examiner la manière dont les itérations passées de « embodiment » ont matérialisé des traces distinctes, en attirant l’attention sur différents modèles de décryptage du mouvement et sur la manière dont nous créons du sens à partir des vestiges de l’expérience incarnée.</p>\n\n<p>Comment considérons-nous les échos de l’« embodiment » dans la recherche basée sur la pratique pour chorégraphier l\'avenir, c\'est-à-dire à la fois articuler et écouter les expériences vécues avec et par le mouvement ? L\'atelier d\'une journée sera divisé en deux parties - la première mettra en avant une présentation du séminaire sur les liens entre « embodiment », histoire et <em>herstory</em>, la seconde sera consacrée à une conférence-démonstration et à une installation expérimentale de gestes sonores interprétés comme des fréquences.</p>'),
(1027,	'<h2>The traces of time in the mountains:</h2>\n\n<h2>The living, the mineral, the narrative</h2>\n\n<h3>Anne Dalmasso, Catherine Hänni</h3>\n\n<p>This workshop proposes to experiment with the ways in which we collect and produce traces in a given environment: the mountain. What traces do we find? For whom are they? What purpose do they serve? What traces do we produce when we go in search of the traces of the other(s) in a “natural” environment? Such are the questions we will practice in the Lautaret Alpine Centre.</p>',	'<h2>Les traces du temps en montagne :</h2>\n\n<h2>Le vivant, le minéral, le récit.</h2>\n\n<h3>Anne Dalmasso, Catherine Hänni</h3>\n\n<p>Cet atelier propose d\'expérimenter le recueil et la production de traces dans un milieu particulier, la montagne. Traces de quoi, de qui, traces pour quoi ? Et quelles traces produisons-nous quand nous partons à la recherche des traces de l\'autre et des autres dans un milieu « naturel » ? Autant de questions à pratiquer autour de la station et du col du Lautaret.</p>'),
(1028,	'<h2>Col du Lautaret</h2>',	''),
(1030,	'',	''),
(1031,	'',	''),
(1032,	'',	''),
(1033,	'',	''),
(1063,	'<h4>Conference</h4>\n\n<h2>Knowledges by traces.<br />\nFrom animal tracks to lines of urban desire.</h2>\n\n<h3>Laurent Gagnol and Coralie Mounet</h3>\n\n<p>This talk will explore “knowledges by traces”, following Carlo Ginzburg\'s work on the epistemology of traces and clues. Based on the analysis of hunting and pastoral knowledge, among the hunters and naturalists of the French Alps and in nomadic Sahara-Sahelian pastoral societies, we will highlight how this knowledge unfolds, in their attention to the clues, the traces of the pathways living beings and lines on the ground. This knowledge, initially built around animal tracks, can also relate to human practices. Urban lines of desire thus draw in space the spontaneous ways of human mobility in the city.</p>\n\n<p>https://www.youtube.com/watch?v=IioOwrrudEI</p>',	'<h4>Conférence</h4>\n\n<h2>Les connaissances par traces.</h2>\n\n<h2>Des pistes animales aux lignes de désir urbaines.</h2>\n\n<h3>Laurent Gagnol et Coralie Mounet</h3>\n\n<p>Cette conférence traitera des « connaissances par traces », étudiées dans le sillage des travaux de Carlo Ginzburg sur l’épistémologie des traces et des indices. En nous appuyant sur l’analyse des savoirs cynégétiques et pastoraux, chez les chasseurs et les naturalistes des Alpes françaises et dans les sociétés pastorales nomades saharo-sahéliennes, nous montrerons comment ces connaissances se déploient, dans leur attention aux indices, aux traces des cheminements des êtres vivants et aux lignes inscrites sur le sol. Ces savoirs indiciels, initialement construits autour de la lecture spatiale des pistes animales, peuvent également porter sur les pratiques humaines. Les lignes de désir urbaines tracent ainsi dans l’espace les manières spontanées de circuler en ville.</p>\n\n<p>https://www.youtube.com/watch?v=VVYe0qR5DnM</p>'),
(1038,	'',	''),
(1039,	'',	''),
(1040,	'',	''),
(1041,	'<h2>Morning presentation</h2>',	'<h2>Présentation matinale</h2>'),
(1042,	'',	''),
(1043,	'',	''),
(1044,	'',	''),
(1045,	'',	''),
(1046,	'',	''),
(1047,	'',	''),
(1048,	'',	''),
(1049,	'',	''),
(1050,	'',	''),
(1051,	'<h2>Feedback</h2>',	''),
(1052,	'',	''),
(1053,	'',	''),
(1054,	'',	''),
(1055,	'',	''),
(1056,	'',	''),
(1057,	'',	''),
(1,	'<h4>Doctoral Spring School</h4>\n\n<h1>Making marks</h1>\n\n<h3>17-22/06/19</h3>\n\n<p>Dans le cadre de ses activités Arts in the Alps, la SFR Création organise tous les deux ans, depuis 2017, la <strong>Spring School/Ecole de Printemps</strong>. Cet événement a pour objectif principal de créer une dynamique scientifique internationale qui se distingue par la valorisation de pratiques de recherche pluridisciplinaire, ainsi que de proposer à des doctorants de se former, par l’expérimentation, au développement de nouvelles approches de recherche qui croisent différentes disciplines des sciences humaines et sociales.</p>\n\n<p>https://www.youtube.com/watch?v=ks9wjcGC6as</p>\n\n<p><strong>The school’s objectives are to:</strong></p>\n\n<ul><li>interrogate the embodied situated thinking that underpins practice based research;</li>\n	<li>question the ways in which the materiality of trace making informs research;</li>\n	<li>question how traces become artefacts which are shared across interdisciplinary and scientific communities.</li>\n</ul><p><strong>Practical workshops and reflective seminars will address:</strong></p>\n\n<ul><li>how the materiality of artistic practice can be used as a reflective tool?</li>\n	<li>how the physicality of creative research artefacts move beyond the performative moment of making?</li>\n	<li>how perceptual traces or artefacts can be shared between different scientific fields?</li>\n</ul><p> </p>\n\n<h2>Organization of the week</h2>\n\n<p>Throughout the six-day intensive doctorate school, the attendees have the opportunity to participate in different workshops and seminars. These include daily movement practice, performance experimentation, creative writing and map making workshops led by an international group of researchers and artists. This year the event took place in the new <a href=\"http://maisondelacreation.univ-grenoble-alpes.fr/en/about/facilities/\" target=\"_blank\" rel=\"noreferrer noopener\">Maison de la création et de l’innovation (MACI)</a> our new building on the University of Grenoble Alps main campus and as well as in surrounding sites. Our new facility has specialized spaces for making and documenting practice based research and aims to support the ways in which the arts, humanities and the social sciences produce research in terms of process and product.</p>\n\n<p><strong><a href=\"https://www.arts-in-the-alps.com/wp-content/uploads/2019/05/www.arts-in-the-alps.com-programme-springschool-2019-1.pdf\" target=\"_blank\" rel=\"noreferrer noopener\">Download the detailed programme</a></strong></p>\n\n<p>For further information, please contact: <a href=\"mailto:sf-recherche-creation@univ-grenoble-alpes.fr\">sf-recherche-creation@univ-grenoble-alpes.fr</a></p>',	'<h4>École de printemps</h4>\n\n<h1>La Fabrique sensible des traces</h1>\n\n<h3>17-22 juin 2019</h3>\n\n<p>Dans le cadre de ses activités Arts in the Alps, la SFR Création organise tous les deux ans, depuis 2017, la <strong>Spring School/Ecole de Printemps</strong>. Cet événement a pour objectif principal de créer une dynamique scientifique internationale qui se distingue par la valorisation de pratiques de recherche pluridisciplinaire, ainsi que de proposer à des doctorants de se former, par l’expérimentation, au développement de nouvelles approches de recherche qui croisent différentes disciplines des sciences humaines et sociales.</p>\n\n<p>https://www.youtube.com/watch?v=ks9wjcGC6as</p>\n\n<p>L’édition 2019 pose un cadre scientifique précis, dont<strong> les objectifs</strong> sont :</p>\n\n<ul><li>d’interroger la pensée matérielle, située incarnée qui sous-tend la recherche basée sur la pratique ;</li>\n	<li>de mettre l’accent sur la manière dont les traces matérielles sont créées, à travers et pour la recherche artistique ;</li>\n	<li>de déterminer comment de telles traces (artefacts) peuvent être partagées entre les communautés du monde de la recherche.</li>\n</ul><p>Les artistes et chercheurs travailleront aux croisements de leurs <strong>méthodes</strong> et de leur rapport à la matérialité en s’interrogeant sur différentes questions lors d’ateliers et de séminaires :</p>\n\n<ul><li>Comment la matérialité d’une pratique artistique peut-elle être utilisée comme outil réflexif ?</li>\n	<li>Comment la physicalité et les artefacts de la recherche créative peuvent-ils résonner au-delà du moment performatif de la création et de la représentation artistiques (cela est particulièrement pertinent pour l’étude historique de la performance) ?</li>\n	<li>Comment les traces perceptuelles peuvent-elles être transférées d’un domaine sensoriel à un autre (par exemple, du visuel au son, du geste au texte) ?</li>\n</ul><p> </p>\n\n<h2>Organisation de la semaine</h2>\n\n<p>Durant ces six jours d’école doctorale intensive, les participants ont la possibilité de participer à différents ateliers et séminaires. Ceux-ci comprennent des ateliers sur le mouvement, l’écriture créative, la cartographie et la performance animés par un groupe international de chercheurs et d’artistes. La semaine comprend des visites de paysages culturels et naturels dans les Alpes.</p>\n\n<p>Cette année, l’événement a eu lieu à la <a href=\"http://maisondelacreation.univ-grenoble-alpes.fr/fr/presentation/site-et-structure/\">Maison de la création et de l’innovation (MACI)</a>, notre nouveau bâtiment situé sur le campus principal de l’Université Grenoble Alpes, ainsi que sur quelques sites alpins environnants. Notre nouvelle installation, la MACI, dispose d’espaces spécialisés pour la réalisation et la documentation de recherches fondées sur la pratique et vise à soutenir la manière dont les arts, les sciences humaines et les sciences sociales produisent des recherches en matière de processus et de produits.</p>\n\n<p><a href=\"https://www.arts-in-the-alps.com/wp-content/uploads/2019/05/www.arts-in-the-alps.com-programme-springschool-2019-1.pdf\" target=\"_blank\" rel=\"noreferrer noopener\"><strong>Télécharger le programme détaillé</strong></a></p>\n\n<p>Pour plus d’informations, veuillez contacter : <a href=\"mailto:sf-recherche-creation@univ-grenoble-alpes.fr\">sf-recherche-creation@univ-grenoble-alpes.fr </a></p>'),
(1058,	'<h4>Conference</h4>\n\n<h2>The sensorial, the stage and the room.</h2>\n\n<h2>Ethnography, scenography, choreography.</h2>\n\n<h3>François Laplantine</h3>\n\n<p>When ethnography as sensory knowledge encounters artistic creation, there is an intensification and re-development of the sensory experience. Theater and to a greater extent, contemporary dance, allow us to reintegrate knowledges associated with the sensorial largely unrecognized by the Western thought. These two activities, like Asian traditions, are not so much about expressing forms as they are about capturing forces. They are animated by an energy of embodiment and exteriorization which raises a series of interrogations: the relation between the body and language, the ways of saying and not saying, the transformation of time into space.</p>\n\n<p>If the question of that which is traced (notably choreographic images) are available for artists and spectators, the question of the trace is much more problematic in the live art. The latter cannot be strictly re-presented because it does not repeat itself exactly the same each night. It takes place differently each time as an event.</p>\n\n<p>https://www.youtube.com/watch?v=sRFX4HRneBM</p>',	'<h4>Conférence</h4>\n\n<h2>Le sensible, la scène et la salle.</h2>\n\n<h2>Ethnographie, scénographie, chorégraphie.</h2>\n\n<h3>François Laplantine</h3>\n\n<p>Quand l\'ethnographie comme connaissance sensible rencontre la création artistique, c\'est-à-dire l\'intensification et la réélaboration de l\'expérience sensible. Le théâtre et plus encore la danse contemporaine permettent de réintégrer dans la connaissance une dimension du sensible largement méconnue par la pensée occidentale. Ces deux activités, à l\'instar des traditions asiatiques, ne consistent pas tant à exprimer des formes qu\'à capter des forces. Elles sont animées par une énergie d\'incorporation et d\'extériorisation qui pose une série d\'interrogations : la relation entre le corps et le langage, les voies du dire et du non dire, la transformation du temps en espace.</p>\n\n<p>Si la question du tracé (notamment des figures chorégraphiques) s\'impose pour les artistes et les spectateurs, la question de la trace est beaucoup plus problématique dans le spectacle vivant. Ce dernier ne peut être au sens strict re-présenté car il ne revient pas tout à fait identique à ce qu\'il était un autre soir. Il advient chaque fois différent à la manière d\'un événement.</p>'),
(1060,	'<h2>Feedback from Natalia Amaya García</h2>\n\n<p>“June 19, 2019<br />\nGrenoble<br />\nThe Alps</p>\n\n<p>The mountains share their magic with me, with their softness and their colossal presence I can not contain them in my eyes, I love them, I am touched and honored by their presence and their beauty, I owe them respect. I wonder about my mountains, their greenery and my knowledge of them. I say to myself: Geography has allowed me to understand my research.”</p>\n\n<p>During the Spring School, I conducted a small field journal where I tried to record various sensations and reflections generated by different workshops and conferences. The previous paragraph is an excerpt from this journal, and I chose it because I mention two things from this experience that are remarkable for me, geography and mountains.</p>\n\n<p>The meaning that geography gives to the trace as index, allows me, as part of my research, to give value to the smallest of things and to recognize that there is a hidden potential. This potential will allow me to validate another type of knowledge that has been historically underestimated: the knowledge of the sensitive and the knowledge of oneself.</p>\n\n<p>For me, the whole experience of this spring school can be summed up in the mountains as a metaphor for the problematic and the methodology of research.</p>\n\n<p>In the mountain we find various paths, when the path is already made, our point of departure and our point of arrival become clear, we can move safely, but we are only moving through. But when the road is drawn and there is not a lot of traffic, and when it is a dirt road, taking it will require a risk, it is here that the adventure begins. It may take longer, however, this extra time, this detour, will allow us to observe, hear, touch and feel the landscape and find signs that have gone unnoticed before. We discover the traces of others and we can choose to deviate to follow our intuition.</p>\n\n<p>In the mountains, one is never alone, making the path traced collectively can teach us (show, prove) the logic of selection, direction and decision of the other and his knowledge. The researcher must be flexible, must refine his sense of listening, to recognize the contributions of the other who lives in the mountain or around.</p>\n\n<p>The mountain in this amalgam of knowledge is the political place to know better the voices of others (their languages, Turkish, Brazilian Portuguese, Spanish).</p>\n\n<p>Knowing the mountain from the inside is doing creative research. Here, the action of doing has a primordial role, in the mountain we sow, we harvest, we investigate, we rest, we know, we create links, we move. To do this, makes us ask questions and find answers generated by sensitivity, here we recognize not only the length of the path but also its width, its possibilities.</p>\n\n<p>I believe that the result of the last collective work based around the time spent in the mountains, where we could interact with them, summarizes the contributions of this experience and the metaphor of the mountain. The mountain is a place where several languages, several fields of knowledge, several researchers and several research communities live, where each has a specificity that the other can learn. To learn about ants and their organization and their work is also to learn about the harvest and the river, it is to learn clouds and their forms, glaciers, flowers, bees; all these elements with their own voice and with something important to say.</p>\n\n<p>We are all in this unstable questioning of creative research, we are all afraid of mud, but we know that the time will come to harvest something.</p>\n\n<p>For all süreç con amor.</p>',	'<h2>Retours de Natalia Amaya García</h2>\n\n<p>« 19 juin 2019<br />\nGrenoble<br />\nLes Alpes</p>\n\n<p>Les montagnes partagent leur magie avec moi, avec leur douceur et leur colossale présence je n\'arrive pas à les contenir dans mes yeux, je les aime, je suis touchée et honorée par leur présence et leur beauté, je leur dois du respect. Je m\'interroge sur mes montagnes, leurs verdure et ma connaissance sur elles. Je me dis : C\'est la géographie qui m\'a permis de comprendre mes recherches. »</p>\n\n<p>Pendant l\'école, j\'ai mené un petit journal de terrain où j\'ai essayé d\'enregistrer diverses sensations et réflexions générées par les différents ateliers et conférences. Le paragraphe précédent correspond à un extrait de ce journal, et je l’ai choisi parce que j\'y mentionne deux choses de cette expérience qui sont remarquables pour moi, la géographie et les montagnes.</p>\n\n<p>Le sens que donne la géographie à la trace comme índice, me permet, dans le cadre de mes recherches, de donner de la valeur au plus petit et de reconnaître qu\'il existe un potentiel caché. Ce potentiel me permettra de valider un autre type de connaissance qui a été historiquement sous-­estimée : la connaissance du sensible et la connaissance de soi-­même.</p>\n\n<p>Pour moi, toute l\'expérience de cette école de printemps peut se résumer dans la montagne comme une métaphore de la problematique et de la méthodologie de la recherche.</p>\n\n<p>Dans la montagne nous trouvons des chemins divers, quand le chemin est déjà fait , notre point de départ et notre point d\'arrivée deviennent clairs, nous pouvons nous déplacer en toute sécurité, nous ne faisons qu’y circuler. Mais lorsque le chemin est tracé et qu\'il n\'y a pas beaucoup d\'affluence, et quand c’est un chemin de terre, le prendre demandera un risque, c\'est ici que l\'aventure commence. Cela peut prendre plus de temps, cependant, ce temps en plus, ce détour, nous permettra d\'observer, d\'entendre, de toucher et de sentir le paysage et de trouver des signes qui sont passés inaperçus auparavant. Nous découvrons les traces des autres et nous pouvons choisir de nous dévier pour suivre notre intuition.</p>\n\n<p>Dans la montagne, on n\'est jamais seul, faire le chemin tracé en collectif peut nous enseigner (montrer, prouver) la logique de sélection, de direction et de décision de l’autre et de sa connaissance. Le chercheur doit être flexible, doit affiner son sens de l\'écoute, pour reconnaître les contributions de l\'autre qui vit dans la montagne ou aux alentours.</p>\n\n<p>La montagne dans cet amalgame de connaissances est le lieu politique pour connaître mieux la voix des autres (leurs langues, turc, portugais brésilien, espagnol).</p>\n\n<p>Connaître la montagne de l\'intérieur, c\'est faire de la recherche en création. Ici, l’action de faire a un rôle primordial, dans la montagne on sème, on récolte, on enquête, on se repose, on sait, on crée des liens, on se déplace. Faire celà, nous fait poser des questions et trouver des réponses generées par la sensibilité, ici nous reconnaissons non seulement la longueur du chemin mais aussi sa largeur, ses possibilités.</p>\n\n<p>Je crois que le résultat du dernier travail collectif sur le temps passé dans la montagne, où on a pu interagir avec elle, résume les apports de cette expérience et la métaphore de la montagne. La montagne comme un espace où vivent plusieurs langues, plusieurs champs de connaissances, plusieurs chercheurs et plusieurs communautés de recherche où chacun a une spécificité dont l\'autre peut apprendre. Aprendre des fourmis et de leur organisation et de leur travail c’est aussi apprendre de la recolte et du fleuve, c’est apprendre des nuages et de leurs formes, des glaciers, des fleurs, des abeilles ; tous ces élements avec leur propre voix et avec quelque chose d’important à dire.</p>\n\n<p>Nous sommes tous dans ces questionnements instables de la recherche en creation, nous avons tous peur de la boue, mais nous savons que le temps viendra de récolter quelque chose.</p>\n\n<p>Pour tous süreç con amor.</p>'),
(1061,	'<h2>If I jump in the “Pyrenees”, will the ground tremble in “Caucasus Mountains”?</h2>\n\n<p>If I jump in the “Pyrenees”, will the ground tremble in “Caucasus Mountains”?</p>\n\n<p>If I spend too much time reading the labels, will the other flowers lose their patience?</p>\n\n<p>Were the remarkable plants obliged to apply for a passport?</p>\n\n<p>Are the plants in the water stateless?</p>\n\n<p>Can I hear the same birdsong in the “montagnes australes” as in the “alpes orientales”?</p>\n\n<p>Did the foreign plants have to learn French, and have the gardeners created French as a foreign language programs for the international plants?</p>\n\n<p>When does the sun set in Japan?</p>\n\n<p>What is exotic?</p>\n\n<p>Do I feel more welcome in North America than I do in the Andes and Patagonia?</p>\n\n<p>How many steps do I need to take to get from The Arctic to Corsica?</p>\n\n<p>Do the plants live in an aristocracy? Do they all agree on the political organization?</p>\n\n<p>If the blue thistle regains it’s crown in August, when does the “almost silver geranium” loose its own?</p>\n\n<p>Have any plants managed to escape from the garden?</p>\n\n<p>Does the air taste different in “Corsica”, what about in the “southern Alps”?</p>\n\n<p>Is it snowier in the Arctic than in the Apennines? Do I need an extra layer?</p>\n\n<p>Do the flowers still bloom when it snows? Or do they go on vacation?</p>\n\n<p>Will it take the same amount of time to tour around Japan as it takes to travel around “Central Asia and China”?</p>\n\n<p>Can the same butterfly cross-pollinate between the “Balkan Peninsula” and “Himalaya and Tibet”?</p>\n\n<p>Do the Queens of the garden flirt with each other?</p>\n\n<p>Can two people sing the soundtrack to the sound of music in duet; with one being in the Carpates and the other in the “Massif Central”?</p>\n\n<p>Do all of the plants agree on their pronouns?</p>\n\n<p>The snow of Kilimanjaro has melted. Does this mean that it’s already too late for it’s plants to be represented?</p>',	'<h2>Si je saute dans les « Pyrénées », est-ce que le sol va trembler dans les « Montagnes du Caucase » ?</h2>\n\n<p>Si je prend trop de temps à lire les cartels, est-ce que les autres fleurs vont perdre patience ?</p>\n\n<p>Les plantes remarquables ont-elles été obligées de demander un passeport ?</p>\n\n<p>Les plantes dans l\'eau sont-elles apatrides ?</p>\n\n<p>Est-ce que je peux entendre le même chant d\'oiseaux dans les « Montagnes australes » que dans les « Alpes orientales » ?</p>\n\n<p>Les plantes étrangères ont-elles dû apprendre le français, et les jardiniers ont-ils créé des programmes d\'apprentissage du français pour les plantes internationales ?</p>\n\n<p>Quand est-ce que le soleil se couche au Japon ?</p>\n\n<p>Qu\'est-ce qui est exotique ?</p>\n\n<p>Est-ce que je me sens mieux accueilli en « Amérique du Nord » que dans les « Andes » et en « Patagonie » ?</p>\n\n<p>Combien de pas dois-je faire pour aller de « l\'Arctique » à la « Corse » ?</p>\n\n<p>Les plantes vivent-elles dans une aristocratie ? Sont-elles toutes d\'accord sur l\'organisation politique ?</p>\n\n<p>Si le chardon bleu retrouve sa couronne en août, quand le « Géranium argenté » perd-il la sienne ?</p>\n\n<p>Des plantes ont-elles réussi à s\'échapper du jardin ?</p>\n\n<p>L\'air a-t-il un goût différent en « Corse », qu\'en est-il dans les « Alpes du Sud » ?</p>\n\n<p>La neige est-elle plus abondante en « Arctique » que dans les « Apennins » ? Ai-je besoin d\'une couche supplémentaire ?</p>\n\n<p>Est-ce que les fleurs fleurissent encore quand il neige ? Ou bien vont-elles en vacances ?</p>\n\n<p>Est-ce qu\'il faut autant de temps pour faire le tour du « Japon » que pour faire le tour de « l\'Asie centrale et de la Chine » ?</p>\n\n<p>Est-ce que le même papillon peut se reproduire entre la « Péninsule des Balkans » et « l\'Himalaya et le Tibet » ?</p>\n\n<p>Les Reines du jardin flirtent-elles entre elles ?</p>\n\n<p>Deux personnes peuvent-elles chanter en duo ; l\'une étant dans les « Carpates » et l\'autre dans le « Massif Central » ?</p>\n\n<p>Est-ce que toutes les plantes s\'accordent de leurs pronoms ?</p>\n\n<p>La neige du Kilimandjaro a fondu. Est-ce que cela signifie qu\'il est déjà trop tard pour que ses plantes soient représentées ?</p>'),
(1064,	'<h2>Feedback from Célia Hoffstetter</h2>\n\n<p>What inspired me the most, in the literal and figurative senses, during the summer school – a true breath of fresh air – will have been the understanding that creativity is essential to all research activities.</p>\n\n<p>Having recently entered the academic world (I started this year my doctorate in English linguistics), I initially perceived myself as an apprentice researcher, a language detective. From the beginning of the school year, I went in search of clues, signs; I read tirelessly, always on the lookout for information to interpret, analyse, dissect. More than anything, I strived to find things: traces that might answer my questions and allow me to gradually reconstruct a model. But at the end of the day, seeing my dissertation as an inquiry with drawers or as a huge enigma to solve did not bring me the proverbial \"results\" that I called for, nor personal satisfaction. At a time of year when fatigue and discouragement (which is often said to be normal during a Ph.D.) seriously dampened my ambition to be the university equivalent of Sherlock Holmes, Arts in the Alps changed my outlook on research. By working for a week with researchers who are also artists (or artists who are also researchers), I understood that traces are not the only things that we find, but truly things that we create, or even undo, sometimes.</p>\n\n<p>There is a poem by Brecht, in his Handbook for City Dwellers, which makes a singular appeal to the reader: \"Erase your traces\". In light of this short sentence, Arts in the Alps allowed me to define the research process differently. What if the reader were not only the detective searching for scientific truth, but also the bandit who blurs the carefully drawn tracks, escapes ambushes and revels in complexity? The academic world, in short, could well be the domain of the \"Fun Lovin’ Criminals\", as Jamie sang, while we went in search of lost time at the Lautaret pass…</p>\n\n<p>Like a guided missile, I track, I find, I observe; but even more, I create possibilities, I erase and I start again, I open new paths and close others, I form and transform what I see. Ironically, Arts in the Alps allowed me to appropriate a central concept of my work in linguistics, that of \"agency\", which I have not only discovered in the semantics of the verbs of my texts, but that I was able to implement in my practices. I passed, so to speak, from the linguistic corpus to the physical body: the morning sessions of physical awakening with Stephanie, the walks around the campus and in the mountains, listening to the sound produced by our movements… all this helped me to reposition myself as the actress of my own musings.</p>',	'<h2>Retours de Célia Hoffstetter</h2>\n\n<p>La grande inspiration, au sens propre comme au sens figuré, que j’aurai prise de cette école d’été – un véritable bol d’air – aura été que la créativité est essentielle à toute activité de recherche.</p>\n\n<p>Fraîchement arrivée dans le monde académique (j’ai débuté cette année mon doctorat en linguistique anglaise), je me percevais au départ comme une apprentie chercheuse, une détective du langage. Dès la rentrée, je suis partie à la recherche d’indices, de signes ; j’ai lu inlassablement, toujours à l’affût d’informations à interpréter, à analyser, à décortiquer. Plus que tout, j’espérais <em>trouver </em>: trouver des traces qui répondent à mes questions et me permettent de reconstituer petit à petit un modèle. Mais au bout du compte, voir ma thèse comme une enquête à tiroirs ou comme une immense énigme à résoudre ne m’a apporté ni les fameux « résultats » que j’appelais de mes vœux, ni la satisfaction personnelle. A un moment de l’année où la fatigue et le découragement (qu’on dit souvent normaux pendant le doctorat) pesaient sérieusement sur ma casquette de Sherlock Holmes universitaire, <em>Arts in the Alps</em> a changé mon regard sur la recherche. En côtoyant pendant une semaine des chercheur.se.s également artistes (ou des artistes également chercheur.se.s…), j’ai compris que les traces ne sont pas seulement des choses que l’on trouve, mais bien des choses que l’on crée, voire même que l’on défait, parfois.</p>\n\n<p>Il y a un poème de Brecht, dans son <em>Manuel pour habitants des villes</em>, qui lance un appel singulier au lecteur : « Efface tes traces ». A la lumière de cette petite phrase, <em>Arts in the Alps</em> m’a permis de définir différemment le processus de recherche. Et si ce dernier ne tenait pas seulement du détective en quête de vérité scientifique, mais aussi du bandit qui brouille les pistes toutes tracées, échappe aux embuscades et se délecte de la complexité ? Le monde académique, en somme, pourrait bien être le domaine des « Fun Lovin’ Criminals », comme l’a chanté Jamie, tandis que nous partions à la recherche du temps perdu au col du Lautaret…</p>\n\n<p>Tête chercheuse, je traque, je déniche, j’observe ; mais plus encore, je crée des possibilités, j’efface et je recommence, j’ouvre de nouvelles pistes et en referme d’autres, je forme et transforme ce que je vois. De manière ironique, <em>Arts in the Alps</em> m’a permis de m’approprier un concept central de mon travail en linguistique, celui d’« agentivité », que je n’ai plus seulement débusqué dans le sémantisme des verbes de mes textes, mais que j’ai pu mettre en œuvre dans mes pratiques. Je suis passée pour ainsi dire du corpus, linguistique, au <em>corps</em>, physique : les séances matinales d’éveil corporel avec Stéphanie, les marches autour du campus et en montagne, l’écoute du son produit par nos gestes… tout cela m’a aidée à me resituer comme actrice de ma réflexion.</p>'),
(1065,	'<h2>Feedback from Luis Meyer</h2>\n\n<p>1. Gestures imprint/borrowed</p>\n\n<p>Questioning the concept of traces allowed me to approach the idea of the imprint as an index, a three-dimensional and fixed way of studying traces. In this sense, I begin to explore the archeology of the wall as a revealing source of the past-present-future of the material of the wall; in my case the brick walls of Lille and Roubaix. The imprint of bricks made from clay becomes for me a center of interest to explore the links between time and space. Moreover, this extracting of traces with a three-dimensional imprint allows me to continue the analysis of architectural-wall-citizen relational issues in the urban space and ask questions about this practice; do traces have a proper ontology? Is the creation of an urban narrative necessary?</p>\n\n<p>2. Ephemeral landscapes</p>\n\n<p>Creating landscapes with brick powder, is an experimental way of exploring the material of the wall by making simple images, vague and ephemeral; each is a way of thinking and imagining the trace of the building that is disappearing. In this case the trace becomes iconic with the use of the monotype.</p>\n\n<p>3. Towards a new understanding of walls</p>\n\n<p>The trace and the sensitive are two important concepts that were discussed during the Spring School. In continuation with these discussions, I have started to take an interest in the traces of collective urban imaginaries contained in texts and stories that I wish to collect through the implementation of an ethnographic inquiry. This will enable me to understand the imaginaries and the traces inhabitants associate with the walls of the town; this undertaking will also allow for a synergie to take place between the creation-inhabitant-artist process and the collective artistic practice.</p>',	'<h2>Retours de Luis Meyer</h2>\n\n<p>1. Le geste empreinte/emprunté</p>\n\n<p>Le concept de trace m\'a permis de m\'approcher vers l\'idée de l\'empreinte comme un indice, une forme tridimensionnelle et fixe d\'étudier la trace. Dans ce sens, je commence à explorer l\'archéologie du mur comme une source révélatrice du passé-présent-futur de la matière du mur ; dans mon cas les murs de briques de Lille et Roubaix. L\'empreinte de briques avec l\'argile devienne pour moi un centre d’intérêt pour ainsi explorer les liens avec le temps et l\'espace. D\'ailleurs, cette prise de traces avec une empreinte en relief me permet de poursuivre les analyses des enjeux relationnels architecture-mur-citoyen dans l\'espace urbain et me poser de questions autour de cette pratique ; est-ce que les traces ont une ontologie propre ? Il faut la création d\'un récit urbain ?</p>\n\n<p>2. Paysages éphémères</p>\n\n<p>La création de paysages avec la poudre de briques, c\'est une notion expérimentale d\'explorer la matière du mur en faisant des images simples, flou et éphémères ; chacune est une façon de pensée et imaginer la trace du bâtiment qui est en train de disparaître. Dans ce cas la trace devient iconique avec l\'utilisation du monotype.</p>\n\n<p>3. Vers une nouvelle « Muralité »</p>\n\n<p>La trace et le sensible sont deux concepts très remarqués qui ont été abordé pendant l\'école. Dans ce sens, je commence m’intéresser pour la trace de l\'imaginaire collective des habitants à partir des textes et récits que j’aimerais bien recueillir avec la mise en place d\'une enquête de caractère ethnographique. Elle me permettra de connaître les imaginaires et les traces des habitants avec les murs de la ville ; cette démarche permettra aussi une synergie avec le processus de création-habitants-artiste et la pratique artistique de façon collective.</p>'),
(1066,	'',	'');

DROP TABLE IF EXISTS `field_display_title`;
CREATE TABLE `field_display_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `data1019` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1019` (`data1019`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1019` (`data1019`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_email`;
CREATE TABLE `field_email` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_email` (`pages_id`, `data`) VALUES
(41,	'vincent.maillard@univ-grenoble-alpes.fr');

DROP TABLE IF EXISTS `field_footer`;
CREATE TABLE `field_footer` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_footer` (`pages_id`, `data`) VALUES
(1,	'<ul><li><a href=\"https://www.univ-grenoble-alpes.fr/\" target=\"_blank\" rel=\"noreferrer noopener\"><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-uga-bleu.png\" width=\"175\" /></a></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-idex.1600x0-is.jpeg\" width=\"171\" /></li>\n	<li><a href=\"https://www.arts-in-the-alps.com/\" target=\"_blank\" rel=\"noreferrer noopener\"><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-sfr.1261x0-is.png\" width=\"138\" /></a></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo_pacte_quadri.jpg\" width=\"115\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-pl-png.png\" width=\"157\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-cresson-jpg.jpg\" width=\"181\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-littarts.png\" width=\"145\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo_larhra.860x0-is.jpg\" width=\"154\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-ensimag-png.png\" width=\"117\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-esad-png.png\" width=\"157\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-ljk-jpg.jpg\" width=\"125\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-leca.jpg\" width=\"129\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-york.jpg\" width=\"163\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-roehampton.png\" width=\"140\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-taps.png\" width=\"162\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-queen-mary.png\" width=\"207\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-curious.jpg\" width=\"136\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-adm.jpg\" width=\"80\" /></li>\n	<li><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-lyon2_679x350-cropx58y61-is.png\" width=\"208\" /></li>\n	<li><a href=\"https://www.huma-num.fr/\" target=\"_blank\" rel=\"noreferrer noopener\"><img alt=\"\" src=\"/spring_school_book/site/assets/files/1/logo-petit-hn-rvb.png\" width=\"142\" /></a></li>\n</ul>');

DROP TABLE IF EXISTS `field_images`;
CREATE TABLE `field_images` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  `start` varchar(2048) DEFAULT '',
  `end` varchar(2048) DEFAULT '',
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`, `start`, `end`) VALUES
(1030,	'img_20190621_142242.jpg',	1,	'[\"\"]',	'2019-09-27 10:26:42',	'2019-09-27 10:26:42',	'',	'[\"\"]',	'[\"\"]'),
(1030,	'img_20190621_140651.jpg',	2,	'[\"\"]',	'2019-12-16 08:44:23',	'2019-12-16 08:44:23',	'',	'[\"\"]',	'[\"\"]'),
(1038,	'20190617_165745.jpg',	7,	'[\"\"]',	'2019-09-27 10:36:28',	'2019-09-27 10:36:28',	'',	'[\"\"]',	'[\"\"]'),
(1038,	'20190617_165657.jpg',	6,	'[\"\"]',	'2019-09-27 10:36:28',	'2019-09-27 10:36:28',	'',	'[\"\"]',	'[\"\"]'),
(1038,	'20190617_170610.jpg',	1,	'[\"\"]',	'2019-09-27 10:36:28',	'2019-09-27 10:36:28',	'',	'[\"\"]',	'[\"\"]'),
(1038,	'20190617_170646.jpg',	2,	'[\"\"]',	'2019-09-27 10:36:28',	'2019-09-27 10:36:28',	'',	'[\"\"]',	'[\"\"]'),
(1038,	'20190617_170724.jpg',	3,	'[\"\"]',	'2019-09-27 10:36:28',	'2019-09-27 10:36:28',	'',	'[\"\"]',	'[\"\"]'),
(1038,	'20190617_170910.jpg',	4,	'[\"\"]',	'2019-09-27 10:36:28',	'2019-09-27 10:36:28',	'',	'[\"\"]',	'[\"\"]'),
(1038,	'20190617_165614.jpg',	5,	'[\"\"]',	'2019-09-27 10:36:28',	'2019-09-27 10:36:28',	'',	'[\"\"]',	'[\"\"]'),
(1038,	'20190617_170547.jpg',	0,	'[\"\"]',	'2019-09-27 10:36:28',	'2019-09-27 10:36:28',	'',	'[\"\"]',	'[\"\"]'),
(1038,	'20190617_170529.jpg',	8,	'[\"\"]',	'2019-09-27 10:36:28',	'2019-09-27 10:36:28',	'',	'[\"\"]',	'[\"\"]'),
(1039,	'mic_7435.jpg',	3,	'[\"\"]',	'2019-09-27 10:39:34',	'2019-09-27 10:39:34',	'',	'[\"\"]',	'[\"\"]'),
(1039,	'mic_7420.jpg',	1,	'[\"\"]',	'2019-09-27 10:39:34',	'2019-09-27 10:39:34',	'',	'[\"\"]',	'[\"\"]'),
(1039,	'mic_7425.jpg',	2,	'[\"\"]',	'2019-09-27 10:39:34',	'2019-09-27 10:39:34',	'',	'[\"\"]',	'[\"\"]'),
(1039,	'mic_7419.jpg',	0,	'[\"\"]',	'2019-09-27 10:39:34',	'2019-09-27 10:39:34',	'',	'[\"\"]',	'[\"\"]'),
(1040,	'img_20190617_165737.jpg',	4,	'[\"\"]',	'2019-10-04 07:32:48',	'2019-10-04 07:32:48',	'',	'[\"\"]',	'[\"\"]'),
(1040,	'img_20190617_153614.jpg',	1,	'[\"\"]',	'2019-10-04 07:32:48',	'2019-10-04 07:32:48',	'',	'[\"\"]',	'[\"\"]'),
(1040,	'img_20190617_155811.jpg',	2,	'[\"\"]',	'2019-10-04 07:32:48',	'2019-10-04 07:32:48',	'',	'[\"\"]',	'[\"\"]'),
(1040,	'img_20190617_161820.jpg',	3,	'[\"\"]',	'2019-10-04 07:32:48',	'2019-10-04 07:32:48',	'',	'[\"\"]',	'[\"\"]'),
(1040,	'img_20190617_143844.jpg',	0,	'[\"\"]',	'2019-10-04 07:32:48',	'2019-10-04 07:32:48',	'',	'[\"\"]',	'[\"\"]'),
(1042,	'img_20190617_092630.jpg',	0,	'[\"\"]',	'2019-10-04 07:35:21',	'2019-10-04 07:35:21',	'',	'[\"\"]',	'[\"\"]'),
(1043,	'mic_7410.jpg',	1,	'[\"\"]',	'2019-10-04 07:41:12',	'2019-10-04 07:41:12',	'',	'[\"\"]',	'[\"\"]'),
(1043,	'mic_7411.jpg',	2,	'[\"\"]',	'2019-10-04 07:41:12',	'2019-10-04 07:41:12',	'',	'[\"\"]',	'[\"\"]'),
(1043,	'mic_7407.jpg',	0,	'[\"\"]',	'2019-10-04 07:41:12',	'2019-10-04 07:41:12',	'',	'[\"\"]',	'[\"\"]'),
(1044,	'mic_7513.jpg',	1,	'[\"\"]',	'2019-10-04 07:42:36',	'2019-10-04 07:42:36',	'',	'[\"\"]',	'[\"\"]'),
(1044,	'mic_7516.jpg',	2,	'[\"\"]',	'2019-10-04 07:42:36',	'2019-10-04 07:42:36',	'',	'[\"\"]',	'[\"\"]'),
(1044,	'mic_7517.jpg',	3,	'[\"\"]',	'2019-10-04 07:42:36',	'2019-10-04 07:42:36',	'',	'[\"\"]',	'[\"\"]'),
(1044,	'mic_7398.jpg',	4,	'[\"\"]',	'2019-10-04 07:42:36',	'2019-10-04 07:42:36',	'',	'[\"\"]',	'[\"\"]'),
(1044,	'mic_7400.jpg',	5,	'[\"\"]',	'2019-10-04 07:42:36',	'2019-10-04 07:42:36',	'',	'[\"\"]',	'[\"\"]'),
(1044,	'mic_7401.jpg',	6,	'[\"\"]',	'2019-10-04 07:42:36',	'2019-10-04 07:42:36',	'',	'[\"\"]',	'[\"\"]'),
(1045,	'img_20190618_175642.jpg',	2,	'[\"\"]',	'2019-10-04 08:04:39',	'2019-10-04 08:04:39',	'',	'[\"\"]',	'[\"\"]'),
(1044,	'mic_7403.jpg',	0,	'[\"\"]',	'2019-10-04 07:42:36',	'2019-10-04 07:42:36',	'',	'[\"\"]',	'[\"\"]'),
(1045,	'img_20190618_175414.jpg',	1,	'[\"\"]',	'2019-10-04 08:04:39',	'2019-10-04 08:04:39',	'',	'[\"\"]',	'[\"\"]'),
(1045,	'img_20190618_180517.jpg',	0,	'[\"\"]',	'2019-10-04 08:04:39',	'2019-10-04 08:04:39',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7460.jpg',	12,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7461.jpg',	13,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7462.jpg',	14,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7463.jpg',	15,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7464.jpg',	16,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7467.jpg',	17,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7468.jpg',	18,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7470.jpg',	19,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7472.jpg',	20,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7473.jpg',	21,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7474.jpg',	22,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7475.jpg',	23,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7477.jpg',	24,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7457.jpg',	11,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7455.jpg',	10,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7454.jpg',	9,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7451.jpg',	8,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7448.jpg',	7,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7447.jpg',	6,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7446.jpg',	5,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7444.jpg',	4,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7442.jpg',	3,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7441.jpg',	2,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7439.jpg',	1,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1046,	'mic_7479.jpg',	0,	'[\"\"]',	'2019-10-04 08:06:40',	'2019-10-04 08:06:40',	'',	'[\"\"]',	'[\"\"]'),
(1047,	'20190622_113557.jpg',	1,	'[\"\"]',	'2019-10-04 08:11:37',	'2019-10-04 08:11:37',	'',	'[\"\"]',	'[\"\"]'),
(1047,	'20190622_103159.jpg',	0,	'[\"\"]',	'2019-10-04 08:11:37',	'2019-10-04 08:11:37',	'',	'[\"\"]',	'[\"\"]'),
(1048,	'img_20190621_162133.jpg',	1,	'[\"\"]',	'2019-10-29 10:47:35',	'2019-10-29 10:47:35',	'',	'[\"\"]',	'[\"\"]'),
(1048,	'img_20190622_112232.jpg',	2,	'[\"\"]',	'2019-10-29 10:47:35',	'2019-10-29 10:47:35',	'',	'[\"\"]',	'[\"\"]'),
(1048,	'img_20190621_141155.jpg',	0,	'[\"\"]',	'2019-10-29 10:47:35',	'2019-10-29 10:47:35',	'',	'[\"\"]',	'[\"\"]'),
(1049,	'img_20190620_150358.jpg',	1,	'[\"\"]',	'2019-10-29 10:52:15',	'2019-10-29 10:52:15',	'',	'[\"\"]',	'[\"\"]'),
(1049,	'img_20190620_151034.jpg',	2,	'[\"\"]',	'2019-10-29 10:52:15',	'2019-10-29 10:52:15',	'',	'[\"\"]',	'[\"\"]'),
(1049,	'img_20190620_150341.jpg',	0,	'[\"\"]',	'2019-10-29 10:52:15',	'2019-10-29 10:52:15',	'',	'[\"\"]',	'[\"\"]'),
(1050,	'mic_7539.jpg',	4,	'[\"\"]',	'2019-10-29 10:53:59',	'2019-10-29 10:53:59',	'',	'[\"\"]',	'[\"\"]'),
(1050,	'mic_7536.jpg',	1,	'[\"\"]',	'2019-10-29 10:53:59',	'2019-10-29 10:53:59',	'',	'[\"\"]',	'[\"\"]'),
(1050,	'mic_7537.jpg',	2,	'[\"\"]',	'2019-10-29 10:53:59',	'2019-10-29 10:53:59',	'',	'[\"\"]',	'[\"\"]'),
(1050,	'mic_7538.jpg',	3,	'[\"\"]',	'2019-10-29 10:53:59',	'2019-10-29 10:53:59',	'',	'[\"\"]',	'[\"\"]'),
(1050,	'mic_7528.jpg',	0,	'[\"\"]',	'2019-10-29 10:53:59',	'2019-10-29 10:53:59',	'',	'[\"\"]',	'[\"\"]'),
(1030,	'img_20190622_095542.jpg',	3,	'[\"\"]',	'2019-12-16 08:44:23',	'2019-12-16 08:44:23',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-sfr-1.png',	23,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-taps.png',	24,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1030,	'img_20190621_131925.jpg',	0,	'[\"\"]',	'2019-09-27 10:26:42',	'2019-09-27 10:26:42',	'',	'[\"\"]',	'[\"\"]'),
(1029,	'img_20190619_174618.jpg',	4,	'[\"\"]',	'2019-12-16 08:44:36',	'2019-12-16 08:44:36',	'',	'[\"\"]',	'[\"\"]'),
(1029,	'img_20190619_171203.jpg',	3,	'[\"\"]',	'2019-12-16 08:44:36',	'2019-12-16 08:44:36',	'',	'[\"\"]',	'[\"\"]'),
(1029,	'img_20190619_170055.jpg',	1,	'[\"\"]',	'2019-12-16 08:44:36',	'2019-12-16 08:44:36',	'',	'[\"\"]',	'[\"\"]'),
(1029,	'img_20190619_170930.jpg',	2,	'[\"\"]',	'2019-12-16 08:44:36',	'2019-12-16 08:44:36',	'',	'[\"\"]',	'[\"\"]'),
(1029,	'img_20190619_165142.jpg',	0,	'[\"\"]',	'2019-09-27 10:26:42',	'2019-09-27 10:26:42',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-roehampton.png',	22,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-queen-mary.png',	21,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-pl-png.png',	20,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-petit-hn-rvb-1.png',	19,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-lyon2.png',	18,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-ljk-jpg.jpg',	17,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-littarts.png',	16,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-leca.jpg',	15,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-lyon2-1.png',	14,	'[\"\"]',	'2020-02-13 11:49:36',	'2020-02-13 11:49:36',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-lyon2_679x350-cropx58y61-is.png',	13,	'[\"\"]',	'2020-02-13 11:49:36',	'2020-02-13 11:49:36',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-idex.jpeg',	12,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-esad-png.png',	11,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-ensimag-png.png',	10,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-curious.jpg',	9,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-cresson-jpg.jpg',	8,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-adm.jpg',	7,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo_pacte_quadri.jpg',	6,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo_larhra.jpg',	5,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-york.jpg',	4,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-uga-bleu.png',	3,	'[\"\"]',	'2020-02-13 11:49:00',	'2020-02-13 11:49:00',	'',	'[\"\"]',	'[\"\"]'),
(1,	'mic_7520.jpg',	0,	'[\"\"]',	'2019-10-30 11:59:02',	'2019-10-30 11:59:02',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-sfr.png',	1,	'[\"\"]',	'2019-12-09 14:02:24',	'2019-12-09 14:02:24',	'',	'[\"\"]',	'[\"\"]'),
(1,	'logo-petit-hn-rvb.png',	2,	'[\"\"]',	'2019-12-09 14:02:24',	'2019-12-09 14:02:24',	'',	'[\"\"]',	'[\"\"]');

DROP TABLE IF EXISTS `field_language`;
CREATE TABLE `field_language` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_language` (`pages_id`, `data`, `sort`) VALUES
(40,	1017,	0),
(41,	1017,	0);

DROP TABLE IF EXISTS `field_language_files`;
CREATE TABLE `field_language_files` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_language_files_site`;
CREATE TABLE `field_language_files_site` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_pass`;
CREATE TABLE `field_pass` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` char(40) NOT NULL,
  `salt` char(32) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=ascii;

INSERT INTO `field_pass` (`pages_id`, `data`, `salt`) VALUES
(41,	'//7hG03negVzMjMohMHjt42BY4p10zm',	'$2y$11$e7fwgwDgrJkxOcgD0EKzeO'),
(40,	'',	'');

DROP TABLE IF EXISTS `field_permissions`;
CREATE TABLE `field_permissions` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_permissions` (`pages_id`, `data`, `sort`) VALUES
(38,	32,	1),
(38,	34,	2),
(38,	35,	3),
(37,	36,	0),
(38,	36,	0),
(38,	50,	4),
(38,	51,	5),
(38,	52,	7),
(38,	53,	8),
(38,	54,	6);

DROP TABLE IF EXISTS `field_process`;
CREATE TABLE `field_process` (
  `pages_id` int(11) NOT NULL DEFAULT '0',
  `data` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_process` (`pages_id`, `data`) VALUES
(6,	17),
(3,	12),
(8,	12),
(9,	14),
(10,	7),
(11,	47),
(16,	48),
(300,	104),
(21,	50),
(29,	66),
(23,	10),
(304,	138),
(31,	136),
(22,	76),
(30,	68),
(303,	129),
(2,	87),
(302,	121),
(301,	109),
(28,	76),
(1007,	150),
(1010,	159),
(1012,	161),
(1016,	164),
(1018,	165),
(1067,	181);

DROP TABLE IF EXISTS `field_related_workshop`;
CREATE TABLE `field_related_workshop` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_related_workshop` (`pages_id`, `data`, `sort`) VALUES
(1044,	1022,	0),
(1038,	1023,	0),
(1039,	1023,	0),
(1040,	1023,	0),
(1045,	1024,	0),
(1046,	1024,	0),
(1066,	1024,	0),
(1029,	1025,	0),
(1049,	1026,	0),
(1050,	1026,	0),
(1047,	1027,	0),
(1048,	1027,	0),
(1061,	1027,	0),
(1030,	1028,	0),
(1032,	1028,	0),
(1042,	1041,	0),
(1043,	1041,	0),
(1031,	1051,	0),
(1033,	1051,	0),
(1052,	1051,	0),
(1053,	1051,	0),
(1054,	1051,	0),
(1055,	1051,	0),
(1056,	1051,	0),
(1057,	1051,	0),
(1060,	1051,	0),
(1064,	1051,	0),
(1065,	1051,	0);

DROP TABLE IF EXISTS `field_roles`;
CREATE TABLE `field_roles` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_roles` (`pages_id`, `data`, `sort`) VALUES
(40,	37,	0),
(41,	37,	0),
(41,	38,	2);

DROP TABLE IF EXISTS `field_sounds`;
CREATE TABLE `field_sounds` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_sounds` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES
(1031,	'alice.mp3',	0,	'[\"\"]',	'2019-09-24 09:05:16',	'2019-09-24 09:05:16',	''),
(1033,	'elena.mp3',	0,	'[\"\"]',	'2019-09-25 09:54:31',	'2019-09-25 09:54:31',	''),
(1052,	'jen.mp3',	0,	'[\"\"]',	'2019-10-30 10:29:44',	'2019-10-30 10:29:44',	''),
(1053,	'jiayi.mp3',	0,	'[\"\"]',	'2019-10-30 10:31:01',	'2019-10-30 10:31:01',	''),
(1054,	'jul.mp3',	0,	'[\"\"]',	'2019-10-30 10:32:12',	'2019-10-30 10:32:12',	''),
(1055,	'luis.mp3',	0,	'[\"\"]',	'2019-10-30 10:34:28',	'2019-10-30 10:34:28',	''),
(1056,	'natalia.mp3',	0,	'[\"\"]',	'2019-10-30 10:37:34',	'2019-10-30 10:37:34',	''),
(1057,	'nuala_ni_fhlathuin.mp3',	0,	'[\"\"]',	'2019-10-30 10:38:28',	'2019-10-30 10:38:28',	'');

DROP TABLE IF EXISTS `field_title`;
CREATE TABLE `field_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `data1019` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1019` (`data1019`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1019` (`data1019`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_title` (`pages_id`, `data`, `data1019`) VALUES
(11,	'Templates',	NULL),
(16,	'Fields',	NULL),
(22,	'Setup',	NULL),
(3,	'Pages',	NULL),
(6,	'Add Page',	NULL),
(8,	'Tree',	NULL),
(9,	'Save Sort',	NULL),
(10,	'Edit',	NULL),
(21,	'Modules',	NULL),
(29,	'Users',	NULL),
(30,	'Roles',	NULL),
(2,	'Admin',	NULL),
(7,	'Trash',	NULL),
(27,	'404 Not Found',	NULL),
(302,	'Insert Link',	NULL),
(23,	'Login',	NULL),
(304,	'Profile',	NULL),
(301,	'Empty Trash',	NULL),
(300,	'Search',	NULL),
(303,	'Insert Image',	NULL),
(28,	'Access',	NULL),
(31,	'Permissions',	NULL),
(32,	'Edit pages',	NULL),
(34,	'Delete pages',	NULL),
(35,	'Move pages (change parent)',	NULL),
(36,	'View pages',	NULL),
(50,	'Sort child pages',	NULL),
(51,	'Change templates on pages',	NULL),
(52,	'Administer users',	NULL),
(53,	'User can update profile/password',	NULL),
(54,	'Lock or unlock a page',	NULL),
(1,	'Spring School 2019',	''),
(1006,	'Use Page Lister',	NULL),
(1007,	'Find',	NULL),
(1010,	'Recent',	NULL),
(1011,	'Can see recently edited pages',	NULL),
(1012,	'Logs',	NULL),
(1013,	'Can view system logs',	NULL),
(1014,	'Can manage system logs',	NULL),
(1015,	'Administer languages and static translation files',	NULL),
(1016,	'Languages',	NULL),
(1017,	'en',	NULL),
(1018,	'Language Translator',	NULL),
(1019,	'fr',	NULL),
(1020,	'Events',	''),
(1021,	'Contributions',	''),
(1022,	'Gyrotonic ®',	''),
(1023,	'Archiving the Future City',	'Archiver la ville du futur'),
(1024,	'Creative acts of rooting',	'Actes créatifs d’enracinement'),
(1025,	'Creative geography',	'Géographies créatives'),
(1026,	'Echoes of Embodiment',	''),
(1027,	'The traces of time in the mountains',	'Les traces du temps en montagne'),
(1028,	'Col du Lautaret',	''),
(1029,	'IMG Célia 1',	''),
(1030,	'IMG Célia 2',	''),
(1031,	'SND Alice',	''),
(1032,	'VID Miru',	''),
(1033,	'SND Elena',	''),
(1064,	'TXT Célia Hoffstetter - feedback',	''),
(1065,	'TXT Luis - Feedback',	''),
(1066,	'VID Miru - Creative acts of rooting',	''),
(1038,	'IMG Nataliya',	''),
(1039,	'IMG Michel',	''),
(1040,	'IMG Ozgul - Archiving future',	''),
(1041,	'Morning presentation',	'Présentation matinale'),
(1042,	'IMG Ozgul - Morning presentation',	''),
(1043,	'IMG Michel - Morning presentation',	''),
(1044,	'IMG Michel - Gyrotonic',	''),
(1045,	'IMG Ozgul - Seed workshop',	''),
(1046,	'IMG Michel - Seed workshop',	''),
(1047,	'IMG Nataliya - Traces of time',	''),
(1048,	'IMG Ozgul - Traces of time',	''),
(1049,	'IMG Ozgul - Echoes of Embodiment',	''),
(1050,	'IMG Michel - Echoes of Embodiment',	''),
(1051,	'Feedback',	''),
(1052,	'SND Jen - feedback',	''),
(1053,	'SND Jiayi - feeback',	''),
(1054,	'SND Jul - Feedback',	''),
(1055,	'SND Luis - Feedback',	''),
(1056,	'SND Natalia - Feedback',	''),
(1057,	'SND Nuala - Feedback',	''),
(1063,	'Knowledges by traces',	''),
(1058,	'The sensorial, the stage and the room',	'Le sensible, la scène et la salle. Ethnographie, scénographie, chorégraphie.'),
(1060,	'TXT Natalia - Feedback',	''),
(1061,	'TXT Tom+Miru+Annalisa+Clémence+Vincent',	''),
(1067,	'Sessions',	'');

DROP TABLE IF EXISTS `field_videos`;
CREATE TABLE `field_videos` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_videos` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES
(1032,	'video-1567348669.mp4',	0,	'[\"\"]',	'2019-12-16 13:57:44',	'2019-12-16 13:57:44',	''),
(1066,	'video-1567349194.mp4',	0,	'[\"\"]',	'2019-12-16 14:11:04',	'2019-12-16 14:11:04',	'');

DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(128) CHARACTER SET ascii NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `class` (`class`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES
(1,	'FieldtypeTextarea',	1,	'',	'2019-09-24 07:39:23'),
(2,	'FieldtypeNumber',	0,	'',	'2019-09-24 07:39:23'),
(3,	'FieldtypeText',	1,	'',	'2019-09-24 07:39:23'),
(4,	'FieldtypePage',	3,	'',	'2019-09-24 07:39:23'),
(30,	'InputfieldForm',	0,	'',	'2019-09-24 07:39:23'),
(6,	'FieldtypeFile',	1,	'',	'2019-09-24 07:39:23'),
(7,	'ProcessPageEdit',	1,	'',	'2019-09-24 07:39:23'),
(10,	'ProcessLogin',	0,	'',	'2019-09-24 07:39:23'),
(12,	'ProcessPageList',	0,	'{\"pageLabelField\":\"title\",\"paginationLimit\":25,\"limit\":50}',	'2019-09-24 07:39:23'),
(121,	'ProcessPageEditLink',	1,	'',	'2019-09-24 07:39:23'),
(14,	'ProcessPageSort',	0,	'',	'2019-09-24 07:39:23'),
(15,	'InputfieldPageListSelect',	0,	'',	'2019-09-24 07:39:23'),
(117,	'JqueryUI',	1,	'',	'2019-09-24 07:39:23'),
(17,	'ProcessPageAdd',	0,	'',	'2019-09-24 07:39:23'),
(125,	'SessionLoginThrottle',	11,	'',	'2019-09-24 07:39:23'),
(122,	'InputfieldPassword',	0,	'',	'2019-09-24 07:39:23'),
(25,	'InputfieldAsmSelect',	0,	'',	'2019-09-24 07:39:23'),
(116,	'JqueryCore',	1,	'',	'2019-09-24 07:39:23'),
(27,	'FieldtypeModule',	1,	'',	'2019-09-24 07:39:23'),
(28,	'FieldtypeDatetime',	0,	'',	'2019-09-24 07:39:23'),
(29,	'FieldtypeEmail',	1,	'',	'2019-09-24 07:39:23'),
(108,	'InputfieldURL',	0,	'',	'2019-09-24 07:39:23'),
(32,	'InputfieldSubmit',	0,	'',	'2019-09-24 07:39:23'),
(33,	'InputfieldWrapper',	0,	'',	'2019-09-24 07:39:23'),
(34,	'InputfieldText',	0,	'',	'2019-09-24 07:39:23'),
(35,	'InputfieldTextarea',	0,	'',	'2019-09-24 07:39:23'),
(36,	'InputfieldSelect',	0,	'',	'2019-09-24 07:39:23'),
(37,	'InputfieldCheckbox',	0,	'',	'2019-09-24 07:39:23'),
(38,	'InputfieldCheckboxes',	0,	'',	'2019-09-24 07:39:23'),
(39,	'InputfieldRadios',	0,	'',	'2019-09-24 07:39:23'),
(40,	'InputfieldHidden',	0,	'',	'2019-09-24 07:39:23'),
(41,	'InputfieldName',	0,	'',	'2019-09-24 07:39:23'),
(43,	'InputfieldSelectMultiple',	0,	'',	'2019-09-24 07:39:23'),
(45,	'JqueryWireTabs',	0,	'',	'2019-09-24 07:39:23'),
(46,	'ProcessPage',	0,	'',	'2019-09-24 07:39:23'),
(47,	'ProcessTemplate',	0,	'',	'2019-09-24 07:39:23'),
(48,	'ProcessField',	32,	'',	'2019-09-24 07:39:23'),
(50,	'ProcessModule',	0,	'',	'2019-09-24 07:39:23'),
(114,	'PagePermissions',	3,	'',	'2019-09-24 07:39:23'),
(97,	'FieldtypeCheckbox',	1,	'',	'2019-09-24 07:39:23'),
(115,	'PageRender',	3,	'{\"clearCache\":1}',	'2019-09-24 07:39:23'),
(55,	'InputfieldFile',	0,	'',	'2019-09-24 07:39:23'),
(56,	'InputfieldImage',	0,	'',	'2019-09-24 07:39:23'),
(57,	'FieldtypeImage',	1,	'',	'2019-09-24 07:39:23'),
(60,	'InputfieldPage',	0,	'{\"inputfieldClasses\":[\"InputfieldSelect\",\"InputfieldSelectMultiple\",\"InputfieldCheckboxes\",\"InputfieldRadios\",\"InputfieldAsmSelect\",\"InputfieldPageListSelect\",\"InputfieldPageListSelectMultiple\",\"InputfieldPageAutocomplete\"]}',	'2019-09-24 07:39:23'),
(61,	'TextformatterEntities',	0,	'',	'2019-09-24 07:39:23'),
(66,	'ProcessUser',	0,	'{\"showFields\":[\"name\",\"email\",\"roles\"]}',	'2019-09-24 07:39:23'),
(67,	'MarkupAdminDataTable',	0,	'',	'2019-09-24 07:39:23'),
(68,	'ProcessRole',	0,	'{\"showFields\":[\"name\"]}',	'2019-09-24 07:39:23'),
(76,	'ProcessList',	0,	'',	'2019-09-24 07:39:23'),
(78,	'InputfieldFieldset',	0,	'',	'2019-09-24 07:39:23'),
(79,	'InputfieldMarkup',	0,	'',	'2019-09-24 07:39:23'),
(80,	'InputfieldEmail',	0,	'',	'2019-09-24 07:39:23'),
(89,	'FieldtypeFloat',	1,	'',	'2019-09-24 07:39:23'),
(83,	'ProcessPageView',	0,	'',	'2019-09-24 07:39:23'),
(84,	'FieldtypeInteger',	0,	'',	'2019-09-24 07:39:23'),
(85,	'InputfieldInteger',	0,	'',	'2019-09-24 07:39:23'),
(86,	'InputfieldPageName',	0,	'',	'2019-09-24 07:39:23'),
(87,	'ProcessHome',	0,	'',	'2019-09-24 07:39:23'),
(90,	'InputfieldFloat',	0,	'',	'2019-09-24 07:39:23'),
(94,	'InputfieldDatetime',	0,	'',	'2019-09-24 07:39:23'),
(98,	'MarkupPagerNav',	0,	'',	'2019-09-24 07:39:23'),
(129,	'ProcessPageEditImageSelect',	1,	'',	'2019-09-24 07:39:23'),
(103,	'JqueryTableSorter',	1,	'',	'2019-09-24 07:39:23'),
(104,	'ProcessPageSearch',	1,	'{\"searchFields\":\"title\",\"displayField\":\"title path\"}',	'2019-09-24 07:39:23'),
(105,	'FieldtypeFieldsetOpen',	1,	'',	'2019-09-24 07:39:23'),
(106,	'FieldtypeFieldsetClose',	1,	'',	'2019-09-24 07:39:23'),
(107,	'FieldtypeFieldsetTabOpen',	1,	'',	'2019-09-24 07:39:23'),
(109,	'ProcessPageTrash',	1,	'',	'2019-09-24 07:39:23'),
(111,	'FieldtypePageTitle',	1,	'',	'2019-09-24 07:39:23'),
(112,	'InputfieldPageTitle',	0,	'',	'2019-09-24 07:39:23'),
(113,	'MarkupPageArray',	3,	'',	'2019-09-24 07:39:23'),
(131,	'InputfieldButton',	0,	'',	'2019-09-24 07:39:23'),
(133,	'FieldtypePassword',	1,	'',	'2019-09-24 07:39:23'),
(134,	'ProcessPageType',	33,	'{\"showFields\":[]}',	'2019-09-24 07:39:23'),
(135,	'FieldtypeURL',	1,	'',	'2019-09-24 07:39:23'),
(136,	'ProcessPermission',	1,	'{\"showFields\":[\"name\",\"title\"]}',	'2019-09-24 07:39:23'),
(137,	'InputfieldPageListSelectMultiple',	0,	'',	'2019-09-24 07:39:23'),
(138,	'ProcessProfile',	1,	'{\"profileFields\":[\"pass\",\"email\",\"admin_theme\",\"language\"]}',	'2019-09-24 07:39:23'),
(139,	'SystemUpdater',	1,	'{\"systemVersion\":17,\"coreVersion\":\"3.0.147\"}',	'2019-09-24 07:39:23'),
(148,	'AdminThemeDefault',	10,	'{\"colors\":\"classic\"}',	'2019-09-24 07:39:23'),
(149,	'InputfieldSelector',	42,	'',	'2019-09-24 07:39:23'),
(150,	'ProcessPageLister',	32,	'',	'2019-09-24 07:39:23'),
(151,	'JqueryMagnific',	1,	'',	'2019-09-24 07:39:23'),
(155,	'InputfieldCKEditor',	0,	'',	'2019-09-24 07:39:23'),
(156,	'MarkupHTMLPurifier',	0,	'',	'2019-09-24 07:39:23'),
(159,	'ProcessRecentPages',	1,	'',	'2019-09-24 07:39:43'),
(160,	'AdminThemeUikit',	10,	'',	'2019-09-24 07:39:44'),
(161,	'ProcessLogger',	1,	'',	'2019-09-24 07:39:50'),
(162,	'InputfieldIcon',	0,	'',	'2019-09-24 07:39:50'),
(163,	'LanguageSupport',	35,	'{\"languagesPageID\":1016,\"defaultLanguagePageID\":1017,\"otherLanguagePageIDs\":[1019],\"languageTranslatorPageID\":1018,\"uninstall\":\"\",\"submit_save_module\":\"Submit\"}',	'2019-09-24 08:45:38'),
(164,	'ProcessLanguage',	1,	'',	'2019-09-24 08:45:38'),
(165,	'ProcessLanguageTranslator',	1,	'',	'2019-09-24 08:45:39'),
(166,	'LanguageSupportFields',	3,	'',	'2019-09-24 08:45:58'),
(167,	'FieldtypeTextLanguage',	1,	'',	'2019-09-24 08:45:58'),
(168,	'FieldtypePageTitleLanguage',	1,	'',	'2019-09-24 08:45:59'),
(169,	'FieldtypeTextareaLanguage',	1,	'',	'2019-09-24 08:45:59'),
(172,	'LanguageTabs',	11,	'{\"tabField\":\"title\",\"uninstall\":\"\",\"submit_save_module\":\"Submit\"}',	'2019-09-25 12:43:57'),
(171,	'LanguageSupportPageNames',	3,	'{\"moduleVersion\":10,\"pageNumUrlPrefix1017\":\"page\",\"useHomeSegment\":\"0\",\"uninstall\":\"\",\"submit_save_module\":\"Submit\"}',	'2019-09-24 08:46:25'),
(173,	'ImageExtra',	3,	'',	'2019-09-27 10:24:36'),
(178,	'InputfieldPageAutocomplete',	0,	'',	'2019-12-09 10:19:26'),
(179,	'TextformatterVideoEmbed',	1,	'{\"maxWidth\":640,\"maxHeight\":480,\"responsive\":1,\"clearCache\":\"\",\"uninstall\":\"\",\"submit_save_module\":\"Submit\"}',	'2019-12-09 14:49:00'),
(180,	'SessionHandlerDB',	3,	'',	'2020-01-13 10:10:31'),
(181,	'ProcessSessionDB',	1,	'',	'2020-01-13 10:10:31');

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `templates_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET ascii NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '1',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '2',
  `created` timestamp NOT NULL DEFAULT '2015-12-18 05:09:00',
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '2',
  `published` datetime DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `name1019` varchar(128) CHARACTER SET ascii DEFAULT NULL,
  `status1019` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_parent_id` (`name`,`parent_id`),
  UNIQUE KEY `name1019_parent_id` (`name1019`,`parent_id`),
  KEY `parent_id` (`parent_id`),
  KEY `templates_id` (`templates_id`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `status` (`status`),
  KEY `published` (`published`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1019`, `status1019`) VALUES
(1,	0,	1,	'home',	9,	'2020-02-13 11:10:48',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	0,	'fr',	1),
(2,	1,	2,	'processwire',	1035,	'2019-12-16 14:34:04',	40,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	5,	NULL,	1),
(3,	2,	2,	'page',	21,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	0,	NULL,	1),
(6,	3,	2,	'add',	21,	'2019-09-24 07:39:54',	40,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	1,	NULL,	1),
(7,	1,	2,	'trash',	1039,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	6,	NULL,	1),
(8,	3,	2,	'list',	21,	'2019-09-24 07:39:59',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	0,	NULL,	1),
(9,	3,	2,	'sort',	1047,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	3,	NULL,	1),
(10,	3,	2,	'edit',	1045,	'2019-09-24 07:39:56',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	4,	NULL,	1),
(11,	22,	2,	'template',	21,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	0,	NULL,	1),
(16,	22,	2,	'field',	21,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	2,	NULL,	1),
(21,	2,	2,	'module',	21,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	2,	NULL,	1),
(22,	2,	2,	'setup',	21,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	1,	NULL,	1),
(23,	2,	2,	'login',	1035,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	4,	NULL,	1),
(27,	1,	29,	'http404',	1035,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	3,	'2019-09-24 09:39:23',	4,	NULL,	1),
(28,	2,	2,	'access',	13,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	3,	NULL,	1),
(29,	28,	2,	'users',	29,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	0,	NULL,	1),
(30,	28,	2,	'roles',	29,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	1,	NULL,	1),
(31,	28,	2,	'permissions',	29,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	2,	NULL,	1),
(32,	31,	5,	'page-edit',	25,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	2,	NULL,	1),
(34,	31,	5,	'page-delete',	25,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	3,	NULL,	1),
(35,	31,	5,	'page-move',	25,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	4,	NULL,	1),
(36,	31,	5,	'page-view',	25,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	0,	NULL,	1),
(37,	30,	4,	'guest',	25,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	0,	NULL,	1),
(38,	30,	4,	'superuser',	25,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	1,	NULL,	1),
(41,	29,	3,	'admin',	1,	'2020-01-13 10:11:59',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	0,	NULL,	1),
(40,	29,	3,	'guest',	25,	'2019-09-24 08:45:39',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	1,	NULL,	1),
(50,	31,	5,	'page-sort',	25,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	41,	'2019-09-24 09:39:23',	5,	NULL,	1),
(51,	31,	5,	'page-template',	25,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	41,	'2019-09-24 09:39:23',	6,	NULL,	1),
(52,	31,	5,	'user-admin',	25,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	41,	'2019-09-24 09:39:23',	10,	NULL,	1),
(53,	31,	5,	'profile-edit',	1,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	41,	'2019-09-24 09:39:23',	13,	NULL,	1),
(54,	31,	5,	'page-lock',	1,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	41,	'2019-09-24 09:39:23',	8,	NULL,	1),
(300,	3,	2,	'search',	1045,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	6,	NULL,	1),
(301,	3,	2,	'trash',	1047,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	6,	NULL,	1),
(302,	3,	2,	'link',	1041,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	7,	NULL,	1),
(303,	3,	2,	'image',	1041,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	2,	'2019-09-24 09:39:23',	8,	NULL,	1),
(304,	2,	2,	'profile',	1025,	'2019-09-24 07:39:23',	41,	'2019-09-24 07:39:23',	41,	'2019-09-24 09:39:23',	5,	NULL,	1),
(1006,	31,	5,	'page-lister',	1,	'2019-09-24 07:39:23',	40,	'2019-09-24 07:39:23',	40,	'2019-09-24 09:39:23',	9,	NULL,	1),
(1007,	3,	2,	'lister',	1,	'2019-09-24 07:39:23',	40,	'2019-09-24 07:39:23',	40,	'2019-09-24 09:39:23',	9,	NULL,	1),
(1010,	3,	2,	'recent-pages',	1,	'2019-09-24 07:39:43',	40,	'2019-09-24 07:39:43',	40,	'2019-09-24 09:39:43',	10,	NULL,	1),
(1011,	31,	5,	'page-edit-recent',	1,	'2019-09-24 07:39:43',	40,	'2019-09-24 07:39:43',	40,	'2019-09-24 09:39:43',	10,	NULL,	1),
(1012,	22,	2,	'logs',	1,	'2019-09-24 07:39:50',	40,	'2019-09-24 07:39:50',	40,	'2019-09-24 09:39:50',	2,	NULL,	1),
(1013,	31,	5,	'logs-view',	1,	'2019-09-24 07:39:50',	40,	'2019-09-24 07:39:50',	40,	'2019-09-24 09:39:50',	11,	NULL,	1),
(1014,	31,	5,	'logs-edit',	1,	'2019-09-24 07:39:50',	40,	'2019-09-24 07:39:50',	40,	'2019-09-24 09:39:50',	12,	NULL,	1),
(1015,	31,	5,	'lang-edit',	1,	'2019-09-24 08:45:38',	41,	'2019-09-24 08:45:38',	41,	'2019-09-24 10:45:38',	13,	NULL,	1),
(1016,	22,	2,	'languages',	16,	'2019-09-24 08:45:38',	41,	'2019-09-24 08:45:38',	41,	'2019-09-24 10:45:38',	3,	NULL,	1),
(1017,	1016,	46,	'default',	16,	'2019-09-24 08:47:12',	41,	'2019-09-24 08:45:39',	41,	'2019-09-24 10:45:39',	0,	NULL,	1),
(1018,	22,	2,	'language-translator',	1040,	'2019-09-24 08:45:39',	41,	'2019-09-24 08:45:39',	41,	'2019-09-24 10:45:39',	4,	NULL,	1),
(1019,	1016,	46,	'fr',	1,	'2019-09-24 08:46:58',	41,	'2019-09-24 08:46:49',	41,	'2019-09-24 10:46:49',	1,	NULL,	1),
(1020,	1,	43,	'events',	1,	'2019-12-16 11:07:44',	41,	'2019-09-24 08:54:18',	41,	'2019-09-24 10:54:18',	3,	'events',	1),
(1021,	1,	43,	'contributions',	1,	'2019-09-24 08:54:37',	41,	'2019-09-24 08:54:37',	41,	'2019-09-24 10:54:37',	4,	NULL,	1),
(1022,	1020,	44,	'gyrotonic',	1,	'2020-01-08 11:05:39',	41,	'2019-09-24 08:55:21',	41,	'2019-09-24 10:55:27',	1,	NULL,	1),
(1023,	1020,	44,	'archiving-the-future-city',	1,	'2019-12-16 11:19:00',	41,	'2019-09-24 08:56:03',	41,	'2019-09-24 10:56:08',	2,	NULL,	1),
(1024,	1020,	44,	'creative-acts-of-rooting',	1,	'2019-12-04 14:35:13',	41,	'2019-09-24 08:56:52',	41,	'2019-09-24 10:57:10',	5,	NULL,	1),
(1025,	1020,	44,	'creative-geography',	1,	'2019-12-04 14:35:49',	41,	'2019-09-24 08:57:25',	41,	'2019-09-24 10:57:30',	6,	NULL,	1),
(1026,	1020,	44,	'echoes-of-embodiment',	1,	'2019-12-04 14:37:46',	41,	'2019-09-24 08:57:43',	41,	'2019-09-24 10:57:48',	7,	NULL,	1),
(1027,	1020,	44,	'the-traces-of-time-in-mountains',	1,	'2019-12-16 08:47:14',	41,	'2019-09-24 08:58:08',	41,	'2019-09-24 10:58:12',	8,	NULL,	1),
(1028,	1020,	44,	'col-du-lautaret',	1,	'2019-12-04 14:38:38',	41,	'2019-09-24 08:59:21',	41,	'2019-09-24 10:59:26',	9,	NULL,	1),
(1029,	1021,	45,	'celia-1',	1,	'2019-12-16 08:44:36',	41,	'2019-09-24 09:01:01',	41,	'2019-09-24 11:01:01',	0,	NULL,	1),
(1030,	1021,	45,	'img-celia-2',	1,	'2019-12-16 08:44:23',	41,	'2019-09-24 09:03:38',	41,	'2019-09-24 11:04:25',	1,	NULL,	1),
(1031,	1021,	45,	'snd-alice',	1,	'2019-10-30 09:40:17',	41,	'2019-09-24 09:04:50',	41,	'2019-09-24 11:05:16',	2,	NULL,	1),
(1032,	1021,	45,	'vid-miru',	1,	'2019-12-16 13:57:44',	41,	'2019-09-24 09:06:59',	41,	'2019-12-16 14:57:44',	3,	NULL,	1),
(1033,	1021,	45,	'snd-elena',	1,	'2019-10-30 09:40:29',	41,	'2019-09-25 09:51:13',	41,	'2019-09-25 11:54:25',	4,	NULL,	1),
(1063,	1020,	47,	'knowledges-by-traces',	1,	'2020-01-28 09:27:33',	41,	'2019-12-16 11:05:43',	41,	'2019-12-16 12:06:54',	4,	NULL,	1),
(1064,	1021,	45,	'txt-celia-hoffstetter-feedback',	1,	'2019-12-16 11:27:06',	41,	'2019-12-16 11:21:30',	41,	'2019-12-16 12:22:37',	25,	NULL,	1),
(1065,	1021,	45,	'txt-luis-feedback',	1,	'2019-12-16 14:19:21',	41,	'2019-12-16 13:46:40',	41,	'2019-12-16 14:47:04',	26,	NULL,	1),
(1066,	1021,	45,	'vid-miru-creative-arts-of-rooting',	1,	'2019-12-16 14:11:04',	41,	'2019-12-16 14:05:13',	41,	'2019-12-16 15:09:22',	27,	NULL,	1),
(1038,	1021,	45,	'img-nataliya',	1,	'2019-09-27 10:36:28',	41,	'2019-09-27 10:35:20',	41,	'2019-09-27 12:36:28',	5,	NULL,	1),
(1039,	1021,	45,	'img-michel',	1,	'2019-09-27 10:39:34',	41,	'2019-09-27 10:39:10',	41,	'2019-09-27 12:39:34',	6,	NULL,	1),
(1040,	1021,	45,	'img-ozgul-archiving-future',	1,	'2019-10-04 07:32:48',	41,	'2019-10-04 07:32:11',	41,	'2019-10-04 09:32:48',	7,	NULL,	1),
(1041,	1020,	44,	'morning-presentation',	1,	'2019-12-16 10:03:06',	41,	'2019-10-04 07:34:09',	41,	'2019-10-04 09:34:15',	0,	NULL,	1),
(1042,	1021,	45,	'img-ozgul-morning-presentation',	1,	'2019-10-04 07:35:21',	41,	'2019-10-04 07:34:59',	41,	'2019-10-04 09:35:21',	8,	NULL,	1),
(1043,	1021,	45,	'img-michel-morning-presentation',	1,	'2019-10-04 07:41:12',	41,	'2019-10-04 07:40:48',	41,	'2019-10-04 09:41:12',	9,	NULL,	1),
(1044,	1021,	45,	'img-michel-gyrotonic',	1,	'2019-10-04 08:00:38',	41,	'2019-10-04 07:42:01',	41,	'2019-10-04 09:42:36',	10,	NULL,	1),
(1045,	1021,	45,	'img-ozgul-seed-workshop',	1,	'2019-10-04 08:04:39',	41,	'2019-10-04 08:04:10',	41,	'2019-10-04 10:04:39',	11,	NULL,	1),
(1046,	1021,	45,	'img-michel-seed-workshop',	1,	'2019-10-04 08:06:40',	41,	'2019-10-04 08:05:24',	41,	'2019-10-04 10:06:40',	12,	NULL,	1),
(1047,	1021,	45,	'img-nataliya-traces-of-time',	1,	'2019-10-29 10:45:18',	41,	'2019-10-04 08:10:42',	41,	'2019-10-04 10:11:37',	13,	'img-nataliya-traces-of-time',	1),
(1048,	1021,	45,	'img-ozgul-traces-of-time',	1,	'2019-10-29 10:47:35',	41,	'2019-10-29 10:46:54',	41,	'2019-10-29 11:47:35',	14,	NULL,	1),
(1049,	1021,	45,	'img-ozgul-echoes-of-embodiment',	1,	'2019-10-29 10:52:15',	41,	'2019-10-29 10:51:39',	41,	'2019-10-29 11:52:15',	15,	NULL,	1),
(1050,	1021,	45,	'img-michel-echoes-of-embodiment',	1,	'2019-10-29 10:53:59',	41,	'2019-10-29 10:53:26',	41,	'2019-10-29 11:53:59',	16,	NULL,	1),
(1051,	1020,	44,	'feedbacks',	1,	'2019-12-04 14:38:34',	41,	'2019-10-30 09:39:56',	41,	'2019-10-30 10:40:01',	10,	NULL,	1),
(1052,	1021,	45,	'snd-jen-feedback',	1,	'2019-10-30 10:29:44',	41,	'2019-10-30 10:29:26',	41,	'2019-10-30 11:29:44',	17,	NULL,	1),
(1053,	1021,	45,	'snd-jiayi-feeback',	1,	'2019-10-30 10:31:01',	41,	'2019-10-30 10:30:45',	41,	'2019-10-30 11:31:01',	18,	NULL,	1),
(1054,	1021,	45,	'snd-jul-feedback',	1,	'2019-10-30 10:32:12',	41,	'2019-10-30 10:32:02',	41,	'2019-10-30 11:32:12',	19,	NULL,	1),
(1055,	1021,	45,	'snd-luis-feedback',	1,	'2019-10-30 10:34:28',	41,	'2019-10-30 10:34:04',	41,	'2019-10-30 11:34:28',	20,	NULL,	1),
(1056,	1021,	45,	'snd-natalia-feedback',	1,	'2019-10-30 10:37:34',	41,	'2019-10-30 10:37:17',	41,	'2019-10-30 11:37:34',	21,	NULL,	1),
(1057,	1021,	45,	'snd-nuala-feedback',	1,	'2019-10-30 10:38:28',	41,	'2019-10-30 10:38:11',	41,	'2019-10-30 11:38:28',	22,	NULL,	1),
(1058,	1020,	47,	'the-sensorial-the-stage-and-the-room-ethnography-scenography-choreography',	1,	'2020-01-28 09:27:00',	41,	'2019-11-04 14:43:54',	41,	'2019-11-04 15:44:10',	3,	NULL,	1),
(1060,	1021,	45,	'txt-natalia',	1,	'2019-12-16 13:34:24',	41,	'2019-11-27 09:16:11',	41,	'2019-11-27 10:21:09',	23,	NULL,	1),
(1061,	1021,	45,	'txt-tom-miru-annalisa-clemence-vincent',	1,	'2020-01-06 15:12:29',	41,	'2019-11-28 09:52:43',	41,	'2019-11-28 10:53:00',	24,	NULL,	1),
(1067,	2,	2,	'sessions-db',	1,	'2020-01-13 10:10:31',	40,	'2020-01-13 10:10:31',	40,	'2020-01-13 11:10:31',	6,	NULL,	0);

DROP TABLE IF EXISTS `pages_access`;
CREATE TABLE `pages_access` (
  `pages_id` int(11) NOT NULL,
  `templates_id` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pages_id`),
  KEY `templates_id` (`templates_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES
(37,	2,	'2019-09-24 07:39:23'),
(38,	2,	'2019-09-24 07:39:23'),
(32,	2,	'2019-09-24 07:39:23'),
(34,	2,	'2019-09-24 07:39:23'),
(35,	2,	'2019-09-24 07:39:23'),
(36,	2,	'2019-09-24 07:39:23'),
(50,	2,	'2019-09-24 07:39:23'),
(51,	2,	'2019-09-24 07:39:23'),
(52,	2,	'2019-09-24 07:39:23'),
(53,	2,	'2019-09-24 07:39:23'),
(54,	2,	'2019-09-24 07:39:23'),
(1006,	2,	'2019-09-24 07:39:23'),
(1011,	2,	'2019-09-24 07:39:43'),
(1013,	2,	'2019-09-24 07:39:50'),
(1014,	2,	'2019-09-24 07:39:50'),
(1015,	2,	'2019-09-24 08:45:38'),
(1017,	2,	'2019-09-24 08:45:39'),
(1019,	2,	'2019-09-24 08:46:49'),
(1020,	1,	'2019-09-24 08:54:18'),
(1021,	1,	'2019-09-24 08:54:37'),
(1022,	1,	'2019-09-24 08:55:21'),
(1023,	1,	'2019-09-24 08:56:03'),
(1024,	1,	'2019-09-24 08:56:52'),
(1025,	1,	'2019-09-24 08:57:25'),
(1026,	1,	'2019-09-24 08:57:43'),
(1027,	1,	'2019-09-24 08:58:08'),
(1028,	1,	'2019-09-24 08:59:21'),
(1029,	1,	'2019-09-24 09:01:01'),
(1030,	1,	'2019-09-24 09:03:38'),
(1031,	1,	'2019-09-24 09:04:50'),
(1032,	1,	'2019-09-24 09:06:59'),
(1033,	1,	'2019-09-25 09:51:13'),
(1063,	1,	'2019-12-16 11:05:43'),
(1065,	1,	'2019-12-16 13:46:40'),
(1064,	1,	'2019-12-16 11:21:30'),
(1066,	1,	'2019-12-16 14:05:13'),
(1038,	1,	'2019-09-27 10:35:20'),
(1039,	1,	'2019-09-27 10:39:10'),
(1040,	1,	'2019-10-04 07:32:11'),
(1041,	1,	'2019-10-04 07:34:09'),
(1042,	1,	'2019-10-04 07:34:59'),
(1043,	1,	'2019-10-04 07:40:48'),
(1044,	1,	'2019-10-04 07:42:01'),
(1045,	1,	'2019-10-04 08:04:10'),
(1046,	1,	'2019-10-04 08:05:24'),
(1047,	1,	'2019-10-04 08:10:42'),
(1048,	1,	'2019-10-29 10:46:54'),
(1049,	1,	'2019-10-29 10:51:39'),
(1050,	1,	'2019-10-29 10:53:26'),
(1051,	1,	'2019-10-30 09:39:56'),
(1052,	1,	'2019-10-30 10:29:26'),
(1053,	1,	'2019-10-30 10:30:45'),
(1054,	1,	'2019-10-30 10:32:02'),
(1055,	1,	'2019-10-30 10:34:04'),
(1056,	1,	'2019-10-30 10:37:17'),
(1057,	1,	'2019-10-30 10:38:11'),
(1058,	1,	'2019-11-04 14:43:54'),
(1060,	1,	'2019-11-27 09:16:11'),
(1061,	1,	'2019-11-28 09:52:43');

DROP TABLE IF EXISTS `pages_parents`;
CREATE TABLE `pages_parents` (
  `pages_id` int(10) unsigned NOT NULL,
  `parents_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`parents_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES
(2,	1),
(3,	1),
(3,	2),
(7,	1),
(22,	1),
(22,	2),
(28,	1),
(28,	2),
(29,	1),
(29,	2),
(29,	28),
(30,	1),
(30,	2),
(30,	28),
(31,	1),
(31,	2),
(31,	28),
(1016,	2),
(1016,	22);

DROP TABLE IF EXISTS `pages_sortfields`;
CREATE TABLE `pages_sortfields` (
  `pages_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sortfield` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` char(32) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` int(10) unsigned NOT NULL DEFAULT '0',
  `ua` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `pages_id` (`pages_id`),
  KEY `user_id` (`user_id`),
  KEY `ts` (`ts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sessions` (`id`, `user_id`, `pages_id`, `data`, `ts`, `ip`, `ua`) VALUES
('0vrrfpnnr0ae9ui0oluarlc7ek',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1581667274;}}',	'2020-02-14 08:01:15',	0,	''),
('104eo1ht60502bh9l5d2t6ej4o',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1583498662;}}',	'2020-03-06 12:44:22',	0,	''),
('1p56pbjd568ptvmmr2ddarse8u',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579990648;}}',	'2020-01-25 22:17:28',	0,	''),
('2effnln69gqlao72vg4a3402eg',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579906315;}}',	'2020-01-24 22:51:56',	0,	''),
('37d06ov0mep4ciaiq1trspaend',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1580284946;}}',	'2020-01-29 08:02:26',	0,	''),
('3dpqsdvti95frr3usek2o5nqsv',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582665407;}}',	'2020-02-25 21:16:47',	0,	''),
('6ca9nie6vq90nooa7rtqac0jk7',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1580130378;}}',	'2020-01-27 13:06:18',	0,	''),
('77s9gn45m092pnkj36tr5b5a5v',	41,	27,	'Session|a:15:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1581592268;}s:15:\"ProcessPageView\";a:1:{s:18:\"loginRequestPageID\";i:2;}s:18:\"InputfieldSelector\";a:6:{s:5:\"id23_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:4:\"id2_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:4:\"id3_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:4:\"id8_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:5:\"id10_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:6:\"id303_\";a:1:{s:13:\"lastTemplates\";a:0:{}}}s:12:\"ProcessLogin\";a:0:{}s:5:\"_user\";a:4:{s:2:\"id\";i:41;s:2:\"ts\";i:1581592268;s:9:\"challenge\";s:32:\"m6fKda0oM2MzApxvoAkcU6yQdiZbYDsQ\";s:11:\"fingerprint\";s:32:\"c54ee3b3adeaad20b78ff1885ce6c242\";}s:5:\"hidpi\";b:0;s:5:\"touch\";b:0;s:11:\"clientWidth\";i:1920;s:11:\"SessionCSRF\";a:2:{s:4:\"name\";s:26:\"TOKEN1706868249X1581590774\";s:26:\"TOKEN1706868249X1581590774\";s:32:\"SuTXL2nDQE..xWA/LIhK2.ghtfjHAHIV\";}s:15:\"AdminThemeUikit\";a:2:{s:5:\"prnav\";s:7008:\"<li class=\'page-3-\'><a href=\'/spring_school_book/processwire/page/\' id=\'prnav-page-3\' data-from=\'prnav-page-2\' class=\'pw-dropdown-toggle\'>Pages</a><ul class=\'pw-dropdown-menu prnav\' data-my=\'left top\' data-at=\'left bottom\'><li class=\'page-8-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-3\' data-json=\'/spring_school_book/processwire/page/list/navJSON/\' href=\'/spring_school_book/processwire/page/list/\'><i class=\'fa fa-sitemap pw-nav-icon fa-fw\'></i>Tree</a><ul></ul><li class=\'page-6-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-3\' data-json=\'/spring_school_book/processwire/page/add/navJSON/\' href=\'/spring_school_book/processwire/page/add/\'><i class=\'fa fa-plus-circle pw-nav-icon fa-fw\'></i>Add New</a><ul></ul><li class=\'page-1007-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-3\' data-json=\'/spring_school_book/processwire/page/lister/navJSON/\' href=\'/spring_school_book/processwire/page/lister/\'><i class=\'fa fa-search pw-nav-icon fa-fw\'></i>Find</a><ul></ul><li class=\'page-1010-\'><a class=\'pw-has-items\' data-from=\'prnav-page-3\' href=\'/spring_school_book/processwire/page/recent-pages/\'><i class=\'fa fa-clock-o pw-nav-icon fa-fw\'></i>Recent</a><ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?edited=1\' href=\'/spring_school_book/processwire/page/recent-pages/?edited=1\'><i class=\'fa fa-users pw-nav-icon fa-fw\'></i>Edited</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?added=1\' href=\'/spring_school_book/processwire/page/recent-pages/?added=1\'><i class=\'fa fa-users pw-nav-icon fa-fw\'></i>Created</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?edited=1&amp;me=1\' href=\'/spring_school_book/processwire/page/recent-pages/?edited=1&me=1\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Edited by me</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?added=1&amp;me=1\' href=\'/spring_school_book/processwire/page/recent-pages/?added=1&me=1\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Created by me</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/anotherNavJSON/\' href=\'/spring_school_book/processwire/page/recent-pages/another/\'><i class=\'fa fa-plus-circle pw-nav-icon fa-fw\'></i>Add another</a><ul></ul></li></ul></li></ul></li><li class=\'page-22-\'><a href=\'/spring_school_book/processwire/setup/\' id=\'prnav-page-22\' data-from=\'prnav-page-2\' class=\'pw-dropdown-toggle\'>Setup</a><ul class=\'pw-dropdown-menu prnav\' data-my=\'left-1 top\' data-at=\'left bottom\'><li class=\'page-11-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-22\' data-json=\'/spring_school_book/processwire/setup/template/navJSON/\' href=\'/spring_school_book/processwire/setup/template/\'><i class=\'fa fa-cubes pw-nav-icon fa-fw\'></i>Templates</a><ul></ul><li class=\'page-16-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-22\' data-json=\'/spring_school_book/processwire/setup/field/navJSON/\' href=\'/spring_school_book/processwire/setup/field/\'><i class=\'fa fa-cube pw-nav-icon fa-fw\'></i>Fields</a><ul></ul><li class=\'page-1012-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-22\' data-json=\'/spring_school_book/processwire/setup/logs/navJSON/\' href=\'/spring_school_book/processwire/setup/logs/\'><i class=\'fa fa-tree pw-nav-icon fa-fw\'></i>Logs</a><ul></ul><li class=\'page-1016-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-22\' data-json=\'/spring_school_book/processwire/setup/languages/navJSON/\' href=\'/spring_school_book/processwire/setup/languages/\'><i class=\'fa fa-language pw-nav-icon fa-fw\'></i>Languages</a><ul></ul></li></ul></li><li class=\'page-21-\'><a href=\'/spring_school_book/processwire/module/\' id=\'prnav-page-21\' data-from=\'prnav-page-2\' class=\'pw-dropdown-toggle\'>Modules</a><ul class=\'pw-dropdown-menu prnav\' data-my=\'left top\' data-at=\'left bottom\'><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-21\' data-json=\'/spring_school_book/processwire/module/navJSON/?site=1\' href=\'/spring_school_book/processwire/module/?site#tab_site_modules\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Site</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-21\' data-json=\'/spring_school_book/processwire/module/navJSON/?core=1\' href=\'/spring_school_book/processwire/module/?core#tab_core_modules\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Core</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-21\' data-json=\'/spring_school_book/processwire/module/navJSON/?configurable=1\' href=\'/spring_school_book/processwire/module/?configurable#tab_configurable_modules\'><i class=\'fa fa-gear pw-nav-icon fa-fw\'></i>Configure</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-21\' data-json=\'/spring_school_book/processwire/module/navJSON/?install=1\' href=\'/spring_school_book/processwire/module/?install#tab_install_modules\'><i class=\'fa fa-sign-in pw-nav-icon fa-fw\'></i>Install</a><ul></ul><li class=\'page-0-\'><a href=\'/spring_school_book/processwire/module/?new#tab_new_modules\'><i class=\'fa fa-plus pw-nav-icon fa-fw\'></i>New</a><li class=\'page-0-\'><a href=\'/spring_school_book/processwire/module/?reset=1\'><i class=\'fa fa-refresh pw-nav-icon fa-fw\'></i>Refresh</a></li></ul></li><li class=\'page-28-\'><a href=\'/spring_school_book/processwire/access/\' id=\'prnav-page-28\' data-from=\'prnav-page-2\' class=\'pw-dropdown-toggle\'>Access</a><ul class=\'pw-dropdown-menu prnav\' data-my=\'left top\' data-at=\'left bottom\'><li class=\'page-29-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-28\' data-json=\'/spring_school_book/processwire/access/users/navJSON/\' href=\'/spring_school_book/processwire/access/users/\'><i class=\'fa fa-group pw-nav-icon fa-fw\'></i>Users</a><ul></ul><li class=\'page-30-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-28\' data-json=\'/spring_school_book/processwire/access/roles/navJSON/\' href=\'/spring_school_book/processwire/access/roles/\'><i class=\'fa fa-gears pw-nav-icon fa-fw\'></i>Roles</a><ul></ul><li class=\'page-31-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-28\' data-json=\'/spring_school_book/processwire/access/permissions/navJSON/\' href=\'/spring_school_book/processwire/access/permissions/\'><i class=\'fa fa-gear pw-nav-icon fa-fw\'></i>Permissions</a><ul></ul></li></ul></li><li class=\'page-1067-\'><a href=\'/spring_school_book/processwire/sessions-db/\'>Sessions</a></li>\";s:7:\"sidenav\";s:9532:\"<li class=\'page-3- uk-parent\'><a href=\'/spring_school_book/processwire/page/\' id=\'sidenav-page-3\'>Pages</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/page/\'><i class=\'fa fa-sitemap pw-nav-icon fa-fw\'></i>Pages</a></li><li class=\'page-8- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/page/list/navJSON/\' href=\'/spring_school_book/processwire/page/list/\'><i class=\'fa fa-sitemap pw-nav-icon fa-fw\'></i>Tree</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-6- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/page/add/navJSON/\' href=\'/spring_school_book/processwire/page/add/\'><i class=\'fa fa-plus-circle pw-nav-icon fa-fw\'></i>Add New</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-1007- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/page/lister/navJSON/\' href=\'/spring_school_book/processwire/page/lister/\'><i class=\'fa fa-search pw-nav-icon fa-fw\'></i>Find</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-1010- uk-parent\'><a href=\'/spring_school_book/processwire/page/recent-pages/\'><i class=\'fa fa-clock-o pw-nav-icon fa-fw\'></i>Recent</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/page/recent-pages/\'><i class=\'fa fa-arrow-circle-right pw-nav-icon fa-fw\'></i>Recent</a></li><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?edited=1\' href=\'/spring_school_book/processwire/page/recent-pages/?edited=1\'><i class=\'fa fa-users pw-nav-icon fa-fw\'></i>Edited</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?added=1\' href=\'/spring_school_book/processwire/page/recent-pages/?added=1\'><i class=\'fa fa-users pw-nav-icon fa-fw\'></i>Created</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?edited=1&amp;me=1\' href=\'/spring_school_book/processwire/page/recent-pages/?edited=1&me=1\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Edited by me</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?added=1&amp;me=1\' href=\'/spring_school_book/processwire/page/recent-pages/?added=1&me=1\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Created by me</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/anotherNavJSON/\' href=\'/spring_school_book/processwire/page/recent-pages/another/\'><i class=\'fa fa-plus-circle pw-nav-icon fa-fw\'></i>Add another</a><ul></ul></li></ul></li></ul></li><li class=\'page-22- uk-parent\'><a href=\'/spring_school_book/processwire/setup/\' id=\'sidenav-page-22\'>Setup</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/setup/\'><i class=\'fa fa-gears pw-nav-icon fa-fw\'></i>Setup</a></li><li class=\'page-11- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/setup/template/navJSON/\' href=\'/spring_school_book/processwire/setup/template/\'><i class=\'fa fa-cubes pw-nav-icon fa-fw\'></i>Templates</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-16- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/setup/field/navJSON/\' href=\'/spring_school_book/processwire/setup/field/\'><i class=\'fa fa-cube pw-nav-icon fa-fw\'></i>Fields</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-1012- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/setup/logs/navJSON/\' href=\'/spring_school_book/processwire/setup/logs/\'><i class=\'fa fa-tree pw-nav-icon fa-fw\'></i>Logs</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-1016- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/setup/languages/navJSON/\' href=\'/spring_school_book/processwire/setup/languages/\'><i class=\'fa fa-language pw-nav-icon fa-fw\'></i>Languages</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li></ul></li><li class=\'page-21- uk-parent\'><a href=\'/spring_school_book/processwire/module/\' id=\'sidenav-page-21\'>Modules</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/module/\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Modules</a></li><li class=\'page-0- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/module/navJSON/?site=1\' href=\'/spring_school_book/processwire/module/?site#tab_site_modules\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Site</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-0- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/module/navJSON/?core=1\' href=\'/spring_school_book/processwire/module/?core#tab_core_modules\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Core</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-0- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/module/navJSON/?configurable=1\' href=\'/spring_school_book/processwire/module/?configurable#tab_configurable_modules\'><i class=\'fa fa-gear pw-nav-icon fa-fw\'></i>Configure</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-0- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/module/navJSON/?install=1\' href=\'/spring_school_book/processwire/module/?install#tab_install_modules\'><i class=\'fa fa-sign-in pw-nav-icon fa-fw\'></i>Install</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-0-\'><a href=\'/spring_school_book/processwire/module/?new#tab_new_modules\'><i class=\'fa fa-plus pw-nav-icon fa-fw\'></i>New</a></li><li class=\'page-0-\'><a href=\'/spring_school_book/processwire/module/?reset=1\'><i class=\'fa fa-refresh pw-nav-icon fa-fw\'></i>Refresh</a></li></ul></li><li class=\'page-28- uk-parent\'><a href=\'/spring_school_book/processwire/access/\' id=\'sidenav-page-28\'>Access</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/access/\'><i class=\'fa fa-key pw-nav-icon fa-fw\'></i>Access</a></li><li class=\'page-29- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/access/users/navJSON/\' href=\'/spring_school_book/processwire/access/users/\'><i class=\'fa fa-group pw-nav-icon fa-fw\'></i>Users</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-30- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/access/roles/navJSON/\' href=\'/spring_school_book/processwire/access/roles/\'><i class=\'fa fa-gears pw-nav-icon fa-fw\'></i>Roles</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-31- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/access/permissions/navJSON/\' href=\'/spring_school_book/processwire/access/permissions/\'><i class=\'fa fa-gear pw-nav-icon fa-fw\'></i>Permissions</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li></ul></li><li class=\'page-1067-\'><a href=\'/spring_school_book/processwire/sessions-db/\' id=\'sidenav-page-1067\'>Sessions</a></li><li class=\'uk-parent\'><a href=\'#\'><!--pw-user-nav-label--></a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li><a target=\'_top\' href=\'/spring_school_book/\'><i class=\'fa fa-eye pw-nav-icon fa-fw\'></i>View site</a></li><li><a href=\'/spring_school_book/processwire/profile/\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Profile</a></li><li><a target=\'_top\' href=\'/spring_school_book/processwire/login/logout/\'><i class=\'fa fa-power-off pw-nav-icon fa-fw\'></i>Logout</a></li></ul></li>\";}s:14:\"ProcessPageAdd\";a:2:{s:3:\"nav\";a:6:{s:3:\"url\";s:41:\"/spring_school_book/processwire/page/add/\";s:5:\"label\";s:5:\"Pages\";s:4:\"icon\";s:11:\"plus-circle\";s:3:\"add\";N;s:4:\"list\";a:0:{}s:8:\"modified\";i:1581592210;}s:10:\"numAddable\";i:0;}s:18:\"InputfieldCKEditor\";a:1:{s:15:\"skipImageFields\";a:0:{}}s:10:\"Pageimages\";a:0:{}s:26:\"ProcessPageEditImageSelect\";a:1:{s:12:\"edit_page_id\";i:1;}s:15:\"ProcessPageList\";a:1:{s:7:\"page_id\";s:5:\"title\";}}',	'2020-02-13 11:11:08',	0,	''),
('7u8k98nqfvc1esiqracunqbcsn',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582502479;}}',	'2020-02-24 00:01:20',	0,	''),
('875ebiim57s49s12f0m8njrta6',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1583386788;}}',	'2020-03-05 05:39:48',	0,	''),
('87uda2nhh4fqas25os46r5d3on',	41,	1,	'Session|a:12:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1580203683;}s:15:\"ProcessPageView\";a:1:{s:18:\"loginRequestPageID\";i:2;}s:18:\"InputfieldSelector\";a:5:{s:5:\"id23_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:4:\"id2_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:4:\"id3_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:4:\"id8_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:5:\"id10_\";a:1:{s:13:\"lastTemplates\";a:0:{}}}s:12:\"ProcessLogin\";a:0:{}s:5:\"_user\";a:4:{s:2:\"id\";i:41;s:2:\"ts\";i:1580203683;s:9:\"challenge\";s:32:\"1uAsh1kmDYuRPQRePgs3gd5FbwyosB2X\";s:11:\"fingerprint\";s:32:\"c54ee3b3adeaad20b78ff1885ce6c242\";}s:5:\"hidpi\";b:0;s:5:\"touch\";b:0;s:11:\"clientWidth\";i:1920;s:11:\"SessionCSRF\";a:2:{s:4:\"name\";s:25:\"TOKEN890724805X1580203576\";s:25:\"TOKEN890724805X1580203576\";s:32:\"yYMersQQSNedCMJszEcyaQAzzhbtcuPK\";}s:15:\"AdminThemeUikit\";a:2:{s:5:\"prnav\";s:7008:\"<li class=\'page-3-\'><a href=\'/spring_school_book/processwire/page/\' id=\'prnav-page-3\' data-from=\'prnav-page-2\' class=\'pw-dropdown-toggle\'>Pages</a><ul class=\'pw-dropdown-menu prnav\' data-my=\'left top\' data-at=\'left bottom\'><li class=\'page-8-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-3\' data-json=\'/spring_school_book/processwire/page/list/navJSON/\' href=\'/spring_school_book/processwire/page/list/\'><i class=\'fa fa-sitemap pw-nav-icon fa-fw\'></i>Tree</a><ul></ul><li class=\'page-6-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-3\' data-json=\'/spring_school_book/processwire/page/add/navJSON/\' href=\'/spring_school_book/processwire/page/add/\'><i class=\'fa fa-plus-circle pw-nav-icon fa-fw\'></i>Add New</a><ul></ul><li class=\'page-1007-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-3\' data-json=\'/spring_school_book/processwire/page/lister/navJSON/\' href=\'/spring_school_book/processwire/page/lister/\'><i class=\'fa fa-search pw-nav-icon fa-fw\'></i>Find</a><ul></ul><li class=\'page-1010-\'><a class=\'pw-has-items\' data-from=\'prnav-page-3\' href=\'/spring_school_book/processwire/page/recent-pages/\'><i class=\'fa fa-clock-o pw-nav-icon fa-fw\'></i>Recent</a><ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?edited=1\' href=\'/spring_school_book/processwire/page/recent-pages/?edited=1\'><i class=\'fa fa-users pw-nav-icon fa-fw\'></i>Edited</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?added=1\' href=\'/spring_school_book/processwire/page/recent-pages/?added=1\'><i class=\'fa fa-users pw-nav-icon fa-fw\'></i>Created</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?edited=1&amp;me=1\' href=\'/spring_school_book/processwire/page/recent-pages/?edited=1&me=1\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Edited by me</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?added=1&amp;me=1\' href=\'/spring_school_book/processwire/page/recent-pages/?added=1&me=1\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Created by me</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/anotherNavJSON/\' href=\'/spring_school_book/processwire/page/recent-pages/another/\'><i class=\'fa fa-plus-circle pw-nav-icon fa-fw\'></i>Add another</a><ul></ul></li></ul></li></ul></li><li class=\'page-22-\'><a href=\'/spring_school_book/processwire/setup/\' id=\'prnav-page-22\' data-from=\'prnav-page-2\' class=\'pw-dropdown-toggle\'>Setup</a><ul class=\'pw-dropdown-menu prnav\' data-my=\'left-1 top\' data-at=\'left bottom\'><li class=\'page-11-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-22\' data-json=\'/spring_school_book/processwire/setup/template/navJSON/\' href=\'/spring_school_book/processwire/setup/template/\'><i class=\'fa fa-cubes pw-nav-icon fa-fw\'></i>Templates</a><ul></ul><li class=\'page-16-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-22\' data-json=\'/spring_school_book/processwire/setup/field/navJSON/\' href=\'/spring_school_book/processwire/setup/field/\'><i class=\'fa fa-cube pw-nav-icon fa-fw\'></i>Fields</a><ul></ul><li class=\'page-1012-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-22\' data-json=\'/spring_school_book/processwire/setup/logs/navJSON/\' href=\'/spring_school_book/processwire/setup/logs/\'><i class=\'fa fa-tree pw-nav-icon fa-fw\'></i>Logs</a><ul></ul><li class=\'page-1016-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-22\' data-json=\'/spring_school_book/processwire/setup/languages/navJSON/\' href=\'/spring_school_book/processwire/setup/languages/\'><i class=\'fa fa-language pw-nav-icon fa-fw\'></i>Languages</a><ul></ul></li></ul></li><li class=\'page-21-\'><a href=\'/spring_school_book/processwire/module/\' id=\'prnav-page-21\' data-from=\'prnav-page-2\' class=\'pw-dropdown-toggle\'>Modules</a><ul class=\'pw-dropdown-menu prnav\' data-my=\'left top\' data-at=\'left bottom\'><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-21\' data-json=\'/spring_school_book/processwire/module/navJSON/?site=1\' href=\'/spring_school_book/processwire/module/?site#tab_site_modules\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Site</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-21\' data-json=\'/spring_school_book/processwire/module/navJSON/?core=1\' href=\'/spring_school_book/processwire/module/?core#tab_core_modules\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Core</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-21\' data-json=\'/spring_school_book/processwire/module/navJSON/?configurable=1\' href=\'/spring_school_book/processwire/module/?configurable#tab_configurable_modules\'><i class=\'fa fa-gear pw-nav-icon fa-fw\'></i>Configure</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-21\' data-json=\'/spring_school_book/processwire/module/navJSON/?install=1\' href=\'/spring_school_book/processwire/module/?install#tab_install_modules\'><i class=\'fa fa-sign-in pw-nav-icon fa-fw\'></i>Install</a><ul></ul><li class=\'page-0-\'><a href=\'/spring_school_book/processwire/module/?new#tab_new_modules\'><i class=\'fa fa-plus pw-nav-icon fa-fw\'></i>New</a><li class=\'page-0-\'><a href=\'/spring_school_book/processwire/module/?reset=1\'><i class=\'fa fa-refresh pw-nav-icon fa-fw\'></i>Refresh</a></li></ul></li><li class=\'page-28-\'><a href=\'/spring_school_book/processwire/access/\' id=\'prnav-page-28\' data-from=\'prnav-page-2\' class=\'pw-dropdown-toggle\'>Access</a><ul class=\'pw-dropdown-menu prnav\' data-my=\'left top\' data-at=\'left bottom\'><li class=\'page-29-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-28\' data-json=\'/spring_school_book/processwire/access/users/navJSON/\' href=\'/spring_school_book/processwire/access/users/\'><i class=\'fa fa-group pw-nav-icon fa-fw\'></i>Users</a><ul></ul><li class=\'page-30-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-28\' data-json=\'/spring_school_book/processwire/access/roles/navJSON/\' href=\'/spring_school_book/processwire/access/roles/\'><i class=\'fa fa-gears pw-nav-icon fa-fw\'></i>Roles</a><ul></ul><li class=\'page-31-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-28\' data-json=\'/spring_school_book/processwire/access/permissions/navJSON/\' href=\'/spring_school_book/processwire/access/permissions/\'><i class=\'fa fa-gear pw-nav-icon fa-fw\'></i>Permissions</a><ul></ul></li></ul></li><li class=\'page-1067-\'><a href=\'/spring_school_book/processwire/sessions-db/\'>Sessions</a></li>\";s:7:\"sidenav\";s:9532:\"<li class=\'page-3- uk-parent\'><a href=\'/spring_school_book/processwire/page/\' id=\'sidenav-page-3\'>Pages</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/page/\'><i class=\'fa fa-sitemap pw-nav-icon fa-fw\'></i>Pages</a></li><li class=\'page-8- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/page/list/navJSON/\' href=\'/spring_school_book/processwire/page/list/\'><i class=\'fa fa-sitemap pw-nav-icon fa-fw\'></i>Tree</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-6- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/page/add/navJSON/\' href=\'/spring_school_book/processwire/page/add/\'><i class=\'fa fa-plus-circle pw-nav-icon fa-fw\'></i>Add New</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-1007- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/page/lister/navJSON/\' href=\'/spring_school_book/processwire/page/lister/\'><i class=\'fa fa-search pw-nav-icon fa-fw\'></i>Find</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-1010- uk-parent\'><a href=\'/spring_school_book/processwire/page/recent-pages/\'><i class=\'fa fa-clock-o pw-nav-icon fa-fw\'></i>Recent</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/page/recent-pages/\'><i class=\'fa fa-arrow-circle-right pw-nav-icon fa-fw\'></i>Recent</a></li><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?edited=1\' href=\'/spring_school_book/processwire/page/recent-pages/?edited=1\'><i class=\'fa fa-users pw-nav-icon fa-fw\'></i>Edited</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?added=1\' href=\'/spring_school_book/processwire/page/recent-pages/?added=1\'><i class=\'fa fa-users pw-nav-icon fa-fw\'></i>Created</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?edited=1&amp;me=1\' href=\'/spring_school_book/processwire/page/recent-pages/?edited=1&me=1\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Edited by me</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?added=1&amp;me=1\' href=\'/spring_school_book/processwire/page/recent-pages/?added=1&me=1\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Created by me</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/anotherNavJSON/\' href=\'/spring_school_book/processwire/page/recent-pages/another/\'><i class=\'fa fa-plus-circle pw-nav-icon fa-fw\'></i>Add another</a><ul></ul></li></ul></li></ul></li><li class=\'page-22- uk-parent\'><a href=\'/spring_school_book/processwire/setup/\' id=\'sidenav-page-22\'>Setup</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/setup/\'><i class=\'fa fa-gears pw-nav-icon fa-fw\'></i>Setup</a></li><li class=\'page-11- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/setup/template/navJSON/\' href=\'/spring_school_book/processwire/setup/template/\'><i class=\'fa fa-cubes pw-nav-icon fa-fw\'></i>Templates</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-16- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/setup/field/navJSON/\' href=\'/spring_school_book/processwire/setup/field/\'><i class=\'fa fa-cube pw-nav-icon fa-fw\'></i>Fields</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-1012- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/setup/logs/navJSON/\' href=\'/spring_school_book/processwire/setup/logs/\'><i class=\'fa fa-tree pw-nav-icon fa-fw\'></i>Logs</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-1016- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/setup/languages/navJSON/\' href=\'/spring_school_book/processwire/setup/languages/\'><i class=\'fa fa-language pw-nav-icon fa-fw\'></i>Languages</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li></ul></li><li class=\'page-21- uk-parent\'><a href=\'/spring_school_book/processwire/module/\' id=\'sidenav-page-21\'>Modules</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/module/\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Modules</a></li><li class=\'page-0- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/module/navJSON/?site=1\' href=\'/spring_school_book/processwire/module/?site#tab_site_modules\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Site</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-0- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/module/navJSON/?core=1\' href=\'/spring_school_book/processwire/module/?core#tab_core_modules\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Core</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-0- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/module/navJSON/?configurable=1\' href=\'/spring_school_book/processwire/module/?configurable#tab_configurable_modules\'><i class=\'fa fa-gear pw-nav-icon fa-fw\'></i>Configure</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-0- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/module/navJSON/?install=1\' href=\'/spring_school_book/processwire/module/?install#tab_install_modules\'><i class=\'fa fa-sign-in pw-nav-icon fa-fw\'></i>Install</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-0-\'><a href=\'/spring_school_book/processwire/module/?new#tab_new_modules\'><i class=\'fa fa-plus pw-nav-icon fa-fw\'></i>New</a></li><li class=\'page-0-\'><a href=\'/spring_school_book/processwire/module/?reset=1\'><i class=\'fa fa-refresh pw-nav-icon fa-fw\'></i>Refresh</a></li></ul></li><li class=\'page-28- uk-parent\'><a href=\'/spring_school_book/processwire/access/\' id=\'sidenav-page-28\'>Access</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/access/\'><i class=\'fa fa-key pw-nav-icon fa-fw\'></i>Access</a></li><li class=\'page-29- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/access/users/navJSON/\' href=\'/spring_school_book/processwire/access/users/\'><i class=\'fa fa-group pw-nav-icon fa-fw\'></i>Users</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-30- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/access/roles/navJSON/\' href=\'/spring_school_book/processwire/access/roles/\'><i class=\'fa fa-gears pw-nav-icon fa-fw\'></i>Roles</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-31- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/access/permissions/navJSON/\' href=\'/spring_school_book/processwire/access/permissions/\'><i class=\'fa fa-gear pw-nav-icon fa-fw\'></i>Permissions</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li></ul></li><li class=\'page-1067-\'><a href=\'/spring_school_book/processwire/sessions-db/\' id=\'sidenav-page-1067\'>Sessions</a></li><li class=\'uk-parent\'><a href=\'#\'><!--pw-user-nav-label--></a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li><a target=\'_top\' href=\'/spring_school_book/\'><i class=\'fa fa-eye pw-nav-icon fa-fw\'></i>View site</a></li><li><a href=\'/spring_school_book/processwire/profile/\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Profile</a></li><li><a target=\'_top\' href=\'/spring_school_book/processwire/login/logout/\'><i class=\'fa fa-power-off pw-nav-icon fa-fw\'></i>Logout</a></li></ul></li>\";}s:14:\"ProcessPageAdd\";a:2:{s:3:\"nav\";a:6:{s:3:\"url\";s:41:\"/spring_school_book/processwire/page/add/\";s:5:\"label\";s:5:\"Pages\";s:4:\"icon\";s:11:\"plus-circle\";s:3:\"add\";N;s:4:\"list\";a:0:{}s:8:\"modified\";i:1580203576;}s:10:\"numAddable\";i:0;}s:15:\"ProcessPageList\";a:2:{s:6:\"lastID\";i:1020;s:9:\"parent_id\";s:5:\"title\";}}',	'2020-01-28 09:28:03',	0,	''),
('98u14q3dhbum1c1kvo7bcaqmav',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1580130345;}}',	'2020-01-27 13:05:45',	0,	''),
('9asj5bmptqsk5to3e28kptpf0v',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579684293;}}',	'2020-01-22 09:11:33',	0,	''),
('9fu64bcsllg8thv5ht2krm60q3',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1581074289;}}',	'2020-02-07 11:18:10',	0,	''),
('9sls81r8gon3g1e837656hudjg',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1583325309;}}',	'2020-03-04 12:35:09',	0,	''),
('9va8884uo3ld2i4kv890rvqo0b',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582534870;}}',	'2020-02-24 09:01:10',	0,	''),
('a9v64fgjjpb3g7hng0m34fgl9i',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579522573;}}',	'2020-01-20 12:16:13',	0,	''),
('aiie5cflgim3p4ns8tjjijgpf6',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1581434736;}}',	'2020-02-11 15:25:36',	0,	''),
('bb69gcau1jqmvju3buojo6trn9',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1581585628;}}',	'2020-02-13 09:20:28',	0,	''),
('cfd82jfo8av0de461camrnbsn8',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582038353;}}',	'2020-02-18 15:05:53',	0,	''),
('chbdadnc2caoe3mkggt744vsim',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1580830589;}}',	'2020-02-04 15:36:30',	0,	''),
('ckjkpbql0saq737np946592v9c',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1583845796;}}',	'2020-03-10 13:09:57',	0,	''),
('coran2jv7saoop3nhkb95h1003',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582542761;}}',	'2020-02-24 11:12:41',	0,	''),
('cpe9vrgthv8ae6e88s9p9mb646',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579869908;}}',	'2020-01-24 12:45:09',	0,	''),
('cq1c1rri78h4pfqlhrb1tqbo6u',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1581339330;}}',	'2020-02-10 12:55:30',	0,	''),
('d1ij7h7kmi67sl5pcvkv013tmb',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1578994965;}}',	'2020-01-14 09:42:46',	0,	''),
('donp9og688m552fbu9g0d5j05l',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579871640;}}',	'2020-01-24 13:14:00',	0,	''),
('f2du6o1sa350fa7dj3c1s7blk2',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582825118;}}',	'2020-02-27 17:38:39',	0,	''),
('g3unj90clrf2bs73hbte4ld2up',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582569596;}}',	'2020-02-24 18:39:56',	0,	''),
('gqd3sm3rgoad5ph8p5l35q93m1',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1583251525;}}',	'2020-03-03 16:05:25',	0,	''),
('ic79d4a0vlt69uuo5b3bpa7djh',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1583616229;}}',	'2020-03-07 21:23:50',	0,	''),
('ieeunauqr9c7bsifuki5k1q9gh',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579871040;}}',	'2020-01-24 13:04:00',	0,	''),
('ihu1miqao4c324nbgou6r2t6e3',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1580983730;}}',	'2020-02-06 10:08:50',	0,	''),
('ij2p2n1gqtmgk0t6e45gjmggrh',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579513779;}}',	'2020-01-20 09:49:39',	0,	''),
('jg8jm54g6ktl2h0g8k1mvl70h5',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582568996;}}',	'2020-02-24 18:29:57',	0,	''),
('joipcdei1hq8tqk4u43i9k6q5a',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579895250;}}',	'2020-01-24 19:47:30',	0,	''),
('jsdovjeg5m0huql1quq9n08tjk',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579513422;}}',	'2020-01-20 09:43:42',	0,	''),
('k954d7urp7i8r3lq7sb5ca5m4b',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1578929659;}}',	'2020-01-13 15:34:19',	0,	''),
('kaosoj129l93bqrkii9c7h3is3',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582066200;}}',	'2020-02-18 22:50:00',	0,	''),
('kfmfitequr3ulp1iot5is4utqe',	41,	27,	'Session|a:12:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1578911435;}s:15:\"ProcessPageView\";a:1:{s:18:\"loginRequestPageID\";i:2;}s:18:\"InputfieldSelector\";a:5:{s:5:\"id23_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:4:\"id2_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:4:\"id3_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:4:\"id8_\";a:1:{s:13:\"lastTemplates\";a:0:{}}s:5:\"id10_\";a:1:{s:13:\"lastTemplates\";a:0:{}}}s:12:\"ProcessLogin\";a:0:{}s:5:\"_user\";a:4:{s:2:\"id\";i:41;s:2:\"ts\";i:1578911435;s:9:\"challenge\";s:32:\"sk//F.BGPxF0CxwlGwAkY9KQ.ccD99kL\";s:11:\"fingerprint\";s:32:\"c54ee3b3adeaad20b78ff1885ce6c242\";}s:5:\"hidpi\";b:0;s:5:\"touch\";b:0;s:11:\"clientWidth\";i:1920;s:11:\"SessionCSRF\";a:2:{s:4:\"name\";s:25:\"TOKEN534657840X1578910637\";s:25:\"TOKEN534657840X1578910637\";s:32:\"upO22xfBObmJdLPZ1nnK4kUUEpKKq8Hw\";}s:15:\"AdminThemeUikit\";a:2:{s:5:\"prnav\";s:7008:\"<li class=\'page-3-\'><a href=\'/spring_school_book/processwire/page/\' id=\'prnav-page-3\' data-from=\'prnav-page-2\' class=\'pw-dropdown-toggle\'>Pages</a><ul class=\'pw-dropdown-menu prnav\' data-my=\'left top\' data-at=\'left bottom\'><li class=\'page-8-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-3\' data-json=\'/spring_school_book/processwire/page/list/navJSON/\' href=\'/spring_school_book/processwire/page/list/\'><i class=\'fa fa-sitemap pw-nav-icon fa-fw\'></i>Tree</a><ul></ul><li class=\'page-6-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-3\' data-json=\'/spring_school_book/processwire/page/add/navJSON/\' href=\'/spring_school_book/processwire/page/add/\'><i class=\'fa fa-plus-circle pw-nav-icon fa-fw\'></i>Add New</a><ul></ul><li class=\'page-1007-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-3\' data-json=\'/spring_school_book/processwire/page/lister/navJSON/\' href=\'/spring_school_book/processwire/page/lister/\'><i class=\'fa fa-search pw-nav-icon fa-fw\'></i>Find</a><ul></ul><li class=\'page-1010-\'><a class=\'pw-has-items\' data-from=\'prnav-page-3\' href=\'/spring_school_book/processwire/page/recent-pages/\'><i class=\'fa fa-clock-o pw-nav-icon fa-fw\'></i>Recent</a><ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?edited=1\' href=\'/spring_school_book/processwire/page/recent-pages/?edited=1\'><i class=\'fa fa-users pw-nav-icon fa-fw\'></i>Edited</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?added=1\' href=\'/spring_school_book/processwire/page/recent-pages/?added=1\'><i class=\'fa fa-users pw-nav-icon fa-fw\'></i>Created</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?edited=1&amp;me=1\' href=\'/spring_school_book/processwire/page/recent-pages/?edited=1&me=1\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Edited by me</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?added=1&amp;me=1\' href=\'/spring_school_book/processwire/page/recent-pages/?added=1&me=1\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Created by me</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/anotherNavJSON/\' href=\'/spring_school_book/processwire/page/recent-pages/another/\'><i class=\'fa fa-plus-circle pw-nav-icon fa-fw\'></i>Add another</a><ul></ul></li></ul></li></ul></li><li class=\'page-22-\'><a href=\'/spring_school_book/processwire/setup/\' id=\'prnav-page-22\' data-from=\'prnav-page-2\' class=\'pw-dropdown-toggle\'>Setup</a><ul class=\'pw-dropdown-menu prnav\' data-my=\'left-1 top\' data-at=\'left bottom\'><li class=\'page-11-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-22\' data-json=\'/spring_school_book/processwire/setup/template/navJSON/\' href=\'/spring_school_book/processwire/setup/template/\'><i class=\'fa fa-cubes pw-nav-icon fa-fw\'></i>Templates</a><ul></ul><li class=\'page-16-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-22\' data-json=\'/spring_school_book/processwire/setup/field/navJSON/\' href=\'/spring_school_book/processwire/setup/field/\'><i class=\'fa fa-cube pw-nav-icon fa-fw\'></i>Fields</a><ul></ul><li class=\'page-1012-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-22\' data-json=\'/spring_school_book/processwire/setup/logs/navJSON/\' href=\'/spring_school_book/processwire/setup/logs/\'><i class=\'fa fa-tree pw-nav-icon fa-fw\'></i>Logs</a><ul></ul><li class=\'page-1016-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-22\' data-json=\'/spring_school_book/processwire/setup/languages/navJSON/\' href=\'/spring_school_book/processwire/setup/languages/\'><i class=\'fa fa-language pw-nav-icon fa-fw\'></i>Languages</a><ul></ul></li></ul></li><li class=\'page-21-\'><a href=\'/spring_school_book/processwire/module/\' id=\'prnav-page-21\' data-from=\'prnav-page-2\' class=\'pw-dropdown-toggle\'>Modules</a><ul class=\'pw-dropdown-menu prnav\' data-my=\'left top\' data-at=\'left bottom\'><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-21\' data-json=\'/spring_school_book/processwire/module/navJSON/?site=1\' href=\'/spring_school_book/processwire/module/?site#tab_site_modules\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Site</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-21\' data-json=\'/spring_school_book/processwire/module/navJSON/?core=1\' href=\'/spring_school_book/processwire/module/?core#tab_core_modules\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Core</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-21\' data-json=\'/spring_school_book/processwire/module/navJSON/?configurable=1\' href=\'/spring_school_book/processwire/module/?configurable#tab_configurable_modules\'><i class=\'fa fa-gear pw-nav-icon fa-fw\'></i>Configure</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-21\' data-json=\'/spring_school_book/processwire/module/navJSON/?install=1\' href=\'/spring_school_book/processwire/module/?install#tab_install_modules\'><i class=\'fa fa-sign-in pw-nav-icon fa-fw\'></i>Install</a><ul></ul><li class=\'page-0-\'><a href=\'/spring_school_book/processwire/module/?new#tab_new_modules\'><i class=\'fa fa-plus pw-nav-icon fa-fw\'></i>New</a><li class=\'page-0-\'><a href=\'/spring_school_book/processwire/module/?reset=1\'><i class=\'fa fa-refresh pw-nav-icon fa-fw\'></i>Refresh</a></li></ul></li><li class=\'page-28-\'><a href=\'/spring_school_book/processwire/access/\' id=\'prnav-page-28\' data-from=\'prnav-page-2\' class=\'pw-dropdown-toggle\'>Access</a><ul class=\'pw-dropdown-menu prnav\' data-my=\'left top\' data-at=\'left bottom\'><li class=\'page-29-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-28\' data-json=\'/spring_school_book/processwire/access/users/navJSON/\' href=\'/spring_school_book/processwire/access/users/\'><i class=\'fa fa-group pw-nav-icon fa-fw\'></i>Users</a><ul></ul><li class=\'page-30-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-28\' data-json=\'/spring_school_book/processwire/access/roles/navJSON/\' href=\'/spring_school_book/processwire/access/roles/\'><i class=\'fa fa-gears pw-nav-icon fa-fw\'></i>Roles</a><ul></ul><li class=\'page-31-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-28\' data-json=\'/spring_school_book/processwire/access/permissions/navJSON/\' href=\'/spring_school_book/processwire/access/permissions/\'><i class=\'fa fa-gear pw-nav-icon fa-fw\'></i>Permissions</a><ul></ul></li></ul></li><li class=\'page-1067-\'><a href=\'/spring_school_book/processwire/sessions-db/\'>Sessions</a></li>\";s:7:\"sidenav\";s:9532:\"<li class=\'page-3- uk-parent\'><a href=\'/spring_school_book/processwire/page/\' id=\'sidenav-page-3\'>Pages</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/page/\'><i class=\'fa fa-sitemap pw-nav-icon fa-fw\'></i>Pages</a></li><li class=\'page-8- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/page/list/navJSON/\' href=\'/spring_school_book/processwire/page/list/\'><i class=\'fa fa-sitemap pw-nav-icon fa-fw\'></i>Tree</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-6- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/page/add/navJSON/\' href=\'/spring_school_book/processwire/page/add/\'><i class=\'fa fa-plus-circle pw-nav-icon fa-fw\'></i>Add New</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-1007- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/page/lister/navJSON/\' href=\'/spring_school_book/processwire/page/lister/\'><i class=\'fa fa-search pw-nav-icon fa-fw\'></i>Find</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-1010- uk-parent\'><a href=\'/spring_school_book/processwire/page/recent-pages/\'><i class=\'fa fa-clock-o pw-nav-icon fa-fw\'></i>Recent</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/page/recent-pages/\'><i class=\'fa fa-arrow-circle-right pw-nav-icon fa-fw\'></i>Recent</a></li><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?edited=1\' href=\'/spring_school_book/processwire/page/recent-pages/?edited=1\'><i class=\'fa fa-users pw-nav-icon fa-fw\'></i>Edited</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?added=1\' href=\'/spring_school_book/processwire/page/recent-pages/?added=1\'><i class=\'fa fa-users pw-nav-icon fa-fw\'></i>Created</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?edited=1&amp;me=1\' href=\'/spring_school_book/processwire/page/recent-pages/?edited=1&me=1\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Edited by me</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/navJSON/?added=1&amp;me=1\' href=\'/spring_school_book/processwire/page/recent-pages/?added=1&me=1\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Created by me</a><ul></ul><li class=\'page-0-\'><a class=\'pw-has-items pw-has-ajax-items\' data-from=\'prnav-page-1010\' data-json=\'/spring_school_book/processwire/page/recent-pages/anotherNavJSON/\' href=\'/spring_school_book/processwire/page/recent-pages/another/\'><i class=\'fa fa-plus-circle pw-nav-icon fa-fw\'></i>Add another</a><ul></ul></li></ul></li></ul></li><li class=\'page-22- uk-parent\'><a href=\'/spring_school_book/processwire/setup/\' id=\'sidenav-page-22\'>Setup</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/setup/\'><i class=\'fa fa-gears pw-nav-icon fa-fw\'></i>Setup</a></li><li class=\'page-11- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/setup/template/navJSON/\' href=\'/spring_school_book/processwire/setup/template/\'><i class=\'fa fa-cubes pw-nav-icon fa-fw\'></i>Templates</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-16- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/setup/field/navJSON/\' href=\'/spring_school_book/processwire/setup/field/\'><i class=\'fa fa-cube pw-nav-icon fa-fw\'></i>Fields</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-1012- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/setup/logs/navJSON/\' href=\'/spring_school_book/processwire/setup/logs/\'><i class=\'fa fa-tree pw-nav-icon fa-fw\'></i>Logs</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-1016- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/setup/languages/navJSON/\' href=\'/spring_school_book/processwire/setup/languages/\'><i class=\'fa fa-language pw-nav-icon fa-fw\'></i>Languages</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li></ul></li><li class=\'page-21- uk-parent\'><a href=\'/spring_school_book/processwire/module/\' id=\'sidenav-page-21\'>Modules</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/module/\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Modules</a></li><li class=\'page-0- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/module/navJSON/?site=1\' href=\'/spring_school_book/processwire/module/?site#tab_site_modules\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Site</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-0- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/module/navJSON/?core=1\' href=\'/spring_school_book/processwire/module/?core#tab_core_modules\'><i class=\'fa fa-plug pw-nav-icon fa-fw\'></i>Core</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-0- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/module/navJSON/?configurable=1\' href=\'/spring_school_book/processwire/module/?configurable#tab_configurable_modules\'><i class=\'fa fa-gear pw-nav-icon fa-fw\'></i>Configure</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-0- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/module/navJSON/?install=1\' href=\'/spring_school_book/processwire/module/?install#tab_install_modules\'><i class=\'fa fa-sign-in pw-nav-icon fa-fw\'></i>Install</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-0-\'><a href=\'/spring_school_book/processwire/module/?new#tab_new_modules\'><i class=\'fa fa-plus pw-nav-icon fa-fw\'></i>New</a></li><li class=\'page-0-\'><a href=\'/spring_school_book/processwire/module/?reset=1\'><i class=\'fa fa-refresh pw-nav-icon fa-fw\'></i>Refresh</a></li></ul></li><li class=\'page-28- uk-parent\'><a href=\'/spring_school_book/processwire/access/\' id=\'sidenav-page-28\'>Access</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li class=\'pw-nav-dup\'><a href=\'/spring_school_book/processwire/access/\'><i class=\'fa fa-key pw-nav-icon fa-fw\'></i>Access</a></li><li class=\'page-29- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/access/users/navJSON/\' href=\'/spring_school_book/processwire/access/users/\'><i class=\'fa fa-group pw-nav-icon fa-fw\'></i>Users</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-30- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/access/roles/navJSON/\' href=\'/spring_school_book/processwire/access/roles/\'><i class=\'fa fa-gears pw-nav-icon fa-fw\'></i>Roles</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li><li class=\'page-31- uk-parent\'><a class=\'pw-has-items pw-has-ajax-items\' data-json=\'/spring_school_book/processwire/access/permissions/navJSON/\' href=\'/spring_school_book/processwire/access/permissions/\'><i class=\'fa fa-gear pw-nav-icon fa-fw\'></i>Permissions</a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'></ul></li></ul></li><li class=\'page-1067-\'><a href=\'/spring_school_book/processwire/sessions-db/\' id=\'sidenav-page-1067\'>Sessions</a></li><li class=\'uk-parent\'><a href=\'#\'><!--pw-user-nav-label--></a><ul class=\'uk-nav-sub uk-nav-default uk-nav-parent-icon\' data-uk-nav=\'animation: false; multiple: true;\'><li><a target=\'_top\' href=\'/spring_school_book/\'><i class=\'fa fa-eye pw-nav-icon fa-fw\'></i>View site</a></li><li><a href=\'/spring_school_book/processwire/profile/\'><i class=\'fa fa-user pw-nav-icon fa-fw\'></i>Profile</a></li><li><a target=\'_top\' href=\'/spring_school_book/processwire/login/logout/\'><i class=\'fa fa-power-off pw-nav-icon fa-fw\'></i>Logout</a></li></ul></li>\";}s:14:\"ProcessPageAdd\";a:2:{s:3:\"nav\";a:6:{s:3:\"url\";s:41:\"/spring_school_book/processwire/page/add/\";s:5:\"label\";s:5:\"Pages\";s:4:\"icon\";s:11:\"plus-circle\";s:3:\"add\";N;s:4:\"list\";a:0:{}s:8:\"modified\";i:1578910637;}s:10:\"numAddable\";i:0;}s:15:\"ProcessPageList\";a:2:{s:6:\"lastID\";i:1020;s:9:\"parent_id\";s:5:\"title\";}}',	'2020-01-13 10:30:35',	0,	''),
('l6la5bhdmppcnj9rpr6224ujcl',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579326303;}}',	'2020-01-18 05:45:03',	0,	''),
('lemobgdl1133ik02lr1ncqs7g3',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1581316355;}}',	'2020-02-10 06:32:36',	0,	''),
('lk3jqll5akq7m4081rbufc8tdr',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582537521;}}',	'2020-02-24 09:45:21',	0,	''),
('lnfh289ovrtebitpt16kafmfhc',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582085926;}}',	'2020-02-19 04:18:46',	0,	''),
('m52kl7mk0fgjcdc2tjcjmm835a',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1580121800;}}',	'2020-01-27 10:43:21',	0,	''),
('m6r71ugh2nkj0kbd7smuff8uio',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1583308553;}}',	'2020-03-04 07:55:53',	0,	''),
('m9i19bh78crm7tj3gn8cpdcblf',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1581021698;}}',	'2020-02-06 20:41:38',	0,	''),
('mo03m1va5vhj5qusb4ob59vj1j',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1578994948;}}',	'2020-01-14 09:42:29',	0,	''),
('mvkj152b84eq184uejel5q8a3q',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1583689094;}}',	'2020-03-08 17:38:14',	0,	''),
('n3u7q7165mn8rrege3dkqvr4it',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582555286;}}',	'2020-02-24 14:41:26',	0,	''),
('no3500kcmi2c73qvh9vlnard1q',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1583327559;}}',	'2020-03-04 13:12:39',	0,	''),
('oicai7si8i8v4ncun08b51na9d',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1581284452;}}',	'2020-02-09 21:40:53',	0,	''),
('onakc0eam20ebape8amuo60qm2',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1583694914;}}',	'2020-03-08 19:15:14',	0,	''),
('prqojvlmb9pcpj7d2d8cr8fg1q',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1580207653;}}',	'2020-01-28 10:34:13',	0,	''),
('q1j2i3jnqfk3s44pcddfmq3du5',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579324780;}}',	'2020-01-18 05:19:40',	0,	''),
('rjsn5c5mervh58j49qdc610n93',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579462987;}}',	'2020-01-19 19:43:07',	0,	''),
('rnqfe0ec7ro1gqjshmqpt77ja0',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1578929974;}}',	'2020-01-13 15:39:35',	0,	''),
('rpur0qnvvtcdvrht10qtbs9d1h',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579871237;}}',	'2020-01-24 13:07:17',	0,	''),
('rqiku2lfchpk5bboidvlo4hni1',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1581156591;}}',	'2020-02-08 10:09:51',	0,	''),
('rro9ic57lf81eon8qfktlltfni',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1582295367;}}',	'2020-02-21 14:29:28',	0,	''),
('rvob80o0tqcvlec970pabbpsgr',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579958216;}}',	'2020-01-25 13:16:57',	0,	''),
('s2pnbpsf5t24ttn89q0vb6pc1t',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579513421;}}',	'2020-01-20 09:43:41',	0,	''),
('slh04t0tc592t0nb8hl1lbuvlj',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1581526549;}}',	'2020-02-12 16:55:50',	0,	''),
('tg92gp21mkc1kcvo6ah5mnn929',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1580202236;}}',	'2020-01-28 09:03:57',	0,	''),
('tp19gfdo1gor7r3e5pl4qd696n',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1583156037;}}',	'2020-03-02 13:33:58',	0,	''),
('tp6v2eg194518e30m117d8kttn',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1578995007;}}',	'2020-01-14 09:43:28',	0,	''),
('tvin3riassaq8kd5flop69c7mq',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1583496352;}}',	'2020-03-06 12:05:52',	0,	''),
('v6d4233lnjsrgs1jd6s122dff8',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1580480790;}}',	'2020-01-31 14:26:30',	0,	''),
('v7efbtdoidncmpdtubdubq79ok',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1580036418;}}',	'2020-01-26 11:00:19',	0,	''),
('vjm9irhnn353ae7qts5qhrr5d9',	40,	1,	'Session|a:1:{s:16:\"SessionHandlerDB\";a:1:{s:2:\"ts\";i:1579464639;}}',	'2020-01-19 20:10:40',	0,	'');

DROP TABLE IF EXISTS `session_login_throttle`;
CREATE TABLE `session_login_throttle` (
  `name` varchar(128) NOT NULL,
  `attempts` int(10) unsigned NOT NULL DEFAULT '0',
  `last_attempt` int(10) unsigned NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `session_login_throttle` (`name`, `attempts`, `last_attempt`) VALUES
('admin',	1,	1581590774);

DROP TABLE IF EXISTS `templates`;
CREATE TABLE `templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  `fieldgroups_id` int(10) unsigned NOT NULL DEFAULT '0',
  `flags` int(11) NOT NULL DEFAULT '0',
  `cache_time` mediumint(9) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fieldgroups_id` (`fieldgroups_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES
(2,	'admin',	2,	8,	0,	'{\"useRoles\":1,\"parentTemplates\":[2],\"allowPageNum\":1,\"redirectLogin\":23,\"slashUrls\":1,\"noGlobal\":1,\"compile\":3,\"modified\":1576506585,\"ns\":\"ProcessWire\"}'),
(3,	'user',	3,	8,	0,	'{\"useRoles\":1,\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"User\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}'),
(4,	'role',	4,	8,	0,	'{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"Role\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}'),
(5,	'permission',	5,	8,	0,	'{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"guestSearchable\":1,\"pageClass\":\"Permission\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}'),
(1,	'home',	1,	0,	0,	'{\"useRoles\":1,\"noParents\":1,\"slashUrls\":1,\"compile\":3,\"modified\":1581591442,\"ns\":\"\\\\\",\"roles\":[37]}'),
(29,	'basic-page',	83,	0,	0,	'{\"slashUrls\":1,\"compile\":3,\"modified\":1578911067,\"ns\":\"\\\\\"}'),
(43,	'abstract-page',	97,	0,	0,	'{\"slashUrls\":1,\"compile\":3,\"modified\":1569315115}'),
(44,	'workshop-page',	98,	0,	0,	'{\"slashUrls\":1,\"compile\":3,\"modified\":1569916059}'),
(45,	'contrib-page',	99,	0,	0,	'{\"slashUrls\":1,\"compile\":3,\"modified\":1576506585,\"ns\":\"\\\\\"}'),
(46,	'language',	100,	8,	0,	'{\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"Language\",\"pageLabelField\":\"name\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noChangeTemplate\":1,\"noUnpublish\":1,\"compile\":3,\"nameContentTab\":1,\"modified\":1569314739}'),
(47,	'conf-page',	101,	0,	0,	'{\"slashUrls\":1,\"compile\":3,\"modified\":1578911067,\"ns\":\"\\\\\"}');

DROP TABLE IF EXISTS `textformatter_video_embed`;
CREATE TABLE `textformatter_video_embed` (
  `video_id` varchar(128) NOT NULL,
  `embed_code` varchar(1024) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `textformatter_video_embed` (`video_id`, `embed_code`, `created`) VALUES
('IioOwrrudEI',	'<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/IioOwrrudEI?feature=oembed\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>',	'2020-01-28 09:27:38'),
('ks9wjcGC6as',	'<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/ks9wjcGC6as?feature=oembed\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>',	'2020-02-13 11:10:52'),
('sRFX4HRneBM',	'<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/sRFX4HRneBM?feature=oembed\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>',	'2020-01-28 09:27:38'),
('VVYe0qR5DnM',	'<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/VVYe0qR5DnM?feature=oembed\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>',	'2019-12-16 11:11:14'),
('XKnG7sikE-U',	'<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/XKnG7sikE-U?feature=oembed\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>',	'2019-12-09 14:51:35');

-- 2020-03-11 14:44:47
