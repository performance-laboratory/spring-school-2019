<?php include("inc/head.php"); ?>

<?php include("inc/header.php"); ?>

<!-- presentation -->
<div class="container-fluid mx-auto mt-4">
	<div class="row no-gutters justify-content-center">
	<article class="sub-header col-12 col-sm-12 col-md-8 col-lg-8 m-1 p-2 p-md-3">
		<?= $page->body; ?>
	</article>
	</div>
</div>
<div class="container-fluid mt-4">

	<div id="main_row" class="row no-gutters">

		<!-- workshops -->
		<div id="container_ws" class="col-sm-12 col-md-4">

<?php foreach($events as $ws): ?>
<?php if($ws->template == "conf-page"): ?>
		    <div class="event conf" data-ws="<?= $ws->id ?>">
			    	<article class="p-2 p-md-3">
				      	<?= $ws->body ?>
			    	</article>
		    	<div class="container-fluid">
		    		<div class="row container_contrib_mobile"></div>
		    	</div>
			</div>
<?php else: ?>
		    <div class="event" data-ws="<?= $ws->id ?>">
		    	<label>
			    	<div class="container_checkbox row justify-content-center">
					  <input type="checkbox" value="<?= $ws->id ?>">
					</div>
			    	<article class="p-2 p-md-3">
				      	<?= $ws->body ?>
			    	</article>
		    	</label>
		    	<div class="container-fluid">
		    		<div class="row container_contrib_mobile"></div>
		    	</div>
			</div>
<?php endif; ?>
<?php endforeach; ?>
		</div>

		<!-- contributions -->
		<div class="col-sm-0 col-md-8" id="container_contrib">

<?php foreach ($contributions as $key => $contrib): ?>

<!-- BODY -->
<?php if ($contrib->body): ?>
		    <div class="col-12 col-sm-6 col-md-7 col-lg-6 offset-md-0 offset-lg-<?=random(0,2)?> contrib text" data-ws="<?= $contrib->related_workshop->id ?>">
		    	<article class="p-2 p-md-3">
					<?= $contrib->body ?>
<?php include("inc/media_caption.php"); ?>
		    	</article>
		    </div>
<?php endif; ?>


<!-- IMAGES -->
<?php foreach ($contrib->images as $key => $img): ?>
<?php
$options = array(
	'upscaling' => false,
	'quality' => 80
);
$low_img = $img->width("100px", $options);
$med_img = $img->width("500px", $options);
// avant le slide : low_img est chargée puis remplacée par lazysizes par med_img 
// breakpoint à 992px
// et à 768px
?>
		    <div class="col-6 col-sm-4 col-lg-<?=random(3,4)?> offset-md-0 offset-lg-<?=random(0,2)?> contrib" data-ws="<?= $contrib->related_workshop->id ?>">
		    	<article class="p-2 p-md-3">
					<img class="blur-up lazyload" src="<?= $low_img->url; ?>" data-src="<?= $med_img->url ?>"  data-flickity-lazyload="<?= $img->url ?>">
<?php include("inc/media_caption.php"); ?>
		      	</article>
		    </div>
<?php endforeach; ?>


<!-- SONS -->
<?php foreach ($contrib->sounds as $key => $snd): ?>
		    <div class="col-12 col-sm-6 col-md-7 col-lg-6 offset-md-0 offset-lg-<?=random(0,2)?> contrib sound" data-ws="<?= $contrib->related_workshop->id ?>">
		    	<article class="p-2 p-md-3">
					<audio class="player" controls>
					    <source src="<?= $snd->url ?>" type="audio/mp3" />
					</audio>
<?php include("inc/media_caption.php"); ?>
		    	</article>
		    </div>
<?php endforeach; ?>


<!-- VIDEOS -->
<?php foreach ($contrib->videos as $key => $vid): ?>
		    <div class="col-6 col-sm-6 col-md-6 col-lg-4 offset-md-0 offset-lg-<?=random(0,1)?> contrib video" data-ws="<?= $contrib->related_workshop->id ?>">
		    	<article class="p-2 p-md-3">
					<video class="player" controls>
						<source src="<?= $vid->url ?>"type="video/mp4">
						Sorry, your browser doesn't support embedded videos.
					</video>
<?php include("inc/media_caption.php"); ?>
		    	</article>
		    </div>
<?php endforeach; ?>


<?php endforeach; ?>
		</div>

</div>


<div class="modal" id="container_slider">
	<div class="modal_content" id="slider"></div>
	<button class="close_modal col-1">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
			<path d="M12 10.6L6.6 5.2 5.2 6.6l5.4 5.4-5.4 5.4 1.4 1.4 5.4-5.4 5.4 5.4 1.4-1.4-5.4-5.4 5.4-5.4-1.4-1.4-5.4 5.4z"></path>
		</svg>
	</button>
</div>

<div class="modal row justify-content-center" id="container_txt">
	<div class="modal_content col-11 col-sm-10 col-md-8 col-lg-8 m-1" id="txt"></div>
	<button class="close_modal col-1">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
			<path d="M12 10.6L6.6 5.2 5.2 6.6l5.4 5.4-5.4 5.4 1.4 1.4 5.4-5.4 5.4 5.4 1.4-1.4-5.4-5.4 5.4-5.4-1.4-1.4-5.4 5.4z"></path>
		</svg>
	</button>
</div>

</div>

<?php include("inc/footer.php"); ?>


<?php include("inc/foot.php"); ?>

